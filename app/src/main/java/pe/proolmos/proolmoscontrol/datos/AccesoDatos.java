package pe.proolmos.proolmoscontrol.datos;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class AccesoDatos extends SQLiteOpenHelper {
    private static String nombreBD = "bdproolmos";
    private static int versionBd = 18;
    public static Context aplicacion;


    private String tablaCategoriaVehiculo = "CREATE TABLE categoria_vehiculo " +
            "( " +
            "  codigo int PRIMARY KEY, " +
            "  nombre varchar(100), " +
            "  descripcion varchar(100), " +
            "  precio real " +
            ");";

    private String tablaCategoriaVehiculoDatos[] = {
            "insert into categoria_vehiculo values(1,'CATEGORÍA A', 'AUTO - CAMIONETA - COMBI - STATION', 10.00);",
            "insert into categoria_vehiculo values(2,'CATEGORÍA B', 'MINIVAN - CAMIONCITO', 15.00);",
            "insert into categoria_vehiculo values(3,'CATEGORÍA C1', 'BUSES DE PERSONAL - CAMIONES', 25.00);",
            "insert into categoria_vehiculo values(4,'CATEGORÍA C2', 'TRAILER - CISTERNA - VOLQUETE', 50.00);",
            "insert into categoria_vehiculo values(5,'CATEGORÍA C3', 'TRAILER CONTENEDORES', 200.00);",
            "insert into categoria_vehiculo values(6,'CATEGORÍA A1', 'MOTO', 0.00);"
    };

    private String tablaTipoVehiculo = "CREATE TABLE tipo_vehiculo "+
            "( " +
            "   codigo int PRIMARY KEY, " +
            "   descripcion varchar(100), " +
            "   codigo_categoria int, " +
            "   constraint fk_tipo_categoria foreign key (codigo_categoria) references categoria_vehiculo(codigo) " +
            ");";

    private String tablaTipoVehiculoDatos[] = {
            "insert into tipo_vehiculo values(1, 'AUTO', 1);",
            "insert into tipo_vehiculo values(2, 'CAMIONETA', 1);",
            "insert into tipo_vehiculo values(3, 'COMBI', 1);",
            "insert into tipo_vehiculo values(4, 'STATION', 1);",
            "insert into tipo_vehiculo values(5, 'MINIVAN', 2);",
            "insert into tipo_vehiculo values(6, 'CAMIONCITO', 2);",
            "insert into tipo_vehiculo values(8, 'CAMIONES', 3);",
            "insert into tipo_vehiculo values(7, 'BUSES DE PERSONAL', 3);",
            "insert into tipo_vehiculo values(9, 'TRAILER', 4);",
            "insert into tipo_vehiculo values(10, 'CISTERNA', 4);",
            "insert into tipo_vehiculo values(11, 'VOLQUETE', 4);",
            "insert into tipo_vehiculo values(12, 'TRAILER CONTENEDOR', 5);",
            "insert into tipo_vehiculo values(13, 'MOTO - OTROS', 6);"
    };

    private String tablaTipoPersonal = "CREATE TABLE tipo_personal " +
            "( " +
            "   codigo int PRIMARY KEY, " +
            "   descripcion varchar(100) " +
            ");";

    private String tablaTipoPersonalDatos[] = {
            "insert into tipo_personal values(1, 'TRABAJADOR');",
            "insert into tipo_personal values(2, 'TRANSPORTE PERSONAL');",
            "insert into tipo_personal values(3, 'PROVEEDOR');",
            "insert into tipo_personal values(4, 'VISITA');"
    };

    private String tablaEmpresa = "CREATE TABLE empresa " +
            "( " +
            "   codigo int primary key, " +
            "   nombre varchar(200), " +
            "   ruc char(11) UNIQUE, " +
            "   longitud real, " +
            "   latitud real " +
            ");";

    private String tablaPersona = "CREATE TABLE persona " +
            "( " +
            "   dni varchar(15) primary key, " +
            "   nombre varchar(100), " +
            "   paterno varchar(100), " +
            "   materno varchar(100), " +
            "   estado char(1), " +
            "   sincronizado char(1), " +
            "   codigo_off_line text" +
            ");";

    private String tablaSancionados = "CREATE TABLE sancionado " +
            "( " +
            "   dni varchar(15), " +
            "   motivo varchar(500), " +
            "   desde text, " +
            "   hasta text " +
            ");";

    private String tablaVisitas = "CREATE TABLE visita " +
            "( " +
            "   codigo_visita int, " +
            "   dni varchar(15), " +
            "   codigo_entrada_fundo int, " +
            "   comentario text, " +
            "   sincronizado char(1), " +
            "   codigo_off_line text, " +
            "   codigo_off_line_fundo text, " +
            "   constraint pk_visita primary key (codigo_off_line, codigo_off_line_fundo)" +
            ");";

    private String tablaFundo = "CREATE TABLE IF NOT EXISTS fundo " +
            "( " +
            "   codigo_entrada_fundo int, " +
            "   fecha_entrada_fundo text, " +
            "   hora_entrada_fundo text, " +
            "   dni_conductor varchar(15), " +
            "   nombre_conductor varchar(500), " +
            "   codigo_categoria_personal int, " +
            "   nombre_empresa varchar(100), " +
            "   codigo_tipo_vehiculo int," +
            "   placa_entrada_fundo varchar(100), " +
            "   procedencia_fundo varchar(100), " +
            "   numero_asientos int, " +
            "   numero_ocupantes int, " +
            "   motivo_fundo text, " +
            "   contenedor char(1), " +
            "   entrada_salida_fundo char(1), " +
            "   codigo_empresa int, " +
            "   codigo_usuario int, " +
            "   estado_entrada char(1), " +
            "   hora_manual text, " +
            "   codigo_ruta int, " +
            "   codigo_entrada int, " +
            "   fecha_manual text, " +
            "   sincronizado char(1), " +
            "   codigo_off_line text, " +
            "   codigo_caseta int, " +
            "   orientacion text, " +
            "   pin char(8),  " +
            "   mano_obra char(1) default 'N', " +
            "   codigo_proveedor int, " +
            "   codigo_ruta_transporte int, " +
            "   codigo_proveedor_empresa int," +
            "   constraint pk_fundo primary key (codigo_entrada_fundo, codigo_off_line) " +
            ");";

    private String tablaSincronizacion = "CREATE TABLE sincronizacion " +
            "(  " +
            "   tabla text primary key, " +
            "   estado char(1), " +
            "   fecha text, " +
            "   hora text " +
            ");";

    String tablaManoObra = "CREATE TABLE IF NOT EXISTS mano_obra(" +
            "codigo int not null, " +
            "nombre varchar(100) not null,  " +
            "estado char(1) DEFAULT 'A', " +
            "CONSTRAINT pk_mano_obra PRIMARY KEY (codigo) " +
            ");";

    private String tablaManoObraEntrada = "CREATE TABLE IF NOT EXISTS mano_obra_entrada " +
            "(  " +
            "   codigo_entrada_fundo int not null, " +
            "   codigo_tipo_mano_obra int not null, " +
            "   numero_trabajadores int not null, " +
            "   sincronizado char(1), " +
            "   codigo_off_line char(32), " +
            "   codigo_off_line_fundo char(32), " +
            "   constraint pk_mano_obra_entrada primary key (codigo_off_line, codigo_off_line_fundo)" +
            ")";

    private String tablaDetalleTransporte = "CREATE TABLE IF NOT EXISTS detalle_transporte" +
            "(" +
            "   codigo_detalle_transporte int not null," +
            "   codigo_conductor int not null," +
            "   codigo_vehiculo int not null, " +
            "   estado char(1) not null, " +
            "   constraint pk_detalle_transporte primary key (codigo_detalle_transporte)" +
            ");";
    private String tablaProveedor = "CREATE TABLE IF NOT EXISTS proveedor " +
            "(" +
            "   codigo_proveedor int not null," +
            "   ruc char(11) UNIQUE not null," +
            "   razon_social varchar(250) not null, " +
            "   constraint pk_proveedor primary key (codigo_proveedor)" +
            ");";

    private String tablaProveedorEmpresa = "CREATE TABLE IF NOT EXISTS proveedor_empresa " +
            "(" +
            "   codigo_proveedor_empresa int not null," +
            "   codigo_proveedor int not null, " +
            "   codigo_conductor int not null, " +
            "   codigo_vehiculo int not null, " +
            "   codigo_ruta_transporte int not null," +
            "   estado char(1) not null, " +
            "   constraint pk_proveedor_empresa primary key (codigo_proveedor_empresa) " +
            ");";

    private String tablaVehiculo = "CREATE TABLE IF NOT EXISTS vehiculo " +
            "(" +
            "   codigo_vehiculo int not null, " +
            "   codigo_tipo_vehiculo int not null," +
            "   placa varchar(8) UNIQUE not null, " +
            "   capacidad int not null, " +
            "   estado char(1) not null, " +
            "   constraint pk_vehiculo primary key (codigo_vehiculo)" +
            ");";
    public AccesoDatos(){super(aplicacion, nombreBD, null, versionBd);}

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(tablaCategoriaVehiculo);
        db.execSQL(tablaTipoVehiculo);
        db.execSQL(tablaTipoPersonal);
        db.execSQL(tablaEmpresa);
        db.execSQL(tablaPersona);
        db.execSQL(tablaSancionados);
        db.execSQL(tablaVisitas);
        db.execSQL(tablaSincronizacion);
        db.execSQL(tablaFundo);

        for(int i = 0; i < tablaCategoriaVehiculoDatos.length; i++){
            db.execSQL(tablaCategoriaVehiculoDatos[i]);
        }
        for(int i = 0; i < tablaTipoVehiculoDatos.length; i++){
            db.execSQL(tablaTipoVehiculoDatos[i]);
        }
        for(int i = 0; i < tablaTipoPersonalDatos.length; i++){
            db.execSQL(tablaTipoPersonalDatos[i]);
        }

        try{
            db.execSQL(tablaManoObra);
        }catch (RuntimeException e){
        }

        try{
            db.execSQL(tablaManoObraEntrada);
        }catch (RuntimeException e){
        }

        try{
            db.execSQL(tablaDetalleTransporte);
        }catch (RuntimeException e){
            Log.e("TAG-SQLite", "tablaDetalleTransporte");
        }
        try{
            db.execSQL(tablaProveedor);
        }catch (RuntimeException e){
            Log.e("TAG-SQLite", "tablaProveedor");
        }
        try{
            db.execSQL(tablaProveedorEmpresa);
        }catch (RuntimeException e){
            Log.e("TAG-SQLite", "tablaProveedorEmpresa");
        }
        try{
            db.execSQL(tablaVehiculo);
        }catch (RuntimeException e){
            Log.e("TAG-SQLite", "tablaVehiculo");
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if(newVersion > oldVersion){
            try{
                db.execSQL("ALTER TABLE fundo add column mano_obra char(1) default 'N'");
            }catch (RuntimeException e){
            }

            try{
                db.execSQL(tablaManoObra);
            }catch (RuntimeException e){
            }

            try{
                db.execSQL(tablaManoObraEntrada);
            }catch (RuntimeException e){
            }

            try{
                db.execSQL("ALTER TABLE fundo add column codigo_proveedor int");
                db.execSQL("ALTER TABLE fundo add column codigo_ruta_transporte int");
                db.execSQL("ALTER TABLE fundo add column codigo_proveedor_empresa int");
            }catch (RuntimeException e){
            }

            try{
                db.execSQL(tablaDetalleTransporte);
            }catch (RuntimeException e){
                Log.e("TAG-SQLite-upd", "tablaDetalleTransporte");
            }
            try{
                db.execSQL(tablaProveedor);
            }catch (RuntimeException e){
                Log.e("TAG-SQLite-upd", "tablaProveedor");
            }
            try{
                db.execSQL(tablaProveedorEmpresa);
            }catch (RuntimeException e){
                Log.e("TAG-SQLite-upd", "tablaProveedorEmpresa");
            }
            try{
                db.execSQL(tablaVehiculo);
            }catch (RuntimeException e){
                Log.e("TAG-SQLite-upd", "tablaVehiculo");
            }
        }
    }
}
