package pe.proolmos.proolmoscontrol;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import java.util.ArrayList;

import androidx.recyclerview.widget.RecyclerView;
import pe.proolmos.proolmoscontrol.Negocio.Visita;

public class AgregarVisitasAdaptador extends RecyclerView.Adapter<AgregarVisitasAdaptador.ViewHolder> {

    private Context context;
    public static ArrayList<Visita> listaDatos;
    View vista;

    public AgregarVisitasAdaptador(Context context, ArrayList<Visita> listaDatos) {
        this.context = context;
        this.listaDatos = listaDatos;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_visita, parent, false);
        vista = v;
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    public void setListaDatos(ArrayList<Visita> mlistaDatos) {
        this.listaDatos = mlistaDatos;
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        final Visita item = listaDatos.get(position);
        holder.txtDocumento.setText("");
        holder.txtDocumento.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                item.setDni(s.toString().toUpperCase());
            }
        });
    }

    @Override
    public int getItemCount() {
        return listaDatos.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        EditText txtDocumento;

        public ViewHolder(View itemView) {
            super(itemView);
            txtDocumento = (EditText) itemView.findViewById(R.id.txtDocumentoVisita);
        }
    }

}
