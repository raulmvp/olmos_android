package pe.proolmos.proolmoscontrol;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import pe.proolmos.proolmoscontrol.Negocio.Fundo;
import pe.proolmos.proolmoscontrol.Negocio.ManoObraEntrada;
import pe.proolmos.proolmoscontrol.Negocio.Persona;
import pe.proolmos.proolmoscontrol.Negocio.Sancionado;
import pe.proolmos.proolmoscontrol.Negocio.Visita;
import pe.proolmos.proolmoscontrol.Sesion.SesionPreferences;
import pe.proolmos.proolmoscontrol.databinding.ActivitySincronizadoBinding;
import pe.proolmos.proolmoscontrol.util.Funciones;

public class SincronizadoActivity extends AppCompatActivity {
    private SesionPreferences sesion;
    private ActivitySincronizadoBinding binding;
    Context contexto;
    public static Dialog cargando;
    Animation arribaAbajo,abajoArriba;

    CargarListaFundos asynFundos;
    CargarListaPersonas asynPersonas;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        contexto = this;

        sesion = new SesionPreferences(contexto);
        sesion.validarSesion();
        sesion.setPinUsuario("");

        ActionBar actionBar = getSupportActionBar();
        actionBar.hide();

        arribaAbajo= AnimationUtils.loadAnimation(this,R.anim.dearribaabajocompleto);
        abajoArriba = AnimationUtils.loadAnimation(this,R.anim.deabajoarribacompleto);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_sincronizado);

        binding.lyHeader.setAnimation(arribaAbajo);
        binding.lyBody.setAnimation(abajoArriba);
        binding.lyEntradas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //sincronizarEntradas();
                Intent i = new Intent(SincronizadoActivity.this, SincronizarPorFechaActivity.class);
                startActivity(i);
            }
        });

        binding.lyConductores.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sincronizarConductores();
            }
        });

        binding.lyReportarError.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                reportarError();
            }
        });

        if(new Fundo().cantidadNoSincronizados() > 0){
            binding.lyReportarError.setVisibility(View.VISIBLE);
        }else{
            binding.lyReportarError.setVisibility(View.GONE);
        }

        binding.btnRetroceder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SincronizadoActivity.this.finish();
            }
        });

        binding.btnInformacion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Funciones.mensaje("info", contexto, getString(R.string.mensaje_sincronizar_activity_info));
            }
        });
    }

    private void sincronizarConductores() {
        boolean ok = Funciones.mensajeConfirmacion("pregunta", contexto, getString(R.string.mensaje_sincronizar_activity_recordatorio));

        if(ok){
            asynPersonas = new CargarListaPersonas();
            if(asynPersonas.getStatus() == AsyncTask.Status.PENDING || asynPersonas.getStatus() == AsyncTask.Status.FINISHED){
                asynPersonas.execute();
            }
        }
    }

    private void sincronizarEntradas(){
        boolean ok = Funciones.mensajeConfirmacion("pregunta", contexto, getString(R.string.mensaje_sincronizar_activity_recordatorio));

        if(ok){
            asynFundos = new CargarListaFundos();
            if(asynFundos.getStatus() == AsyncTask.Status.PENDING || asynFundos.getStatus() == AsyncTask.Status.FINISHED){
                asynFundos.execute();
            }
        }
    }

    private void reportarError(){
        boolean ok = Funciones.mensajeConfirmacion("pregunta", contexto, getString(R.string.mensaje_sincronizar_activity_reportar_error));

        if(ok){
            new EnviarReporteSincronizacion().execute();
        }
    }

    /* Cargando conductores del servidor */

    public class CargarListaPersonas extends AsyncTask<Void, Void, Integer> {

        @Override
        protected void onPreExecute() {
            cargando = Funciones.cargando(contexto, "","sinc");
            Funciones.mensajito = getString(R.string.mensaje_comunicacion);
            super.onPreExecute();
        }

        @Override
        protected Integer doInBackground(Void... params) {
            Integer retorno = 0;
            try {
                String urlListado = Funciones.URL_WS + "persona.cargar.movil.controlador.php";
                HashMap parametros = new HashMap<String, String>();
                String resultado = new Funciones().getHttpContent(urlListado, parametros);
                cargarPersonas(resultado);
                retorno = 1; // Satisfactoriamente

            } catch (Exception e) {
                e.printStackTrace();
            }

            return retorno;
        }

        @Override
        protected void onPostExecute(Integer retorno) {
            if (retorno == 1) {
                if(cargando.isShowing()){
                    cargando.dismiss();
                }
                Funciones.snack(binding.SincronizadoActivity, contexto, "Genial, los conductores han sido sincronizados", "verde", "ok");
                binding.ltSincronizarConductor.setAnimation(R.raw.check);
                binding.ltSincronizarConductor.playAnimation();
                binding.ltSincronizarConductor.setRepeatCount(0);
            } else {
                Funciones.snack(binding.SincronizadoActivity, contexto, "Parece que hubo un error", "rojo", "ok");
            }
        }
    }

    private void cargarPersonas(String resultado) {
        try {
            if(resultado.equalsIgnoreCase("******-----****")){
                Funciones.snack(binding.SincronizadoActivity, contexto,getString(R.string.mensaje_sin_comunicacion), "rojo", "ok");
                return;
            }

            JSONObject json = new JSONObject(resultado);
            JSONArray jsonArrayPersonas = json.getJSONArray("mensaje");
            JSONArray jsonArraySancionados = json.getJSONArray("datos");

            int numeroPersonas = jsonArrayPersonas.length(), numeroSancionados = jsonArraySancionados.length(), progreso = 0, totalSincronizar = numeroPersonas+numeroSancionados, porcentaje = 0;
            Funciones.barraCargando.setProgress(0);
            Funciones.barraCargando.showContextMenu();
            Funciones.barraCargando.setMax(totalSincronizar);
            Funciones.mensajito = "Sincronizando conductores: 0%";

            sesion.unSetSincronizadoPersona();

            new Persona().eliminarPersonas();

            for (int i = 0; i < numeroPersonas; i++) {
                JSONObject jsonData = jsonArrayPersonas.getJSONObject(i);
                Persona item = new Persona();
                item.setDni(jsonData.getString("dni"));
                item.setNombre(jsonData.getString("nombre"));
                item.setPaterno(jsonData.getString("paterno"));
                item.setMaterno(jsonData.getString("materno"));
                item.setEstado(jsonData.getString("estado"));
                item.setSincronizado("S");
                item.setCodigo_off_line(0);
                item.agregarPersona(item);
                Funciones.barraCargando.setProgress(progreso++);
                porcentaje = 100 * progreso / totalSincronizar;
                Funciones.mensajito = "Sincronizando conductores: "+ porcentaje +"%";
            }

            for (int j = 0; j < numeroSancionados; j++){
                JSONObject data = jsonArraySancionados.getJSONObject(j);
                Sancionado san = new Sancionado();
                san.setDni(data.getString("dni"));
                san.setMotivo(data.getString("motivo"));
                san.setDesde(data.getString("desde"));
                san.setHasta(data.getString("hasta"));
                san.agregarSancionado(san);
                Funciones.barraCargando.setProgress(progreso++);
                porcentaje = 100 * progreso / totalSincronizar;
                Funciones.mensajito = "Sincronizando conductores: "+ porcentaje +"%";
            }

            sesion.setSincronizadoPersona();
        }catch (RuntimeException | JSONException e){
            Funciones.snack(binding.SincronizadoActivity, contexto,"error", "rojo", "ok");
        }
    }

    /* Cargando conductores del servidor */


    /* Cargando fundos del servidor */

    public class CargarListaFundos extends AsyncTask<Void, Void, Integer> {

        private JSONArray detalle;
        private int maximo_codigo;
        @Override
        protected void onPreExecute() {
            this.detalle = new Fundo().obtenerNoSincronizados();
            this.maximo_codigo = new Fundo().obtenerMaximoCodigoEntrada();
            cargando = Funciones.cargando(contexto, "","sinc");
            Funciones.mensajito = getString(R.string.mensaje_comunicacion);
            super.onPreExecute();
        }

        @Override
        protected Integer doInBackground(Void... params) {
            Integer retorno = 0;
            try {
                String urlListado = Funciones.URL_WS + "fundo.sincronizar.movil.php";
                HashMap parametros = new HashMap<String, String>();
                parametros.put("p_detalle", detalle.toString());
                parametros.put("p_codigo", String.valueOf(this.maximo_codigo));
                parametros.put("p_empresa",sesion.getCodigoEmpresa());
                parametros.put("p_lista_completa", "S");
                String resultado = new Funciones().getHttpContent(urlListado, parametros);
                cargarFundos(resultado);
                retorno = 1; // Satisfactoriamente
            } catch (Exception e) {
                e.printStackTrace();
            }
            return retorno;
        }

        @Override
        protected void onPostExecute(Integer retorno) {
            if (retorno == 1) {
                if(cargando.isShowing()){
                    cargando.dismiss();
                }
                Funciones.snack(binding.SincronizadoActivity, contexto, "Genial, entradas y salidas sincronizadas", "verde", "ok");
                binding.ltSincronizarEntradas.setAnimation(R.raw.check);
                binding.ltSincronizarEntradas.playAnimation();
                binding.ltSincronizarEntradas.setRepeatCount(0);
            } else {
                Funciones.snack(binding.SincronizadoActivity, contexto, "Parece que hubo un error", "rojo", "ok");
            }
        }
    }

    private void cargarFundos(String resultado) {
        try {
            if(resultado.equalsIgnoreCase("******-----****")){
                Funciones.snack(binding.SincronizadoActivity, contexto, getString(R.string.mensaje_sin_comunicacion), "azul", "ok");
                if(cargando.isShowing()){
                    cargando.dismiss();
                }
                return;
            }

            JSONObject json = new JSONObject(resultado);
            String mensaje = json.getString("mensaje");

            if(mensaje.equalsIgnoreCase("error")){
                String mensajeError = json.getString("datos");
                Funciones.mensajito = "";
                if(cargando.isShowing()){
                    cargando.dismiss();
                }
                Funciones.mensaje("error", contexto, mensajeError);
                return;
            }


            JSONArray jsonDatos = json.getJSONArray("datos");

            int numeroEntradas = jsonDatos.length(), progreso = 0, porcentaje = 0;
            Funciones.barraCargando.setProgress(0);
            Funciones.barraCargando.showContextMenu();
            Funciones.mensajito = "Sincronizando entradas y salidas: 0%";
            Funciones.barraCargando.setMax(numeroEntradas);

            sesion.unSetSincronizadoEntrada();

            Fundo fundo = new Fundo();
            fundo.eliminarDatosFundo();

            for (int i = 0; i < jsonDatos.length(); i++) {
                JSONObject data = jsonDatos.getJSONObject(i);


                Fundo item = new Fundo();
                String ocupantes = data.getString("numero_ocupantes");
                if(ocupantes.equalsIgnoreCase("") || ocupantes.isEmpty()){
                    ocupantes = "0";
                }
                item.setCodigo((data.getInt("codigo_entrada_fundo")));
                item.setFecha(data.getString("fecha_entrada"));
                item.setFecha_manual(data.getString("fecha_manual"));
                item.setHora(data.getString("hora_entrada"));
                item.setDni(data.getString("dni"));
                item.setNombre(data.getString("conductor"));
                item.setCodigo_categoria_personal((data.getInt("codigo_categoria_personal")));
                item.setNombre_empresa(data.getString("nombre_empresa"));
                item.setCodigo_tipo_vehiculo((data.getInt("codigo_tipo_vehiculo")));
                item.setPlaca_entrada(data.getString("placa"));
                item.setProcedencia(data.getString("procedencia_fundo"));
                item.setNum_asientos(Integer.parseInt(data.getString("numero_asientos")));
                item.setNum_ocupantes(Integer.parseInt(ocupantes));
                item.setMotivo(data.getString("motivo_fundo"));
                item.setContenedor(data.getString("contenedor"));
                item.setEntrada_salida(data.getString("entrada_salida_fundo"));
                item.setCodigo_empresa((data.getInt("codigo_empresa")));
                item.setCodigo_usuario((data.getInt("codigo_usuario")));
                item.setEstado_entrada(data.getString("estado_entrada_fundo"));
                item.setHora_manual(data.getString("hora_manual"));
                item.setCodigo_ruta(data.getInt("codigo_ruta"));
                item.setCodigo_entrada(data.getInt("codigo_ruta"));
                item.setSincronizado("S");
                item.setCodigo_off_line(data.getString("codigo_off"));
                item.setOrientacion(data.getString("orientacion"));
                item.setMano_obra(data.getString("mano_obra"));
                item.agregarFundo();

                JSONArray jsonVisitas = (JSONArray) data.get("visitas_lista");

                for (int j = 0; j < jsonVisitas.length(); j++){
                    JSONObject dataVisita = jsonVisitas.getJSONObject(j);
                    if(!dataVisita.getString("dni").equalsIgnoreCase("DNI no encontrado")){
                        Visita visita = new Visita();
                        visita.setCodigo_visita(dataVisita.getInt("codigo_visita_fundo"));
                        visita.setDni(dataVisita.getString("dni"));
                        visita.setCodigo_entrada(dataVisita.getInt("codigo_entrada_fundo"));
                        visita.setComentario(dataVisita.getString("comentario"));
                        visita.setSincronizado("S");
                        visita.setCodigo_off_line("");
                        visita.agregarVisita(visita);
                    }
                }

                JSONArray jsonManoObra = (JSONArray) data.get("manos_obra_lista");

                for (int j = 0; j < jsonManoObra.length(); j++){
                    JSONObject dataManosObra = jsonManoObra.getJSONObject(j);
                    ManoObraEntrada manoObra = new ManoObraEntrada();
                    manoObra.setCodigo_entrada(dataManosObra.getInt("codigo_entrada"));
                    manoObra.setCodigo_mano_obra(dataManosObra.getInt("codigo_mano_obra"));
                    manoObra.setNumero_trabajadores(dataManosObra.getInt("numero_trabajadores"));
                    manoObra.setCodigo_off_line(dataManosObra.getString("off_line"));
                    manoObra.setCodigo_off_line_fundo(dataManosObra.getString("off_line_fundo"));
                    manoObra.setSincronizado("S");
                    manoObra.agregarManoObraEntrada();
                }

                Funciones.barraCargando.setProgress(progreso++);
                porcentaje = 100 * progreso / numeroEntradas;
                Funciones.mensajito = "Sincronizando entradas y salidas: "+ porcentaje +"%";
            }
            sesion.setSincronizadoEntrada();
        }catch (Exception e){
        }
    }

    /* Cargando fundos del servidor */

    /* Reportar error en Sincronizar Datos */
    public class EnviarReporteSincronizacion extends AsyncTask<Void, Void, Integer> {
        JSONArray detalle = new Fundo().obtenerNoSincronizados();
        int codigo = new Fundo().obtenerMaximoCodigoEntrada();

        @Override
        protected void onPreExecute() {
            cargando = Funciones.cargando(contexto, "","sinc");
            Funciones.mensajito = getString(R.string.mensaje_comunicacion);
            super.onPreExecute();
        }

        @Override
        protected Integer doInBackground(Void... params) {
            Integer retorno = 0;
            try {
                String urlListado = Funciones.URL_WS + "fundo.sincronizar.movil.php";
                HashMap parametros = new HashMap<String, String>();
                parametros.put("p_detalle",this.detalle.toString());
                parametros.put("p_codigo",String.valueOf(this.codigo));
                parametros.put("p_reportar","R");
                parametros.put("p_empresa_nombre",sesion.getEmpresa());
                parametros.put("p_empresa",sesion.getCodigoEmpresa());
                parametros.put("p_usuario",sesion.getNombreUsuario());
                String resultado = new Funciones().getHttpContent(urlListado, parametros);
                cargarReporte(resultado, this.detalle);
                retorno = 1;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return retorno;
        }

        @Override
        protected void onPostExecute(Integer retorno) {
            if(new Fundo().cantidadNoSincronizados() > 0){
                binding.lyReportarError.setVisibility(View.VISIBLE);
            }else{
                binding.lyReportarError.setVisibility(View.GONE);
            }

            if (retorno == 1) {
                if(cargando.isShowing()){
                    cargando.dismiss();
                }
            } else {
                Funciones.snack(binding.SincronizadoActivity, contexto, "Parece que hubo un error", "rojo", "ok");
            }
        }
    }

    private void cargarReporte(String resultado, JSONArray detalle){
        try {
            if(resultado.equalsIgnoreCase("******-----****")){
                Funciones.snack(binding.SincronizadoActivity, contexto,getString(R.string.mensaje_sin_comunicacion), "rojo", "ok");
                return;
            }

            JSONObject json = new JSONObject(resultado);
            String mensaje = json.getString("mensaje");
            String datosValidacion = json.getString("datos");


            if(mensaje.equalsIgnoreCase("error")){
                String mensajeError = json.getString("datos");
                Funciones.mensajito = "";
                if(cargando.isShowing()){
                    cargando.dismiss();
                }
                Funciones.mensaje("error", contexto, mensajeError);
                return;
            }

            int numeroEntradas =0, progreso = 0, porcentaje = 0;
            if(datosValidacion.contains("[]")){
                Funciones.snack(binding.SincronizadoActivity, contexto, "Hemos enviado un correo con tus datos no sincronizados.", "azul", "ok");
                return;
            }else{
                JSONArray jsonDatos = json.getJSONArray("datos");
                numeroEntradas = jsonDatos.length();
                if(numeroEntradas>0){
                    Funciones.barraCargando.setProgress(0);
                    Funciones.barraCargando.showContextMenu();
                    Funciones.barraCargando.setMax(numeroEntradas);
                    Funciones.mensajito = "Sincronizando entradas y salidas: 0%";

                    for (int i = 0; i < numeroEntradas; i++) {
                        JSONObject data = jsonDatos.getJSONObject(i);
                        Fundo item = new Fundo();
                        String ocupantes = data.getString("numero_ocupantes");
                        if(ocupantes.equalsIgnoreCase("") || ocupantes.isEmpty()){
                            ocupantes = "0";
                        }
                        item.setCodigo((data.getInt("codigo_entrada_fundo")));
                        item.setFecha(data.getString("fecha_entrada"));
                        item.setFecha_manual(data.getString("fecha_manual"));
                        item.setHora(data.getString("hora_entrada"));
                        item.setDni(data.getString("dni"));
                        item.setNombre(data.getString("conductor"));
                        item.setCodigo_categoria_personal((data.getInt("codigo_categoria_personal")));
                        item.setNombre_empresa(data.getString("nombre_empresa"));
                        item.setCodigo_tipo_vehiculo((data.getInt("codigo_tipo_vehiculo")));
                        item.setPlaca_entrada(data.getString("placa"));
                        item.setProcedencia(data.getString("procedencia_fundo"));
                        item.setNum_asientos(Integer.parseInt(data.getString("numero_asientos")));
                        item.setNum_ocupantes(Integer.parseInt(ocupantes));
                        item.setMotivo(data.getString("motivo_fundo"));
                        item.setContenedor(data.getString("contenedor"));
                        item.setEntrada_salida(data.getString("entrada_salida_fundo"));
                        item.setCodigo_empresa((data.getInt("codigo_empresa")));
                        item.setCodigo_usuario((data.getInt("codigo_usuario")));
                        item.setEstado_entrada(data.getString("estado_entrada_fundo"));
                        item.setHora_manual(data.getString("hora_manual"));
                        item.setCodigo_ruta(data.getInt("codigo_ruta"));
                        item.setCodigo_entrada(data.getInt("codigo_ruta"));
                        item.setSincronizado("S");
                        item.setCodigo_off_line(data.getString("codigo_off"));
                        item.agregarFundo();

                        JSONArray jsonVisitas = (JSONArray) data.get("visitas_lista");

                        for (int j = 0; j < jsonVisitas.length(); j++){
                            JSONObject dataVisita = jsonVisitas.getJSONObject(j);
                            if(!dataVisita.getString("dni").equalsIgnoreCase("DNI no encontrado")){
                                Visita visita = new Visita();
                                visita.setCodigo_visita(dataVisita.getInt("codigo_visita_fundo"));
                                visita.setDni(dataVisita.getString("dni"));
                                visita.setCodigo_entrada(dataVisita.getInt("codigo_entrada_fundo"));
                                visita.setComentario(dataVisita.getString("comentario"));
                                visita.setSincronizado("S");
                                visita.setCodigo_off_line("");
                                visita.agregarVisita(visita);
                            }

                        }

                        Funciones.barraCargando.setProgress(progreso++);
                        porcentaje = 100 * progreso / numeroEntradas;
                        Funciones.mensajito = "Sincronizando entradas y salidas: "+ porcentaje +"%";
                    }

                }
            }

            Fundo fundo = new Fundo();
            fundo.eliminarNoSincronizados(detalle);

            Funciones.mensajito = "";
            if(cargando.isShowing()){
                cargando.dismiss();
            }

            if(numeroEntradas>0){
                Funciones.snack(binding.SincronizadoActivity, contexto, "Hemos podido sincronizar tus datos.", "verde", "ok");
                binding.ltSincronizarConductor.setAnimation(R.raw.check);
                binding.ltSincronizarConductor.playAnimation();
                binding.ltSincronizarConductor.setRepeatCount(0);
            }else{
                Funciones.snack(binding.SincronizadoActivity, contexto, "Hemos enviado un correo con tus datos no sincronizados.", "azul", "ok");
            }
        }catch (Exception e){
            if(cargando.isShowing()){
                cargando.dismiss();
            }
            Funciones.mensaje("error", contexto, e.getMessage());
        }
    }
    /* Reportar error en Sincronizar Datos */
}