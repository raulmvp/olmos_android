package pe.proolmos.proolmoscontrol;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import androidx.recyclerview.widget.RecyclerView;
import pe.proolmos.proolmoscontrol.Negocio.Fundo;

public class ListadoSalidaAdapter extends RecyclerView.Adapter<ListadoSalidaAdapter.ViewHolder> {

    private Context context;
    public static ArrayList<Fundo> listaDatos;
    View vista;

    public ListadoSalidaAdapter(Context context, ArrayList<Fundo> listaDatos) {
        this.context = context;
        this.listaDatos = listaDatos;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_salida, parent, false);
        vista = v;
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    public void setListaDatos(ArrayList<Fundo> mlistaDatos) {
        this.listaDatos = mlistaDatos;
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        final Fundo item = listaDatos.get(position);
        if(item.getSincronizado().equalsIgnoreCase("N")){
            holder.separator.setVisibility(View.VISIBLE);
            holder.lblEstadoSalida.setVisibility(View.VISIBLE);
        }
        holder.lblFecha.setText(item.getFecha_manual());
        holder.lblHora.setText(item.getHora_manual());
        holder.lblNombreConductor.setText(item.getNombre());
        String estado = MainActivity.obtenerEstadoConductor(item.getDni().trim());
        if(estado.equalsIgnoreCase("I")){
            estado = "INACTIVO";
            holder.lblEstado.setTextColor(context.getResources().getColor(R.color.salida));
        }else{
            estado = "ACTIVO";
            holder.lblEstado.setTextColor(context.getResources().getColor(R.color.colorPrimary));
        }
        holder.lblEstado.setText(estado);
        holder.lblDocumento.setText("DNI: "+item.getDni());
        if(item.getContenedor().equalsIgnoreCase("CONTENEDOR")){
            holder.lblContenedor.setText(item.getContenedor());
            holder.lblContenedor.setVisibility(View.VISIBLE);
        }
        holder.lblPlaca.setText("PLACA: "+item.getPlaca_entrada());
        holder.lblEmpresa.setText("EMPRESA: "+ item.getNombre_empresa());
    }

    @Override
    public int getItemCount() {
        return listaDatos.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView lblFecha, lblHora, lblNombreConductor, lblEstado, lblDocumento, lblContenedor, lblPlaca, lblEstadoSalida, lblEmpresa;
        View separator;


        public ViewHolder(View itemView) {
            super(itemView);
            lblFecha = (TextView) itemView.findViewById(R.id.lblFechaSalida);
            lblHora = (TextView) itemView.findViewById(R.id.lblHoraSalida);
            lblNombreConductor = (TextView) itemView.findViewById(R.id.lblConductorSalida);
            lblEstado = (TextView) itemView.findViewById(R.id.lblEstadoConductor);
            lblDocumento = (TextView) itemView.findViewById(R.id.lblDocumentoSalida);
            lblContenedor = (TextView) itemView.findViewById(R.id.lblContenedor);
            lblPlaca = (TextView) itemView.findViewById(R.id.lblPlacaSalida);
            lblEstadoSalida = (TextView) itemView.findViewById(R.id.lblEstadoSincronizacionSalida);
            lblEmpresa = (TextView) itemView.findViewById(R.id.lblEmpresa);
            separator = (View) itemView.findViewById(R.id.separatorSincronizacionSalida);
        }
    }

}
