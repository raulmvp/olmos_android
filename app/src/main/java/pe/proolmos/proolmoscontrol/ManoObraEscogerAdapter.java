package pe.proolmos.proolmoscontrol;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RadioButton;

import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import pe.proolmos.proolmoscontrol.Negocio.ManoObra;

public class ManoObraEscogerAdapter extends RecyclerView.Adapter<ManoObraEscogerAdapter.ViewHolder> {

    private Context context;
    public static ArrayList<ManoObra> listaDatos;
    View vista;
    private int posicionultimaSeleccion = -1;

    public ManoObraEscogerAdapter(Context context, ArrayList<ManoObra> listaDatos) {
        this.context = context;
        this.listaDatos = listaDatos;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_mano_obra_escoger, parent, false);
        vista = v;
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    public void setListaDatos(ArrayList<ManoObra> mlistaDatos) {
        this.listaDatos = mlistaDatos;
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        final ManoObra item = listaDatos.get(position);
        if(item.getEstado().equalsIgnoreCase("A")){
            holder.chbElegirManoObra.setText(item.getNombre());
            holder.chbElegirManoObra.setChecked(position == posicionultimaSeleccion);
            if(holder.chbElegirManoObra.isChecked()){
                item.setElegido("S");
            }
        }
    }

    @Override
    public int getItemCount() {
        return listaDatos.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        RadioButton chbElegirManoObra;

        public ViewHolder(View itemView) {
            super(itemView);
            chbElegirManoObra = (RadioButton) itemView.findViewById(R.id.chbElegirManoObra);
            chbElegirManoObra.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    posicionultimaSeleccion = getAdapterPosition();

                    for (int i= 0; i < listaDatos.size(); i++) {
                        ManoObra item = listaDatos.get(i);
                        item.setElegido("N");
                    }
                    listaDatos.get(posicionultimaSeleccion).setElegido("S");
                    notifyDataSetChanged();
                }
            });
        }
    }

}
