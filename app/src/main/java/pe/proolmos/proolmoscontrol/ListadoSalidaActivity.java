package pe.proolmos.proolmoscontrol;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import pe.proolmos.proolmoscontrol.Negocio.Fundo;
import pe.proolmos.proolmoscontrol.Negocio.ManoObraEntrada;
import pe.proolmos.proolmoscontrol.Negocio.Visita;
import pe.proolmos.proolmoscontrol.Sesion.SesionPreferences;
import pe.proolmos.proolmoscontrol.datos.AccesoDatos;
import pe.proolmos.proolmoscontrol.util.Funciones;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.res.Configuration;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class ListadoSalidaActivity extends AppCompatActivity {
    CargarDatosTabla asyncTabla;
    SincronizarDatos asyncSincronizar;
    CargarListaFundos asyncCargarListaFundos;

    SesionPreferences sesion;
    Context contexto;
    View fragmentoActual, linearNoData, linearFechaRango, linearFiltro, btnRetroceder;;
    public static Dialog cargando;
    public static ArrayList<Fundo> listaDatos;
    ListadoSalidaAdapter adaptador;
    RecyclerView recyclerView;
    SwipeRefreshLayout swipeContenedor;


    TextView lblTitulo, txtFechaInicio, txtFechaInicioElegida;
    EditText txtBuscar;
    RadioGroup rgFiltro;
    RadioButton rbHoy, rbRango;

    FloatingActionButton fbSincronizar;

    int orientation, codigo_max;
    JSONArray detalle, detalle_mano_obra, detalle_visitas;;
    @SuppressLint("RestrictedApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_consulta_salida);
        ActionBar actionBar = getSupportActionBar();
        actionBar.hide();
        Window window = getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(getResources().getColor(R.color.consultaSalidaDark));

        Funciones.habilitarDirectivasInternet();
        contexto = this;
        sesion = new SesionPreferences(contexto);
        AccesoDatos.aplicacion = this;

        fragmentoActual = findViewById(R.id.linearFragmentoListadoSalida);
        recyclerView = (RecyclerView) findViewById(R.id.rvListadoSalida);
        lblTitulo = (TextView) findViewById(R.id.lblTituloListadoSalida);
        txtBuscar = (EditText) findViewById(R.id.txtBuscar);
        linearNoData = (View) findViewById(R.id.linearNoData);
        rgFiltro = (RadioGroup) findViewById(R.id.rgFiltro);
        rbHoy = (RadioButton) findViewById(R.id.rbHoy);
        rbRango = (RadioButton) findViewById(R.id.rbRango);
        txtFechaInicio = (TextView) findViewById(R.id.txtFechaInicio);
        txtFechaInicioElegida = (TextView) findViewById(R.id.txtFechaInicioElegida);
        linearFechaRango = (View) findViewById(R.id.linearFechaRango);
        swipeContenedor = (SwipeRefreshLayout) findViewById(R.id.swlListadoSalida);
        linearFiltro = (View) findViewById(R.id.linearFiltrosSalida);
        fbSincronizar = (FloatingActionButton) findViewById(R.id.fabSincronizarEntrada);
        btnRetroceder = (View) findViewById(R.id.btnRetroceder);

        btnRetroceder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ListadoSalidaActivity.this.finish();
            }
        });

        fbSincronizar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cargando = Funciones.cargando(contexto, "","sinc");
                Fundo fundo = new Fundo();
                detalle = fundo.obtenerNoSincronizados();
                codigo_max = fundo.obtenerMaximoCodigoEntrada();

                detalle_mano_obra = new ManoObraEntrada().obtenerNoSincronizados();
                detalle_visitas = new Visita().obtenerNoSincronizados();

                asyncSincronizar = new SincronizarDatos(detalle.toString(), String.valueOf(codigo_max), detalle_mano_obra.toString(), detalle_visitas.toString());
                sincronizarData(detalle, codigo_max, detalle_mano_obra, detalle_visitas);
            }
        });

        int tipoConexion = Funciones.obtenerTipoConexion(contexto);

        if(tipoConexion > 0){
            fbSincronizar.setVisibility(View.VISIBLE);
        }

        orientation = contexto.getResources().getConfiguration().orientation;

        if (orientation == Configuration.ORIENTATION_LANDSCAPE) {
            lblTitulo.setCompoundDrawablesWithIntrinsicBounds(null,null,contexto.getDrawable(R.drawable.ic_abajo),null);
            linearFiltro.setVisibility(View.GONE);
        } else {
            lblTitulo.setCompoundDrawablesWithIntrinsicBounds(null,null,contexto.getDrawable(R.drawable.ic_arriba),null);
            linearFiltro.setVisibility(View.VISIBLE);
        }

        lblTitulo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(linearFiltro.getVisibility() == View.VISIBLE){
                    linearFiltro.setVisibility(View.GONE);
                    lblTitulo.setCompoundDrawablesWithIntrinsicBounds(null,null,contexto.getDrawable(R.drawable.ic_abajo),null);
                }else{
                    linearFiltro.setVisibility(View.VISIBLE);
                    lblTitulo.setCompoundDrawablesWithIntrinsicBounds(null,null,contexto.getDrawable(R.drawable.ic_arriba),null);
                }
            }
        });

        listaDatos = new ArrayList<Fundo>();
        recyclerView.setLayoutManager(new LinearLayoutManager(contexto));
        adaptador = new ListadoSalidaAdapter(contexto, listaDatos);
        recyclerView.setAdapter(adaptador);

        asyncTabla = new CargarDatosTabla();

        swipeContenedor.setColorSchemeResources(R.color.colorPrimary, R.color.colorAccent, R.color.consultaSalida, R.color.registroEntrada);

        swipeContenedor.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                descargarDatosEntrada();
            }
        });

        rgFiltro.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if(checkedId == rbHoy.getId()){
                    txtFechaInicio.setText(Funciones.obtenerFechaActual("dd/MM/yyyy"));
                    linearFechaRango.setVisibility(View.GONE);
                    descargarDatosEntrada();
                }else{
                    linearFechaRango.setVisibility(View.VISIBLE);
                    txtFechaInicio.setText("");
                    boolean fecha_inicio = Funciones.calendario(contexto, txtFechaInicio, txtFechaInicioElegida, txtFechaInicio.getText().toString());
                    if(fecha_inicio){
                        descargarDatosEntrada();
                    }
                }
            }

        });

        rbHoy.setChecked(true);
        txtFechaInicio.setText(Funciones.obtenerFechaActual("dd/MM/yyyy"));

        txtFechaInicio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Funciones.calendario(contexto, txtFechaInicio, txtFechaInicioElegida, txtFechaInicio.getText().toString());
                descargarDatosEntrada();
            }
        });

        txtBuscar.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable et) {
                String s = et.toString();
                if(!s.equals(s.toUpperCase()))
                {
                    s=s.toUpperCase();
                    txtBuscar.setText(s);
                    txtBuscar.setSelection(txtBuscar.length()); //fix reverse texting
                }
                if(s.trim().length() >= 2){
                    filtrarDatos(listaDatos, s);
                }else{
                    validar(listaDatos);
                }
            }
        });
    }

    private void datosSalidaSqlite(){
        swipeContenedor.setRefreshing(true);
        if(asyncTabla.getStatus() == AsyncTask.Status.PENDING){
            asyncTabla.execute();
        }

        if(asyncTabla.getStatus() == AsyncTask.Status.FINISHED){
            asyncTabla = new CargarDatosTabla();
            asyncTabla.execute();
        }
    }

    private void descargarDatosEntrada(){
        swipeContenedor.setRefreshing(true);
        JSONArray cargar_det_fund  = new Fundo().obtenerNoSincronizados(),
                cargar_det_man_ob = new ManoObraEntrada().obtenerNoSincronizados(),
                cargar_det_vis = new Visita().obtenerNoSincronizados();
        String fecha_seleccionada = txtFechaInicioElegida.getText().toString(),
                fecha_sqlite = txtFechaInicio.getText().toString();
        asyncCargarListaFundos = new CargarListaFundos(cargar_det_fund, cargar_det_man_ob, cargar_det_vis, fecha_seleccionada, fecha_sqlite);
        if(asyncCargarListaFundos.getStatus() == AsyncTask.Status.PENDING){
            asyncCargarListaFundos.execute();
        }

        if(asyncCargarListaFundos.getStatus() == AsyncTask.Status.FINISHED){
            asyncCargarListaFundos = new CargarListaFundos(cargar_det_fund, cargar_det_man_ob, cargar_det_vis, fecha_seleccionada, fecha_sqlite);
            asyncCargarListaFundos.execute();
        }
    }

    private void sincronizarData(JSONArray p_detalle, int p_codigo_max, JSONArray p_detalle_mano_obra, JSONArray p_detalle_visitas){
        //swipeContenedor.setRefreshing(true);

        if(asyncSincronizar.getStatus() == AsyncTask.Status.PENDING){
            asyncSincronizar.execute();
        }

        if(asyncSincronizar.getStatus() == AsyncTask.Status.FINISHED){
            asyncSincronizar = new SincronizarDatos(p_detalle.toString(), String.valueOf(p_codigo_max), p_detalle_mano_obra.toString(), p_detalle_visitas.toString());
            asyncSincronizar.execute();
        }
    }

    /* Sincronizar Datos */
    public class SincronizarDatos extends AsyncTask<Void, Void, String> {
        private String detalleString, codigo, detalleManoObraString, detalleVisitasString;

        public SincronizarDatos(String detalleString, String codigo, String detalleManoObraString, String detalleVisitasString) {
            this.detalleString = detalleString;
            this.codigo = codigo;
            this.detalleManoObraString = detalleManoObraString;
            this.detalleVisitasString = detalleVisitasString;
        }

        @Override
        protected void onPreExecute() {
            Funciones.mensajito = "Estableciendo conexión segura con el servidor...";
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(Void... params) {
            String resultado = "";
            try {
                String urlListado = Funciones.URL_WS + "fundo.sincronizar.movil.php";
                HashMap parametros = new HashMap<String, String>();
                parametros.put("p_detalle",this.detalleString);
                parametros.put("p_detalle_mano_obra",this.detalleManoObraString);
                parametros.put("p_detalle_visita",this.detalleVisitasString);
                parametros.put("p_codigo",this.codigo);
                parametros.put("p_empresa",sesion.getCodigoEmpresa());
                parametros.put("p_empresa_nombre",sesion.getEmpresa());
                parametros.put("p_usuario",sesion.getNombreUsuario());
                resultado = new Funciones().getHttpContent(urlListado, parametros);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return resultado;
        }

        @Override
        protected void onPostExecute(String resultado) {
            try {
                if(resultado.equalsIgnoreCase("******-----****")){
                    Funciones.snack(fragmentoActual, contexto, "No hay conexión con nuestro servidor", "azul", "");
                    if(cargando.isShowing()){
                        cargando.dismiss();
                    }
                    return;
                }
                boolean mensajeConfirmacion;
                JSONObject json = new JSONObject(resultado);
                String mensaje = json.getString("mensaje");

                if(mensaje.equalsIgnoreCase("error")){
                    String mensajeError = json.getString("datos");
                    Funciones.mensajito = "";
                    if(cargando.isShowing()){
                        cargando.dismiss();
                    }
                    Funciones.mensaje("error", contexto, mensajeError);
                    return;
                }

                JSONArray jsonDatos = json.getJSONArray("datos");
                int numeroEntradas = jsonDatos.length(), progreso = 0, porcentaje = 0;
                Funciones.barraCargando.setProgress(0);
                Funciones.barraCargando.showContextMenu();
                Funciones.barraCargando.setMax(numeroEntradas);
                Funciones.mensajito = "Sincronizando datos: 0%";

                for (int i = 0; i < numeroEntradas; i++) {
                    JSONObject data = jsonDatos.getJSONObject(i);
                    Fundo item = new Fundo();
                    String ocupantes = data.getString("numero_ocupantes");
                    if(ocupantes.equalsIgnoreCase("") || ocupantes.isEmpty()){
                        ocupantes = "0";
                    }
                    item.setCodigo((data.getInt("codigo_entrada_fundo")));
                    item.setFecha(data.getString("fecha_entrada"));
                    item.setFecha_manual(data.getString("fecha_manual"));
                    item.setHora(data.getString("hora_entrada"));
                    item.setDni(data.getString("dni"));
                    item.setNombre(data.getString("conductor"));
                    item.setCodigo_categoria_personal((data.getInt("codigo_categoria_personal")));
                    item.setNombre_empresa(data.getString("nombre_empresa"));
                    item.setCodigo_tipo_vehiculo((data.getInt("codigo_tipo_vehiculo")));
                    item.setPlaca_entrada(data.getString("placa"));
                    item.setProcedencia(data.getString("procedencia_fundo"));
                    item.setNum_asientos(Integer.parseInt(data.getString("numero_asientos")));
                    item.setNum_ocupantes(Integer.parseInt(ocupantes));
                    item.setMotivo(data.getString("motivo_fundo"));
                    item.setContenedor(data.getString("contenedor"));
                    item.setEntrada_salida(data.getString("entrada_salida_fundo"));
                    item.setCodigo_empresa((data.getInt("codigo_empresa")));
                    item.setCodigo_usuario((data.getInt("codigo_usuario")));
                    item.setEstado_entrada(data.getString("estado_entrada_fundo"));
                    item.setHora_manual(data.getString("hora_manual"));
                    item.setCodigo_ruta(data.getInt("codigo_ruta"));
                    item.setCodigo_entrada(data.getInt("codigo_ruta"));
                    item.setSincronizado("S");
                    item.setCodigo_off_line(data.getString("codigo_off"));
                    item.setMano_obra(data.getString("mano_obra"));
                    item.agregarFundo();

                    JSONArray jsonVisitas = (JSONArray) data.get("visitas_lista");

                    for (int j = 0; j < jsonVisitas.length(); j++){
                        JSONObject dataVisita = jsonVisitas.getJSONObject(j);
                        if(!dataVisita.getString("dni").equalsIgnoreCase("DNI no encontrado")){
                            Visita visita = new Visita();
                            visita.setCodigo_visita(dataVisita.getInt("codigo_visita_fundo"));
                            visita.setDni(dataVisita.getString("dni"));
                            visita.setCodigo_entrada(dataVisita.getInt("codigo_entrada_fundo"));
                            visita.setComentario(dataVisita.getString("comentario"));
                            visita.setSincronizado("S");
                            visita.setCodigo_off_line("");
                            visita.agregarVisita(visita);
                        }
                    }

                    JSONArray jsonManoObra = (JSONArray) data.get("manos_obra_lista");

                    for (int j = 0; j < jsonManoObra.length(); j++){
                        JSONObject dataManosObra = jsonManoObra.getJSONObject(j);
                        ManoObraEntrada manoObra = new ManoObraEntrada();
                        manoObra.setCodigo_entrada(dataManosObra.getInt("codigo_entrada"));
                        manoObra.setCodigo_mano_obra(dataManosObra.getInt("codigo_mano_obra"));
                        manoObra.setNumero_trabajadores(dataManosObra.getInt("numero_trabajadores"));
                        manoObra.setCodigo_off_line(dataManosObra.getString("off_line"));
                        manoObra.setCodigo_off_line_fundo(dataManosObra.getString("off_line_fundo"));
                        manoObra.setSincronizado("S");
                        manoObra.agregarManoObraEntrada();
                    }

                    Funciones.barraCargando.setProgress(progreso++);
                    porcentaje = 100 * progreso / numeroEntradas;
                    Funciones.mensajito = "Sincronizando datos: "+ porcentaje +"%";
                }

                Fundo fundo = new Fundo();
                fundo.eliminarNoSincronizados(detalle);
                new ManoObraEntrada().eliminarNoSincronizados(detalle_mano_obra);
                new Visita().eliminarNoSincronizados(detalle_visitas);
                Funciones.mensajito = "";
                if(cargando.isShowing()){
                    cargando.dismiss();
                }

                int cantidadNoSincronizados = fundo.cantidadNoSincronizados();
                if(cantidadNoSincronizados > 0){ //seguir sincronizando datos
                    fbSincronizar.performClick();
                }else{
                    mensajeConfirmacion = Funciones.mensaje("exito",contexto,"Datos actualizados");
                    if(mensajeConfirmacion){
                        datosSalidaSqlite();
                    }
                }
            }catch (Exception e){
                if(cargando.isShowing()){
                    cargando.dismiss();
                }
                Funciones.mensaje("error", contexto, e.getMessage());
            }
        }
    }
    /* Sincronizar Datos */

    /** DECARGAR DATA DEL SERVIDOR**/
    public class CargarListaFundos extends AsyncTask<Void, Void, Integer> {

        private JSONArray cargar_det_fund, cargar_det_man_ob, cargar_det_vis;
        private String fecha_seleccionada, fecha_sqlite;

        public CargarListaFundos(JSONArray cargar_det_fund, JSONArray cargar_det_man_ob, JSONArray cargar_det_vis, String fecha_seleccionada, String fecha_sqlite) {
            this.cargar_det_fund = cargar_det_fund;
            this.cargar_det_man_ob = cargar_det_man_ob;
            this.cargar_det_vis = cargar_det_vis;
            this.fecha_seleccionada = fecha_seleccionada;
            this.fecha_sqlite = fecha_sqlite;
        }

        @Override
        protected void onPreExecute() {
            cargando = Funciones.cargando(contexto, getString(R.string.mensaje_comunicacion),"sinc");
            super.onPreExecute();
        }

        @Override
        protected Integer doInBackground(Void... params) {
            Integer retorno = 0;
            try {
                String urlListado = Funciones.URL_WS + "fundo.cargar.fecha.movil.controlador.php";
                HashMap parametros = new HashMap<String, String>();
                parametros.put("codigo_empresa",sesion.getCodigoEmpresa());
                parametros.put("detalleFundos",this.cargar_det_fund.toString());
                parametros.put("detalleManoObra",this.cargar_det_man_ob.toString());
                parametros.put("detalleVisitas",this.cargar_det_vis.toString());
                parametros.put("fecha_seleccionada",this.fecha_seleccionada);
                String resultado = new Funciones().getHttpContent(urlListado, parametros);
                retorno = cargarFundos(resultado, this.cargar_det_fund, this.cargar_det_man_ob, this.cargar_det_vis, this.fecha_seleccionada, this.fecha_sqlite); // Satisfactoriamente
            } catch (Exception e) {
                Log.e("crash", "doInBackground#->"+e.getLocalizedMessage());
            }
            return retorno;
        }

        @Override
        protected void onPostExecute(Integer retorno) {
            swipeContenedor.setRefreshing(true);
            if(cargando.isShowing()){
                cargando.dismiss();
            }
            switch (retorno){
                case -1:
                    Funciones.snack(fragmentoActual, contexto, "Parece que hubo un error, inténtalo en un momento.", "rojo", "ok");
                    break;
                case 0:
                    Funciones.snack(fragmentoActual, contexto, "No hay conexión con nuestro servidor", "azul", "");
                    break;
                case 1:
                    Funciones.snack(fragmentoActual, contexto, "Datos actualizados", "verde", "ok");
                    break;
            }
            datosSalidaSqlite();
        }
    }

    private Integer cargarFundos(String resultado, JSONArray cargar_det_fund, JSONArray cargar_det_man_ob, JSONArray cargar_det_vis, String fecha, String fecha_sqlite) {
        try {
            if(resultado.equalsIgnoreCase("******-----****")){
                if(cargando.isShowing()){
                    cargando.dismiss();
                }
                return 0;
            }
            JSONObject json = new JSONObject(resultado);
            JSONArray jsonDatos = json.getJSONArray("datos");

            //new Fundo().eliminarHoy(fecha_sqlite);
            new Fundo().eliminarHoy(fecha);
            int numeroEntradas = jsonDatos.length(), progreso = 0, porcentaje = 0;
            Funciones.barraCargando.setProgress(0);
            Funciones.barraCargando.showContextMenu();
            Funciones.barraCargando.setMax(numeroEntradas);
            Funciones.mensajito = "Sincronizando entradas y salidas: 0%";

            for (int i = 0; i < numeroEntradas; i++) {
                JSONObject data = jsonDatos.getJSONObject(i);
                Fundo item = new Fundo();
                String ocupantes = data.getString("numero_ocupantes");
                if(ocupantes.equalsIgnoreCase("") || ocupantes.isEmpty()){
                    ocupantes = "0";
                }
                item.setCodigo((data.getInt("codigo_entrada_fundo")));
                item.setFecha(data.getString("fecha_entrada"));
                item.setFecha_manual(data.getString("fecha_manual"));
                item.setHora(data.getString("hora_entrada"));
                item.setDni(data.getString("dni"));
                item.setNombre(data.getString("conductor"));
                item.setCodigo_categoria_personal((data.getInt("codigo_categoria_personal")));
                item.setNombre_empresa(data.getString("nombre_empresa"));
                item.setCodigo_tipo_vehiculo((data.getInt("codigo_tipo_vehiculo")));
                item.setPlaca_entrada(data.getString("placa"));
                item.setProcedencia(data.getString("procedencia_fundo"));
                String asientos = data.getString("numero_asientos");
                if(asientos.isEmpty() || asientos.equalsIgnoreCase("")){
                    asientos = "0";
                }
                item.setNum_asientos(Integer.parseInt(asientos));
                item.setNum_ocupantes(Integer.parseInt(ocupantes));
                item.setMotivo(data.getString("motivo_fundo"));
                item.setContenedor(data.getString("contenedor"));
                item.setEntrada_salida(data.getString("entrada_salida_fundo"));
                item.setCodigo_empresa((data.getInt("codigo_empresa")));
                item.setCodigo_usuario((data.getInt("codigo_usuario")));
                item.setEstado_entrada(data.getString("estado_entrada_fundo"));
                item.setHora_manual(data.getString("hora_manual"));
                item.setCodigo_ruta(data.getInt("codigo_ruta"));
                item.setCodigo_entrada(data.getInt("codigo_ruta"));
                item.setSincronizado("S");
                item.setCodigo_off_line(data.getString("codigo_off"));
                item.setOrientacion(data.getString("orientacion"));
                item.setMano_obra(data.getString("mano_obra"));
                item.agregarFundo();

                JSONArray jsonVisitas = (JSONArray) data.get("visitas_lista");

                for (int j = 0; j < jsonVisitas.length(); j++){
                    JSONObject dataVisita = jsonVisitas.getJSONObject(j);
                    if(!dataVisita.getString("dni").equalsIgnoreCase("DNI no encontrado")){
                        Visita visita = new Visita();
                        visita.setCodigo_visita(dataVisita.getInt("codigo_visita_fundo"));
                        visita.setDni(dataVisita.getString("dni"));
                        visita.setCodigo_entrada(dataVisita.getInt("codigo_entrada_fundo"));
                        visita.setComentario(dataVisita.getString("comentario"));
                        visita.setSincronizado("S");
                        visita.setCodigo_off_line("");
                        visita.agregarVisita(visita);
                    }
                }

                JSONArray jsonManoObra = (JSONArray) data.get("manos_obra_lista");

                for (int j = 0; j < jsonManoObra.length(); j++){
                    JSONObject dataManosObra = jsonManoObra.getJSONObject(j);
                    ManoObraEntrada manoObra = new ManoObraEntrada();
                    manoObra.setCodigo_entrada(dataManosObra.getInt("codigo_entrada"));
                    manoObra.setCodigo_mano_obra(dataManosObra.getInt("codigo_mano_obra"));
                    manoObra.setNumero_trabajadores(dataManosObra.getInt("numero_trabajadores"));
                    manoObra.setCodigo_off_line(dataManosObra.getString("off_line"));
                    manoObra.setCodigo_off_line_fundo(dataManosObra.getString("off_line_fundo"));
                    manoObra.setSincronizado("S");
                    manoObra.agregarManoObraEntrada();
                }

                Funciones.barraCargando.setProgress(progreso++);
                porcentaje = 100 * progreso / numeroEntradas;
                Funciones.mensajito = "Sincronizando entradas y salidas: "+ porcentaje +"%";
            }
            sesion.setSincronizadoEntrada();
            sesion.setSincronizadoVisita();

            new Fundo().eliminarNoSincronizados(cargar_det_fund);
            new Visita().eliminarNoSincronizados(cargar_det_vis);
            new ManoObraEntrada().eliminarNoSincronizados(cargar_det_man_ob);
            Funciones.mensajito = "";
            return 1;
        }catch (RuntimeException | JSONException e){
            Log.e("Error cargarFundos" , e.getMessage());
            return -1;
        }
    }
    /** DECARGAR DATA DEL SERVIDOR**/

    /* Cargar datos de la tabla */
    public class CargarDatosTabla extends AsyncTask<Void, Void, ArrayList<Fundo>>{
        @SuppressLint("LongLogTag")
        @Override
        protected void onPostExecute(ArrayList<Fundo> fundos) {
            try{
                swipeContenedor.setRefreshing(false);
                if(txtBuscar.getText().toString().trim().length() >= 2){
                    filtrarDatos(listaDatos, txtBuscar.getText().toString());
                }else{
                    validar(listaDatos);
                }
            }catch (RuntimeException e){
            }
            super.onPostExecute(fundos);
        }


        @Override
        protected ArrayList<Fundo> doInBackground(Void... voids) {
            String tipo = "H"; //hoy por defecto
            String fechaInicio = txtFechaInicio.getText().toString();

            if(rbRango.isChecked()){ //si se selecciona el radio button rango cambia el tipo
                tipo = "R";
                fechaInicio = txtFechaInicioElegida.getText().toString();
            }
            Fundo obj = new Fundo();
            ArrayList<Fundo> lista = new ArrayList<Fundo>();
            lista = obj.cargarEntradasFundo(lista, "S", tipo, fechaInicio);
            listaDatos.clear();
            listaDatos = lista;
            return lista;
        }
    }
    /* Cargar datos de la tabla */

    private void validar(ArrayList<Fundo> listaDatos){
        int size = listaDatos.size();

        if(size == 0){
            linearNoData.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
            return;
        }

        linearNoData.setVisibility(View.GONE);
        recyclerView.setVisibility(View.VISIBLE);
        recyclerView.setAdapter(adaptador);
        adaptador.setListaDatos(listaDatos);
    }

    private void filtrarDatos(ArrayList<Fundo> listaDatos, String filtro){
        ArrayList<Fundo> listaFiltrada = new ArrayList<Fundo>();
        int size = listaDatos.size();
        for(int i=0; i < size; i++){
            Fundo item = listaDatos.get(i);
            if(item.getNombre().contains(filtro) || item.getDni().contains(filtro) || item.getPlaca_entrada().contains(filtro)){
                listaFiltrada.add(item);
            }
        }
        validar(listaFiltrada);
    }
}
