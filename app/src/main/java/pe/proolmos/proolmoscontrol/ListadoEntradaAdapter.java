package pe.proolmos.proolmoscontrol;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.ArrayList;
import androidx.recyclerview.widget.RecyclerView;
import pe.proolmos.proolmoscontrol.Negocio.Fundo;
import pe.proolmos.proolmoscontrol.util.Funciones;

public class ListadoEntradaAdapter extends RecyclerView.Adapter<ListadoEntradaAdapter.ViewHolder> {

    private Context context;
    public static ArrayList<Fundo> listaDatos;
    View vista;

    public ListadoEntradaAdapter(Context context, ArrayList<Fundo> listaDatos) {
        this.context = context;
        this.listaDatos = listaDatos;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_entrada, parent, false);
        vista = v;
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    public void setListaDatos(ArrayList<Fundo> mlistaDatos) {
        this.listaDatos = mlistaDatos;
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        final Fundo item = listaDatos.get(position);
        if(item.getSincronizado().equalsIgnoreCase("N")){
            holder.separatorEstado.setVisibility(View.VISIBLE);
            holder.lblEstadoSincronizacion.setVisibility(View.VISIBLE);
        }
        holder.lblFecha.setText(item.getFecha_manual());
        holder.lblHora.setText(item.getHora_manual());
        holder.lblNombreConductor.setText(item.getNombre());
        String estado = MainActivity.obtenerEstadoConductor(item.getDni().trim());
        if(estado.equalsIgnoreCase("I")){
            estado = "INACTIVO";
            holder.lblEstado.setTextColor(context.getResources().getColor(R.color.salida));
        }else{
            estado = "ACTIVO";
            holder.lblEstado.setTextColor(context.getResources().getColor(R.color.colorPrimary));
        }
        holder.lblEstado.setText(estado);
        holder.lblDocumento.setText(item.getDni());
        holder.lblTipoVehiculo.setText(MainActivity.obtenerTipoVehiculp(item.getCodigo_tipo_vehiculo()));
        holder.lblPlaca.setText(item.getPlaca_entrada());
        holder.lblEmpresa.setText(item.getNombre_empresa());
        holder.lblTipoPersonal.setText(MainActivity.obtenerTipoPersonal(item.getCodigo_categoria_personal()));
        holder.lblNumeroOcupantes.setText(String.valueOf(item.getNum_ocupantes()));
        Log.e("numero_ocupantes", ""+item.getNum_ocupantes());
    }

    @Override
    public int getItemCount() {
        return listaDatos.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView lblFecha, lblHora, lblNombreConductor, lblEstado, lblDocumento, lblTipoVehiculo, lblEstadoSincronizacion, lblPlaca, lblEmpresa, lblTipoPersonal, lblNumeroOcupantes;
        View separatorEstado;

        public ViewHolder(View itemView) {
            super(itemView);
            lblFecha = (TextView) itemView.findViewById(R.id.lblFechaEntrada);
            lblHora = (TextView) itemView.findViewById(R.id.lblHoraEntrada);
            lblNombreConductor = (TextView) itemView.findViewById(R.id.lblConductorEntrada);
            lblEstado = (TextView) itemView.findViewById(R.id.lblEstadoConductor);
            lblDocumento = (TextView) itemView.findViewById(R.id.lblDocumentoEntrada);
            lblTipoVehiculo = (TextView) itemView.findViewById(R.id.lblTipoVehiculoEntrada);
            lblEmpresa = (TextView) itemView.findViewById(R.id.lblEmpresa);
            lblPlaca = (TextView) itemView.findViewById(R.id.lblPlacaEntrada);
            lblTipoPersonal = (TextView) itemView.findViewById(R.id.lblTipoPersonal);
            lblNumeroOcupantes = (TextView) itemView.findViewById(R.id.txtNumeroOcupantes);
            lblEstadoSincronizacion = (TextView) itemView.findViewById(R.id.lblEstadoSincronizacion);
            separatorEstado = (View) itemView.findViewById(R.id.separatorSincronizacion);
        }
    }

}
