package pe.proolmos.proolmoscontrol;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;
import pe.proolmos.proolmoscontrol.Negocio.Empresa;
import pe.proolmos.proolmoscontrol.Negocio.Fundo;
import pe.proolmos.proolmoscontrol.Negocio.ManoObra;
import pe.proolmos.proolmoscontrol.Negocio.ManoObraEntrada;
import pe.proolmos.proolmoscontrol.Negocio.Persona;
import pe.proolmos.proolmoscontrol.Negocio.Sancionado;
import pe.proolmos.proolmoscontrol.Negocio.Visita;
import pe.proolmos.proolmoscontrol.Sesion.SesionPreferences;
import pe.proolmos.proolmoscontrol.datos.AccesoDatos;
import pe.proolmos.proolmoscontrol.util.Funciones;

import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.airbnb.lottie.LottieAnimationView;
import com.airbnb.lottie.LottieDrawable;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class OnBoardingActivity extends AppCompatActivity {
    private SesionPreferences sesion;

    Context contexto;
    Persona persona;
    Sancionado sancionado;
    Fundo fundo;
    Visita visitaGeneral;


    Animation arribaAbajo,derechaIzquierda,abajoArriba, fadeIn;

    LottieAnimationView ltImagen;
    TextView txtDescripcion, lblTitulo;
    Button btnSincronizar, btnSiguiente;

    public static Dialog cargando;
    CargarListaPersonas asynPersonas;
    CargarListaFundos asynFundos;
    @SuppressLint("SourceLockedOrientationActivity")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_on_boarding);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        ActionBar actionBar = getSupportActionBar();
        actionBar.hide();

        Funciones.habilitarDirectivasInternet();
        AccesoDatos.aplicacion = this;

        contexto = this;
        sesion = new SesionPreferences(contexto.getApplicationContext());
        sesion.validarSesion();

        if(sesion.sincronizadoTodo()){
            Intent i = new Intent(contexto, MainActivity.class);
            startActivity(i);
            this.finish();
        }
        Funciones.cargarManoObra(sesion);

        ltImagen= (LottieAnimationView) findViewById(R.id.ltImageOnBoarding);
        txtDescripcion = (TextView) findViewById(R.id.txtOnboarding);
        lblTitulo= (TextView) findViewById(R.id.lblTituloOnBoarding);
        btnSincronizar = (Button) findViewById(R.id.btnOnBoarding);
        btnSiguiente = (Button) findViewById(R.id.btnSiguienteOnBoarding);

        arribaAbajo= AnimationUtils.loadAnimation(this,R.anim.dearribaabajocompleto);
        derechaIzquierda = AnimationUtils.loadAnimation(this,R.anim.derechaizquierda);
        abajoArriba = AnimationUtils.loadAnimation(this,R.anim.deabajoarribacompleto);
        fadeIn = AnimationUtils.loadAnimation(this, R.anim.fadein);

        txtDescripcion.setAnimation(derechaIzquierda);
        btnSincronizar.setAnimation(abajoArriba);
        ltImagen.setAnimation(arribaAbajo);
        arribaAbajo.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                lblTitulo.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                lblTitulo.setAnimation(fadeIn);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        fadeIn.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                lblTitulo.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        persona = new Persona();
        sancionado = new Sancionado();

        fundo = new Fundo();
        visitaGeneral = new Visita();
        //vista = (View)findViewById(R.id.lyOnBoarding);
        lblTitulo.setText("Sincronización de conductores");


        if(sesion.sincronizadoPersona() && sesion.sincronizadoSancionado()){
            ltImagen.setMinAndMaxProgress(0,1);
            ltImagen.setAnimation(R.raw.check_boarding);
            ltImagen.playAnimation();
            txtDescripcion.setText("Genial, la lista de conductores ha sido sincronizada con éxito.");
            btnSincronizar.setVisibility(View.GONE);
            btnSiguiente.setVisibility(View.VISIBLE);
            btnSiguiente.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    comprobarEntradasYvisitas();
                }
            });
        }else{
            ltImagen.setMinAndMaxProgress(0f, 0.15f);
            ltImagen.setAnimation(R.raw.carsinc);
            ltImagen.playAnimation();
            txtDescripcion.setText("Para poder utilizar esta aplicación con conexión a internet y sin conexión, primero debes sincronizar la lista de conductores. \n \n Por favor presiona el botón \"SINCRONIZAR\".");
            btnSincronizar.setVisibility(View.VISIBLE);
            btnSiguiente.setVisibility(View.GONE);
            btnSincronizar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    asynPersonas = new CargarListaPersonas();
                    descargarDatosPersona();
                }
            });
        }
        //crearPuntos();
    }

    private void comprobarEntradasYvisitas(){
        if(sesion.sincronizadoEntrada() && sesion.sincronizadoVisita()){
            ltImagen.setAnimation(R.raw.check_boarding);
            ltImagen.playAnimation();
            txtDescripcion.setText("Genial, tu lista de entradas ya ha sido sincronizada.");
            btnSincronizar.setVisibility(View.GONE);
            btnSiguiente.setVisibility(View.VISIBLE);
            btnSiguiente.setText("TERMINAR");
            btnSiguiente.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(contexto, MainActivity.class);
                    contexto.startActivity(i);
                    OnBoardingActivity.this.finish();
                }
            });
        }else{
            ltImagen.setAnimation(R.raw.sincentry);
            ltImagen.setMinAndMaxProgress(0,1);
            ltImagen.playAnimation();
            ltImagen.setRepeatCount(LottieDrawable.INFINITE);
            lblTitulo.setText("Sincronización de entradas y salidas");
            txtDescripcion.setText("Tan solo hace falta sincronizar tu lista de entradas y salidas. \n \n Por favor presiona el botón \"SINCRONIZAR\".");
            btnSincronizar.setVisibility(View.VISIBLE);
            btnSiguiente.setVisibility(View.GONE);
            btnSincronizar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    asynFundos = new CargarListaFundos();
                    descargarDatosFundo();
                }
            });
        }
    }

    private void descargarDatosPersona(){
        Funciones.mensajito = "Estableciendo conexión segura con el servidor...";
        cargando = Funciones.cargando(contexto, "","sinc");
        if(asynPersonas.getStatus() == AsyncTask.Status.PENDING || asynPersonas.getStatus() == AsyncTask.Status.FINISHED){
            asynPersonas.execute();
        }
    }

    private void descargarDatosFundo(){
        Funciones.mensajito = "Estableciendo conexión segura con el servidor...";
        cargando = Funciones.cargando(contexto, "","sinc");
        if(asynFundos.getStatus() == AsyncTask.Status.PENDING || asynFundos.getStatus() == AsyncTask.Status.FINISHED){
            asynFundos.execute();
        }
    }

    /* Cargando conductores del servidor */

    public class CargarListaPersonas extends AsyncTask<Void, Void, Integer> {


        @Override
        protected Integer doInBackground(Void... params) {
            Integer retorno = 0;
            try {
                String urlListado = Funciones.URL_WS + "persona.cargar.movil.controlador.php";
                HashMap parametros = new HashMap<String, String>();
                String resultado = new Funciones().getHttpContent(urlListado, parametros);
                cargarPersonas(resultado);
                retorno = 1; // Satisfactoriamente

            } catch (Exception e) {
                e.printStackTrace();
            }

            return retorno;
        }

        @Override
        protected void onPostExecute(Integer retorno) {
            if (retorno == 1) {
                if(cargando.isShowing()){
                    cargando.dismiss();
                    ltImagen.setMinAndMaxProgress(0,1);
                    ltImagen.setAnimation(R.raw.check_boarding);
                    ltImagen.playAnimation();
                    txtDescripcion.setText("Genial, la lista de conductores ha sido sincronizada con éxito.");
                    btnSincronizar.setVisibility(View.GONE);
                    btnSiguiente.setVisibility(View.VISIBLE);
                    btnSiguiente.setText("SIGUIENTE");
                    btnSiguiente.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            comprobarEntradasYvisitas();
                        }
                    });
                    /*asynFundos = new CargarListaFundos();
                    descargarDatosFundo();*/
                }
            } else {
                Log.e("error","cargarListaPersonas");
            }
        }
    }

    private void cargarPersonas(String resultado) {
        try {
            JSONObject json = new JSONObject(resultado);
            JSONArray jsonArrayPersonas = json.getJSONArray("mensaje");
            JSONArray jsonArraySancionados = json.getJSONArray("datos");

            //ArrayList<Persona> listaPersonas = new ArrayList<Persona>();
            //ArrayList<Sancionado> listaSancionados = new ArrayList<Sancionado>();

            int numeroPersonas = jsonArrayPersonas.length(), numeroSancionados = jsonArraySancionados.length(), progreso = 0, totalSincronizar = numeroPersonas+numeroSancionados, porcentaje = 0;
            Funciones.barraCargando.setProgress(0);
            Funciones.barraCargando.showContextMenu();
            Funciones.barraCargando.setMax(totalSincronizar);
            Funciones.mensajito = "Sincronizando conductores: 0%";

            for (int i = 0; i < numeroPersonas; i++) {
                JSONObject jsonData = jsonArrayPersonas.getJSONObject(i);
                Persona item = new Persona();
                item.setDni(jsonData.getString("dni"));
                item.setNombre(jsonData.getString("nombre"));
                item.setPaterno(jsonData.getString("paterno"));
                item.setMaterno(jsonData.getString("materno"));
                item.setEstado(jsonData.getString("estado"));
                item.setSincronizado("S");
                item.setCodigo_off_line(0);
                persona.agregarPersona(item);
                Funciones.barraCargando.setProgress(progreso++);
                porcentaje = 100 * progreso / totalSincronizar;
                Funciones.mensajito = "Sincronizando conductores: "+ porcentaje +"%";
            }

            for (int j = 0; j < numeroSancionados; j++){
                JSONObject data = jsonArraySancionados.getJSONObject(j);
                Sancionado san = new Sancionado();
                san.setDni(data.getString("dni"));
                san.setMotivo(data.getString("motivo"));
                san.setDesde(data.getString("desde"));
                san.setHasta(data.getString("hasta"));
                sancionado.agregarSancionado(san);
                Funciones.barraCargando.setProgress(progreso++);
                porcentaje = 100 * progreso / totalSincronizar;
                Funciones.mensajito = "Sincronizando conductores: "+ porcentaje +"%";
            }
            sesion.setSincronizadoSancionado();
            sesion.setSincronizadoPersona();
        }catch (RuntimeException | JSONException e){
            Log.e("Error cargarPersonas: " , e.getMessage());
        }
    }

    /* Cargando conductores del servidor */


    /* Cargando fundos del servidor */

    public class CargarListaFundos extends AsyncTask<Void, Void, Integer> {


        @SuppressLint("LongLogTag")
        @Override
        protected Integer doInBackground(Void... params) {
            Integer retorno = 0;
            try {
                String urlListado = Funciones.URL_WS + "fundo.cargar.movil.controlador.php";
                HashMap parametros = new HashMap<String, String>();
                parametros.put("codigo_empresa",sesion.getCodigoEmpresa());
                String resultado = new Funciones().getHttpContent(urlListado, parametros);
                cargarFundos(resultado);
                retorno = 1; // Satisfactoriamente

            } catch (Exception e) {
                Log.e("doInBackground-cargarFundos", e.getLocalizedMessage());
                e.printStackTrace();
            }
            return retorno;
        }

        @SuppressLint("LongLogTag")
        @Override
        protected void onPostExecute(Integer retorno) {
            try{
                if (retorno == 1) {
                    //validarFundos(cargando);
                    if(cargando.isShowing()){
                        cargando.dismiss();
                    }
                    ltImagen.setAnimation(R.raw.check_boarding);
                    ltImagen.playAnimation();
                    ltImagen.setRepeatCount(0);
                    txtDescripcion.setText("Genial, tu lista de entradas ya ha sido sincronizada.");
                    btnSincronizar.setVisibility(View.GONE);
                    btnSiguiente.setVisibility(View.VISIBLE);
                    if(sesion.sincronizadoVisita() && sesion.sincronizadoEntrada() && sesion.sincronizadoPersona() && sesion.sincronizadoSancionado()){
                        sesion.setSincronizadoTodo();
                    }
                    btnSiguiente.setText("TERMINAR");
                    btnSiguiente.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent i = new Intent(contexto, MainActivity.class);
                            contexto.startActivity(i);
                            OnBoardingActivity.this.finish();
                        }
                    });
                } else {
                    Log.e("error","cargarListaFundos");
                }
            }catch (RuntimeException e){
                Log.e("onPostExecute-cargarFundos-catch", retorno+"");
                Intent i = new Intent(contexto, MainActivity.class);
                contexto.startActivity(i);
                OnBoardingActivity.this.finish();
            }
        }
    }

    private void cargarFundos(String resultado) {
        try {
            Log.e("cargarFundos", resultado);
            JSONObject json = new JSONObject(resultado);
            JSONArray jsonDatos = json.getJSONArray("datos");

            int numeroEntradas = jsonDatos.length(), progreso = 0, porcentaje = 0;
            Funciones.barraCargando.setProgress(0);
            Funciones.barraCargando.showContextMenu();
            Funciones.barraCargando.setMax(numeroEntradas);
            Funciones.mensajito = "Sincronizando entradas y salidas: 0%";

            for (int i = 0; i < numeroEntradas; i++) {
                JSONObject data = jsonDatos.getJSONObject(i);
                Fundo item = new Fundo();
                String ocupantes = data.getString("numero_ocupantes");
                if(ocupantes.equalsIgnoreCase("") || ocupantes.isEmpty()){
                    ocupantes = "0";
                }
                item.setCodigo((data.getInt("codigo_entrada_fundo")));
                item.setFecha(data.getString("fecha_entrada"));
                item.setFecha_manual(data.getString("fecha_manual"));
                item.setHora(data.getString("hora_entrada"));
                item.setDni(data.getString("dni"));
                item.setNombre(data.getString("conductor"));
                item.setCodigo_categoria_personal((data.getInt("codigo_categoria_personal")));
                item.setNombre_empresa(data.getString("nombre_empresa"));
                item.setCodigo_tipo_vehiculo((data.getInt("codigo_tipo_vehiculo")));
                item.setPlaca_entrada(data.getString("placa"));
                item.setProcedencia(data.getString("procedencia_fundo"));
                String asientos = data.getString("numero_asientos");
                if(asientos.isEmpty() || asientos.equalsIgnoreCase("")){
                    asientos = "0";
                }
                item.setNum_asientos(Integer.parseInt(asientos));
                item.setNum_ocupantes(Integer.parseInt(ocupantes));
                item.setMotivo(data.getString("motivo_fundo"));
                item.setContenedor(data.getString("contenedor"));
                item.setEntrada_salida(data.getString("entrada_salida_fundo"));
                item.setCodigo_empresa((data.getInt("codigo_empresa")));
                item.setCodigo_usuario((data.getInt("codigo_usuario")));
                item.setEstado_entrada(data.getString("estado_entrada_fundo"));
                item.setHora_manual(data.getString("hora_manual"));
                item.setCodigo_ruta(data.getInt("codigo_ruta"));
                item.setCodigo_entrada(data.getInt("codigo_ruta"));
                item.setSincronizado("S");
                item.setCodigo_off_line(data.getString("codigo_off"));
                item.setOrientacion(data.getString("orientacion"));
                item.setMano_obra(data.getString("mano_obra"));
                item.agregarFundo();

                JSONArray jsonVisitas = (JSONArray) data.get("visitas_lista");

                for (int j = 0; j < jsonVisitas.length(); j++){
                    JSONObject dataVisita = jsonVisitas.getJSONObject(j);
                    if(!dataVisita.getString("dni").equalsIgnoreCase("DNI no encontrado")){
                        Visita visita = new Visita();
                        visita.setCodigo_visita(dataVisita.getInt("codigo_visita_fundo"));
                        visita.setDni(dataVisita.getString("dni"));
                        visita.setCodigo_entrada(dataVisita.getInt("codigo_entrada_fundo"));
                        visita.setComentario(dataVisita.getString("comentario"));
                        visita.setSincronizado("S");
                        visita.setCodigo_off_line("");
                        visita.agregarVisita(visita);
                    }
                }

                JSONArray jsonManoObra = (JSONArray) data.get("manos_obra_lista");

                for (int j = 0; j < jsonManoObra.length(); j++){
                    JSONObject dataManosObra = jsonManoObra.getJSONObject(j);
                    ManoObraEntrada manoObra = new ManoObraEntrada();
                    manoObra.setCodigo_entrada(dataManosObra.getInt("codigo_entrada"));
                    manoObra.setCodigo_mano_obra(dataManosObra.getInt("codigo_mano_obra"));
                    manoObra.setNumero_trabajadores(dataManosObra.getInt("numero_trabajadores"));
                    manoObra.setCodigo_off_line(dataManosObra.getString("off_line"));
                    manoObra.setCodigo_off_line_fundo(dataManosObra.getString("off_line_fundo"));
                    manoObra.setSincronizado("S");
                    manoObra.agregarManoObraEntrada();
                }

                Funciones.barraCargando.setProgress(progreso++);
                porcentaje = 100 * progreso / numeroEntradas;
                porcentaje = Math.min(porcentaje, 100);
                Funciones.mensajito = "Sincronizando entradas y salidas: "+ porcentaje +"%";
            }
            sesion.setSincronizadoEntrada();
            sesion.setSincronizadoVisita();
            Funciones.mensajito = "";
        }catch (RuntimeException | JSONException e){
            Log.e("Error cargarFundos" , e.getMessage());
        }
    }

    /* Cargando fundos del servidor */
}
