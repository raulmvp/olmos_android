package pe.proolmos.proolmoscontrol;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.airbnb.lottie.LottieAnimationView;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.firebase.ml.vision.FirebaseVision;
import com.google.firebase.ml.vision.barcode.FirebaseVisionBarcode;
import com.google.firebase.ml.vision.barcode.FirebaseVisionBarcodeDetector;
import com.google.firebase.ml.vision.barcode.FirebaseVisionBarcodeDetectorOptions;
import com.google.firebase.ml.vision.common.FirebaseVisionImage;
import com.google.firebase.ml.vision.common.FirebaseVisionImageMetadata;
import com.otaliastudios.cameraview.controls.Flash;
import com.otaliastudios.cameraview.frame.Frame;
import com.otaliastudios.cameraview.frame.FrameProcessor;
import com.otaliastudios.cameraview.gesture.Gesture;
import com.otaliastudios.cameraview.gesture.GestureAction;
import com.suke.widget.SwitchButton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import pe.proolmos.proolmoscontrol.Negocio.Fundo;
import pe.proolmos.proolmoscontrol.Negocio.ManoObra;
import pe.proolmos.proolmoscontrol.Negocio.ManoObraEntrada;
import pe.proolmos.proolmoscontrol.Negocio.Persona;
import pe.proolmos.proolmoscontrol.Negocio.TipoVehiculo;
import pe.proolmos.proolmoscontrol.Negocio.Visita;
import pe.proolmos.proolmoscontrol.Sesion.SesionPreferences;
import pe.proolmos.proolmoscontrol.databinding.ActivityCamaraSalidaBinding;
import pe.proolmos.proolmoscontrol.datos.AccesoDatos;
import pe.proolmos.proolmoscontrol.util.Funciones;

public class CamaraActivitySalida extends AppCompatActivity {
    public static ActivityCamaraSalidaBinding binding;
    private static final int ID_PERMISO_CAMARA = 101;
    Context contexto;
    SesionPreferences sesion;
    public static int contador;
    public static boolean codigoDetectado;
    FirebaseVisionBarcodeDetectorOptions opciones;
    FirebaseVisionBarcodeDetector detector;

    public static Dialog cargando;
    BottomSheetDialog dialogBottomSheet;
    private String ultimoQR;

    CargarDatosConductor asyncConductor;
    RegistrarSalida asyncSalida;
    RegistrarSql asyncSql;
    CargarDatosConductorSQLite asyncConductorSQLite;

    Button btnRegistro;
    TextView txtFecha, txtFechaElegida, txtHora, lblNombreConductor, lblEstadoConductor;
    EditText txtDocumento, txtEmpresa, txtProcedencia, txtPlaca, txtNumeroAsientos, txtNumeroOcupantes, txtPlacaContenedor;
    LottieAnimationView ltEstadoConductor;
    CheckBox cbContenedor;
    View fragmentoActual, linearContenedor, btnRetroceder, vista;


    /*Settings entrada rápida*/
    boolean tipoScan;
    Animation derechaIzquierda, centroIzquierda;
    BottomSheetDialog dialogRapidaBottomSheet;

    TextView lblNombreConductorRapida, lblDniConductorRapida, lblEstadoConductorRapida, lblPlacaConductorRapida;
    EditText txtOcupantesRapida, txtPlacaContenedorRapida;
    LottieAnimationView ltEstadoConductorRapida;
    CheckBox checkContenedorRapida;
    View btnRetrocederRapida, linearPlacaContenedorRapida;
    int codigo_empresaRapida;
    Button btnAceptarRapida, btnEditarRapida;
    boolean editarData;
    /*Settings entrada rápida*/

    /*PROVEEDOR ATRIBUTOS*/
    private int codigo_proveedor, codigo_ruta_transporte, codigo_proveedor_empresa;
    /*PROVEEDOR ATRIBUTOS*/
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        contexto = this;
        AccesoDatos.aplicacion = contexto;
        sesion = new SesionPreferences(contexto);
        sesion.validarSesion();

        /*PROVEEDOR ATRIBUTOS*/
        codigo_proveedor=0; codigo_ruta_transporte=0; codigo_proveedor_empresa=0;
        /*PROVEEDOR ATRIBUTOS*/
        ultimoQR = "";
        contador = 0;
        codigoDetectado = false;
        tipoScan = false;
        editarData = false;
        binding = DataBindingUtil.setContentView(this, R.layout.activity_camara_salida);
        fragmentoActual = (View) findViewById(R.id.CamaraActivitySalida);
        binding.txtDocumentoCamara.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if(keyCode == 66 && event.getAction() == 1) { //Si se presionó "enter" y se soltó la tecla
                    String dni = binding.txtDocumentoCamara.getText().toString();
                    buscarConductor(dni, "");
                }
                return false;
            }
        });

        binding.btnEnviar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String dni = binding.txtDocumentoCamara.getText().toString();
                buscarConductor(dni, "");
            }
        });

        binding.swEntradaRapida.setOnCheckedChangeListener(new SwitchButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(SwitchButton view, boolean isChecked) {
                if(isChecked){
                    centroIzquierda = AnimationUtils.loadAnimation(contexto,R.anim.centroizquierda);
                    binding.lblEntradaRapida.setTextColor(ContextCompat.getColor(contexto, R.color.colorPrimaryLight));
                    binding.txtDocumentoCamara.setVisibility(View.GONE);
                    binding.btnEnviar.setVisibility(View.GONE);
                    binding.btnEnviar.setAnimation(centroIzquierda);
                    binding.txtDocumentoCamara.setAnimation(centroIzquierda);
                }else{
                    derechaIzquierda = AnimationUtils.loadAnimation(contexto,R.anim.derechaizquierda);
                    binding.lblEntradaRapida.setTextColor(ContextCompat.getColor(contexto, R.color.registroEntradaRapida));
                    binding.txtDocumentoCamara.setVisibility(View.VISIBLE);
                    binding.btnEnviar.setVisibility(View.VISIBLE);
                    binding.btnEnviar.setAnimation(derechaIzquierda);
                    binding.txtDocumentoCamara.setAnimation(derechaIzquierda);
                }
                tipoScan = isChecked;
            }
        });

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                binding.btnCerrar.playAnimation();
            }
        }, 200);
        binding.btnCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CamaraActivitySalida.this.finish();
            }
        });

        if(revisarPermisoCamara()){
            iniciarCamara();
            iniciarBottomSheetLayout();
            iniciarBottomSheetRapidaLayout();
        }else{
            pedirPermiso();
        }
    }

    private void buscarConductor(String dni, String tipo){
        if(dni.length() >= 8){
            asyncConductor = new CargarDatosConductor(dni, tipo);
            binding.txtDocumentoCamara.setText("");
            if(asyncConductor.getStatus() == AsyncTask.Status.PENDING || asyncConductor.getStatus() == AsyncTask.Status.FINISHED){
                asyncConductor.execute();
            }
        }else{
            Funciones.snack(fragmentoActual, contexto, "El número ingresado no es válido", "rojo", "ok");
        }
    }

    private void limpiarDataBottomSheet(){
        reanudarCamaraScan();
    }

    private void iniciarBottomSheetLayout() {
        dialogBottomSheet = new BottomSheetDialog(contexto, R.style.BottomSheetDialogTheme);

        vista = LayoutInflater.from(contexto).inflate(R.layout.bottom_sheet_salida,
                (LinearLayout)findViewById(R.id.bottom_sheet_salida));
        dialogBottomSheet.setContentView(vista);
        dialogBottomSheet.setCanceledOnTouchOutside(false);
        dialogBottomSheet.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                limpiarDataBottomSheet();
            }
        });
        dialogBottomSheet.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                limpiarDataBottomSheet();
            }
        });

        txtFecha = (TextView) vista.findViewById(R.id.txtFecha);
        txtFechaElegida = (TextView) vista.findViewById(R.id.txtFechaElegida);
        txtHora = (TextView) vista.findViewById(R.id.txtHora);
        lblNombreConductor = (TextView) vista.findViewById(R.id.lblNombreConductor);
        lblEstadoConductor = (TextView) vista.findViewById(R.id.lblEstadoConductor);
        txtEmpresa = (EditText)vista.findViewById(R.id.txtEmpresa);
        txtProcedencia = (EditText) vista.findViewById(R.id.txtProcedencia);
        txtPlaca = (EditText) vista.findViewById(R.id.txtPlaca);
        txtNumeroAsientos = (EditText) vista.findViewById(R.id.txtNumeroAsientos);
        txtNumeroOcupantes = (EditText) vista.findViewById(R.id.txtNumeroOcupantes);
        txtPlacaContenedor = (EditText) vista.findViewById(R.id.txtPlacaContenedor);
        txtDocumento = (EditText) vista.findViewById(R.id.txtDocumento);
        btnRegistro = (Button) vista.findViewById(R.id.btnRegistroEntrada);
        ltEstadoConductor = (LottieAnimationView) vista.findViewById(R.id.ltEstadoConductor);
        cbContenedor = (CheckBox) vista.findViewById(R.id.checkboxContenedor);
        linearContenedor = (View) vista.findViewById(R.id.linearPlacaContenedor);
        btnRetroceder = (View) vista.findViewById(R.id.btnCerrarBottomSheet);

        txtEmpresa.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable et) {
                String s = et.toString();
                if(!s.equals(s.toUpperCase()))
                {
                    s=s.toUpperCase();
                    txtEmpresa.setText(s);
                    txtEmpresa.setSelection(txtEmpresa.length()); //fix reverse texting
                }
            }
        });

        txtProcedencia.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable et) {
                String s = et.toString();
                if(!s.equals(s.toUpperCase()))
                {
                    s=s.toUpperCase();
                    txtProcedencia.setText(s);
                    txtProcedencia.setSelection(txtProcedencia.length()); //fix reverse texting
                }
            }
        });


        btnRetroceder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(dialogBottomSheet.isShowing()){
                    dialogBottomSheet.dismiss();
                }
            }
        });

        cbContenedor.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                txtPlacaContenedor.setText("");
                if(cbContenedor.isChecked()){
                    linearContenedor.setVisibility(View.VISIBLE);
                }else{
                    linearContenedor.setVisibility(View.GONE);
                }
            }
        });

        btnRegistro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!validarDatos()){
                    return;
                }

                boolean confirmar = Funciones.mensajeConfirmacion("pregunta", contexto, "Confirmar registro de salida");
                if(confirmar){
                    Fundo fundo = new Fundo();
                    fundo.setCodigo(0);
                    fundo.setFecha(Funciones.obtenerFechaActual("dd/MM/yyyy"));
                    fundo.setHora(Funciones.obtenerFechaActual("HH:mm"));
                    fundo.setDni(txtDocumento.getText().toString());
                    fundo.setNombre(lblNombreConductor.getText().toString());
                    fundo.setCodigo_categoria_personal(0);
                    fundo.setNombre_empresa(txtEmpresa.getText().toString());
                    fundo.setCodigo_tipo_vehiculo(0);
                    fundo.setProcedencia(txtProcedencia.getText().toString());
                    fundo.setNum_asientos(Integer.parseInt(txtNumeroAsientos.getText().toString()));
                    fundo.setNum_ocupantes(Integer.parseInt(txtNumeroOcupantes.getText().toString()));
                    fundo.setMotivo("");
                    if(cbContenedor.isChecked()){
                        fundo.setContenedor("C");
                        fundo.setPlaca_entrada(txtPlacaContenedor.getText().toString().toUpperCase());
                    }else{
                        fundo.setContenedor("");
                        fundo.setPlaca_entrada(txtPlaca.getText().toString().toUpperCase());
                    }

                    fundo.setEntrada_salida("S");
                    fundo.setCodigo_empresa(Integer.parseInt(sesion.getCodigoEmpresa()));
                    fundo.setCodigo_usuario(Integer.parseInt(sesion.getCodigoUsuario()));
                    fundo.setEstado_entrada("E");
                    fundo.setHora_manual(txtHora.getText().toString());
                    fundo.setCodigo_ruta(0);
                    fundo.setCodigo_entrada(0);
                    fundo.setFecha_manual(txtFecha.getText().toString());
                    fundo.setSincronizado("N");
                    fundo.setCodigo_off_line(Funciones.crearCodigoOffLine(sesion.getCodigoEmpresa(), sesion.getCodigoUsuario()));
                    fundo.setCodigo_caseta(0);
                    fundo.setCodigo_proveedor(codigo_proveedor);
                    fundo.setCodigo_proveedor_empresa(codigo_proveedor_empresa);
                    fundo.setCodigo_ruta_transporte(codigo_ruta_transporte);
                    if(sesion.getPinesEmpresa().contains("[]") || sesion.getPinesEmpresa().isEmpty()){
                        fundo.setPin("--------");
                    }else{
                        fundo.setPin(sesion.getPinUsuario());
                    }

                    asyncSql = new RegistrarSql(fundo);
                    if(asyncSql.getStatus() == AsyncTask.Status.PENDING || asyncSql.getStatus() == AsyncTask.Status.FINISHED){
                        asyncSql.execute();
                    }
                }
            }
        });

        txtFecha.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Funciones.calendario(contexto, txtFecha, txtFechaElegida, txtFecha.getText().toString());
            }
        });
        txtHora.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Funciones.reloj(contexto, txtHora);
            }
        });

        txtPlaca.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable et) {
                String s = et.toString();
                if(!s.equals(s.toUpperCase()))
                {
                    s=s.toUpperCase();
                    txtPlaca.setText(s);
                    txtPlaca.setSelection(txtPlaca.length()); //fix reverse texting
                }
            }
        });

        txtPlacaContenedor.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable et) {
                String s = et.toString();
                if(!s.equals(s.toUpperCase()))
                {
                    s=s.toUpperCase();
                    txtPlacaContenedor.setText(s);
                    txtPlacaContenedor.setSelection(txtPlacaContenedor.length()); //fix reverse texting
                }
            }
        });

//        txtPlaca.setOnKeyListener(new View.OnKeyListener() {
//            @Override
//            public boolean onKey(View v, int keyCode, KeyEvent event) {
//                if(keyCode == 66 && event.getAction() == 1) { //Si se presionó "enter" y se soltó la tecla
//                    String placa = txtPlaca.getText().toString();
//                    if(placa.length() >= 4){
//                        String placaDecoded = Funciones.decodeBase64(placa);
//                        if(placaDecoded.contains("cod_ve")){
//                            //cod_ve=3#pla=N3G-R0I#mar=NISSAN#mod=SENTRA #cod_tv=1#cod_tp=1#cod_emp=25
//                            cargando = new Dialog(contexto);
//                            String[] qrDecodificado = placaDecoded.split("#");
//                            placa = qrDecodificado[1].split("=")[1];
//                            txtPlaca.setText(placa);
//                            cargando.dismiss();
//                        }else{
//                            txtPlaca.setText(placa.substring(0,7));
//                        }
//                    }
//                }
//                return false;
//            }
//        });

//        txtPlacaContenedor.setOnKeyListener(new View.OnKeyListener() {
//            @Override
//            public boolean onKey(View v, int keyCode, KeyEvent event) {
//                if(keyCode == 66 && event.getAction() == 1) { //Si se presionó "enter" y se soltó la tecla
//                    String placa = txtPlacaContenedor.getText().toString();
//                    if(placa.length() >= 4){
//                        String placaDecoded = Funciones.decodeBase64(placa);
//                        if(placaDecoded.contains("cod_ve")){
//                            //cod_ve=3#pla=N3G-R0I#mar=NISSAN#mod=SENTRA #cod_tv=1#cod_tp=1#cod_emp=25
//                            cargando = new Dialog(contexto);
//                            String[] qrDecodificado = placaDecoded.split("#");
//                            placa = qrDecodificado[1].split("=")[1];
//                            txtPlacaContenedor.setText(placa);
//                            cargando.dismiss();
//                        }else{
//                            txtPlacaContenedor.setText(placa.substring(0,7));
//                        }
//                    }
//                }
//                return false;
//            }
//        });
    }

    private void limpiarDataBottomSheetRapida(){
        lblNombreConductorRapida.setText("");
        lblDniConductorRapida.setText("");
        lblEstadoConductorRapida.setText("");
        txtPlacaContenedorRapida.setText("");
        lblPlacaConductorRapida.setText("");
        txtOcupantesRapida.setText("");
        checkContenedorRapida.setChecked(false);
        codigo_proveedor =0; codigo_ruta_transporte=0; codigo_proveedor_empresa=0;
    }

    private void iniciarBottomSheetRapidaLayout() {
        dialogRapidaBottomSheet = new BottomSheetDialog(contexto, R.style.BottomSheetDialogTheme);

        vista = LayoutInflater.from(contexto).inflate(R.layout.bottom_sheet_salida_rapida,
                (LinearLayout)findViewById(R.id.bottom_sheet_salida_rapida));
        dialogRapidaBottomSheet.setContentView(vista);

        dialogRapidaBottomSheet.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialogInterface) {
                editarData = false;
            }
        });

        dialogRapidaBottomSheet.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                limpiarDataBottomSheetRapida();
                binding.camaraQr.open();
                codigoDetectado = false;
                contador = 0;
            }
        });
        dialogRapidaBottomSheet.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                limpiarDataBottomSheetRapida();
                if(!editarData){
                    binding.camaraQr.open();
                    codigoDetectado = false;
                    contador = 0;
                }
            }
        });

        checkContenedorRapida = (CheckBox) vista.findViewById(R.id.checkboxContenedorRapida);
        lblNombreConductorRapida = (TextView) vista.findViewById(R.id.lblNombreConductorRapida);
        lblDniConductorRapida = (TextView) vista.findViewById(R.id.lblDniConductorRapida);
        lblEstadoConductorRapida = (TextView) vista.findViewById(R.id.lblEstadoConductorRapida);
        lblPlacaConductorRapida = (TextView) vista.findViewById(R.id.lblPlacaRapida);
        txtOcupantesRapida = (EditText) vista.findViewById(R.id.txtNumeroOcupantesRapida);
        txtPlacaContenedorRapida = (EditText) vista.findViewById(R.id.txtPlacaContenedorRapida);
        ltEstadoConductorRapida = (LottieAnimationView) vista.findViewById(R.id.ltEstadoConductorRapida);

        btnAceptarRapida = (Button) vista.findViewById(R.id.btnRegistroRapido);
        btnEditarRapida = (Button) vista.findViewById(R.id.btnEditarRapida);

        btnRetrocederRapida = (View) vista.findViewById(R.id.btnCerrarBottomSheetRapida);

        linearPlacaContenedorRapida = (View) vista.findViewById(R.id.linearPlacaContenedorRapida);

        btnRetrocederRapida.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(dialogRapidaBottomSheet.isShowing()){
                    dialogRapidaBottomSheet.dismiss();
                    binding.camaraQr.open();
                    codigoDetectado = false;
                    contador = 0;
                }
            }
        });


        txtOcupantesRapida.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(!hasFocus){
                    if(txtOcupantesRapida.getText().toString().trim().isEmpty()){
                        txtOcupantesRapida.setText("0");
                    }
                }
            }
        });

        txtPlacaContenedorRapida.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable et) {
                String s = et.toString();
                if(!s.equals(s.toUpperCase()))
                {
                    s=s.toUpperCase();
                    txtPlacaContenedorRapida.setText(s);
                    txtPlacaContenedorRapida.setSelection(txtPlacaContenedorRapida.length()); //fix reverse texting
                }
            }
        });

        checkContenedorRapida.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                txtPlacaContenedorRapida.setText("");
                if(checkContenedorRapida.isChecked()){
                    linearPlacaContenedorRapida.setVisibility(View.VISIBLE);
                }else{
                    linearPlacaContenedorRapida.setVisibility(View.GONE);
                }
            }
        });
    }

    private void iniciarCamara() {
        binding.camaraQr.setLifecycleOwner(this);
        binding.camaraQr.mapGesture(Gesture.PINCH, GestureAction.ZOOM);
        binding.camaraQr.mapGesture(Gesture.TAP, GestureAction.AUTO_FOCUS);
        binding.btnFlashOn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                binding.camaraQr.setFlash(Flash.TORCH);
                binding.btnFlashOn.setVisibility(View.GONE);
                binding.btnFlashOff.setVisibility(View.VISIBLE);
            }
        });

        binding.btnFlashOff.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                binding.camaraQr.setFlash(Flash.OFF);
                binding.btnFlashOff.setVisibility(View.GONE);
                binding.btnFlashOn.setVisibility(View.VISIBLE);
            }
        });
        binding.camaraQr.addFrameProcessor(new FrameProcessor() {
            @Override
            public void process(@NonNull Frame frame) {
                procesarImagen(obtenerImagenDelFrame(frame));
            }
        });

        opciones = new FirebaseVisionBarcodeDetectorOptions.Builder()
                .setBarcodeFormats(FirebaseVisionBarcode.FORMAT_ALL_FORMATS)
                .build();
        detector = FirebaseVision.getInstance().getVisionBarcodeDetector(opciones);
    }

    private void procesarImagen(FirebaseVisionImage image) {
        if(!codigoDetectado){
            detector.detectInImage(image)
                    .addOnSuccessListener(new OnSuccessListener<List<FirebaseVisionBarcode>>() {
                        @Override
                        public void onSuccess(List<FirebaseVisionBarcode> firebaseVisionBarcodes) {
                            procesarResultado(firebaseVisionBarcodes);
                        }
                    });
        }
    }

    private void procesarResultado(List<FirebaseVisionBarcode> firebaseVisionBarcodes) {
        if(firebaseVisionBarcodes.size() > 0){
            codigoDetectado = true;
            for(FirebaseVisionBarcode item: firebaseVisionBarcodes){
                //int value_type = item.getValueType();
                String qr = item.getRawValue();
                String tipo = "";
                switch (item.getFormat()){
                    case FirebaseVisionBarcode.FORMAT_QR_CODE:
                        tipo = "QR";
                        break;
                    default:
                        tipo = "BC";
                        break;
                }
                asyncConductor = new CargarDatosConductor(qr, tipo);
                if(asyncConductor.getStatus() == AsyncTask.Status.PENDING || asyncConductor.getStatus() == AsyncTask.Status.FINISHED){
                    if(contador == 0){
                        if(ultimoQR.equalsIgnoreCase(qr)){
                            contador ++;
                            if(contador == 1){
                                boolean confirmacion = Funciones.mensajeConfirmacion("pregunta", contexto, "Este código acaba de ser escaneado, ¿estás seguro que deseas volver a registrar la entrada?");
                                if(confirmacion){
                                    asyncConductor.execute();
                                }else{
                                    contador = 0;
                                    codigoDetectado = false;
                                }
                            }
                        }else{
                            asyncConductor.execute();
                            contador ++;
                        }
                    }
                }
            }
        }
    }

    private FirebaseVisionImage obtenerImagenDelFrame(Frame frame) {
        byte[] data = frame.getData();
        FirebaseVisionImageMetadata metadata = new FirebaseVisionImageMetadata.Builder()
                .setFormat(FirebaseVisionImageMetadata.IMAGE_FORMAT_NV21)
                .setHeight(frame.getSize().getHeight())
                .setWidth(frame.getSize().getWidth())
                //.setRotation(frame.getRotation()) soolo usar si está en landscape
                .build();
        return FirebaseVisionImage.fromByteArray(data,metadata);
    }


    private void pedirPermiso() {
        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.CAMERA, Manifest.permission.RECORD_AUDIO},
                ID_PERMISO_CAMARA);
    }


    private boolean revisarPermisoCamara(){
        if(ActivityCompat.checkSelfPermission(contexto, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(contexto, Manifest.permission.RECORD_AUDIO) == PackageManager.PERMISSION_GRANTED){
            return true;
        }
        return false;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == ID_PERMISO_CAMARA) {
            if(grantResults.length>0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                iniciarCamara();
            }else{
                this.finish();
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(revisarPermisoCamara()){

        }else{
            pedirPermiso();
        }
    }

    @Override
    protected void onPause() {
        if(revisarPermisoCamara()){

        }else{
            pedirPermiso();
        }
        super.onPause();
    }

    /* Cargando datos del conductor */
    public class CargarDatosConductorSQLite extends AsyncTask<Void, Void, JSONObject> {

        private String dni;

        public CargarDatosConductorSQLite(String dni) {
            this.dni = dni;
        }

        @Override
        protected JSONObject doInBackground(Void... params) {
            JSONObject resultado = null;
            try {
                Persona persona = new Persona();
                String dniDecoded = Funciones.decodeBase64(this.dni), tipo = "";
//                if(dniDecoded.contains("cod_cond")){
//                    //cod_cond=14297#cod_emp=25#cond=VASQUEZ PACHECO, RAUL MICHAEL#lic=C48248144#cod_tp=1#dni=48248144
//                    String[] qrDecodificado = dniDecoded.split("#");
//                    String codigo_empresa = qrDecodificado[1].split("=")[1];
//                    String nombre_conductor = qrDecodificado[2].split("=")[1];
//                    int tipo_personal = Integer.parseInt(qrDecodificado[4].split("=")[1]);
//                    String dni = qrDecodificado[5].split("=")[1];
//                    resultado = persona.buscarConductorQr(dni, codigo_empresa, nombre_conductor, tipo_personal);
//                }else{
//                    if(dniDecoded.contains("cod_ve")){
////                        cod_ve=3#pla=N3G-R0I#mar=MARCA#mod=MODELO#cod_tv=8#cod_tp=1#cod_emp=25
//                        String[] qrDecodificado = dniDecoded.split("#");
//                        String placa = qrDecodificado[1].split("=")[1];
//                        resultado = persona.buscarConductorPorPlacaQR(placa);
//                    }else{
//                        resultado = persona.buscarConductor(this.dni);
//                    }
//                }
                if (dniDecoded.contains("cod_cond") && dniDecoded.contains("'fecha_ini'")){
                    tipo = "EXPIRABLE";
                } else if(dniDecoded.contains("cod_cond")){
                    tipo = "CONDUCTOR";
                }else if (dniDecoded.contains("cod_ve")){
                    tipo = "VEHICULO";
                } else if(dniDecoded.contains("rut_nomb")){
                    tipo = "TRANSPORTISTA";
                }
                Log.e("tipo", tipo);
                String[] qrDecodificado = dniDecoded.split("#");
                Log.e("qrDecoded", dniDecoded);
                switch (tipo){
                    case "EXPIRABLE":
                        String codigo_empresa = qrDecodificado[1].split("=")[1];
                        String nombre_conductor = qrDecodificado[2].split("=")[1];
                        int tipo_personal = Integer.parseInt(qrDecodificado[4].split("=")[1]);
                        String dni = qrDecodificado[5].split("=")[1];
                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
                        Date fecha_inicio = sdf.parse(qrDecodificado[6].split("=")[1]),
                                fecha_fin = sdf.parse(qrDecodificado[7].split("=")[1]),
                                fecha_hoy = sdf.parse(Funciones.obtenerFechaActual("yyyy-MM-dd"));
                        if(fecha_inicio.compareTo(fecha_hoy) < 0 && fecha_fin.compareTo(fecha_hoy) > 0){
                            Funciones.mensaje("advertencia", contexto, "Este código ya expiró, por favor verifique los datos");
                        }
                        resultado = persona.buscarConductorQr(dni, codigo_empresa, nombre_conductor, tipo_personal);
                        break;
                    case "CONDUCTOR":
                        //cod_cond=14297#cod_emp=25#cond=VASQUEZ PACHECO, RAUL MICHAEL#lic=C48248144#cod_tp=1#dni=48248144
                        String codigo_empresa2 = qrDecodificado[1].split("=")[1];
                        String nombre_conductor2 = qrDecodificado[2].split("=")[1];
                        int tipo_personal2 = Integer.parseInt(qrDecodificado[4].split("=")[1]);
                        String dni2 = qrDecodificado[5].split("=")[1];
                        resultado = persona.buscarConductorQr(dni2, codigo_empresa2, nombre_conductor2, tipo_personal2);
                        break;
                    case "VEHICULO":
                        //                        cod_ve=3#pla=N3G-R0I#mar=MARCA#mod=MODELO#cod_tv=8#cod_tp=1#cod_emp=25
                        String placa = qrDecodificado[1].split("=")[1];
                        resultado = persona.buscarConductorPorPlacaQR(placa);
                        break;
                    case "TRANSPORTISTA":
                        //Array ( [0] => ve_cod=142 [1] => ve_pla=A3X-204 [2] => ve_mar=TOYOTA [3] => ve_mod=MODELO [4] => ve_cap=4 [5] => ve_cod_tv=7 [6] => cond_cod_tp=2 [7] => emp_cod=25 [8] => prov_cod=6 [9] => prov_raz_soc=VASQUEZ PACHECO RAUL MICHAEL [10] => prov_ruc=10482481448 [11] => cond_cod=14297 [12] => cond_nomb=VASQUEZ PACHECO, RAUL MICHAEL [13] => cond_lic=C48248144 [14] => rut_cod=2 [15] => rut_nomb=CHICLAYO - OLMOS [16] => dni=48248144 [17] => empresa=PRO OLMOS [18] => ve_tipo=BUSES DE PERSONAL )
                        JSONObject json = new JSONObject();
                        String placaVehiculo = qrDecodificado[1].split("=")[1], dni_conductor = qrDecodificado[16].split("=")[1];

                        json.put("placa", placaVehiculo);
                        json.put("dni_conductor", dni_conductor);
                        json.put("codigo_caseta", 0);
                        json.put("numero_asientos", qrDecodificado[4].split("=")[1]);
                        json.put("nombre_conductor", qrDecodificado[12].split("=")[1]);
                        json.put("procedencia_fundo", qrDecodificado[15].split("=")[1]);
                        json.put("codigo_tipo_vehiculo", qrDecodificado[5].split("=")[1]);
                        json.put("codigo_categoria_personal", qrDecodificado[6].split("=")[1]);
                        json.put("nombre_empresa", qrDecodificado[9].split("=")[1]);

                        resultado = persona.buscarTransportista(json);
                        break;
                    default:
                        resultado = persona.buscarConductor(this.dni);
                        break;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return resultado;
        }

        @Override
        protected void onPostExecute(JSONObject resultado) {
            //limpiarDatos(false);
            if(cargando.isShowing()){
                cargando.dismiss();
            }

            if(resultado == null){
                txtDocumento.setText("");
                Funciones.snack(fragmentoActual, contexto, "No se encontraron datos.", "rojo", "ok");
                reanudarCamaraScan();
                return;
            }

            if(resultado.toString().equalsIgnoreCase("{}")){
                Funciones.snack(fragmentoActual, contexto, "No se encontraron datos. ", "rojo", "");
                reanudarCamaraScan();
                return;
            }

            try{
                llenarDatosConductor(resultado);
            }catch (RuntimeException e){
                Log.e("CarDatConductor SQLITE", e.getLocalizedMessage());
            }
        }
    }

    public class CargarDatosConductor extends AsyncTask<Void, Void, String> {

        private String dni, tipo;

        public CargarDatosConductor(String dni, String tipo) {
            this.dni = dni;
            this.tipo = tipo;
        }

        @Override
        protected void onPreExecute() {
            cargando = new Dialog(contexto);
            limpiarDatos(false);
            cargando = Funciones.cargando(contexto, "Buscando...", "");
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(Void... params) {
            String resultado = "";
            try {
                String urlListado = Funciones.URL_WS + "fundo.entrada.scan.conductor.movil.php";
                HashMap parametros = new HashMap<String, String>();
                parametros.put("p_dni", this.dni);
                parametros.put("p_tipo", this.tipo);
                ultimoQR = this.dni;
                resultado = new Funciones().getHttpContent(urlListado, parametros);
            } catch (Exception e) {
                e.printStackTrace();
            }

            return resultado;
        }

        @Override
        protected void onPostExecute(String resultado) {


            if(resultado.equalsIgnoreCase("******-----****") || resultado.isEmpty()){
                asyncConductorSQLite = new CargarDatosConductorSQLite(this.dni);
                descargarDatosConductorSql();
                return;
            }

            try{
                JSONObject jsonObject = new JSONObject(resultado);
                String mensaje = jsonObject.getString("mensaje");
                String validarDatos = jsonObject.getString("datos");

                if(validarDatos.contains("[]")){
                    asyncConductorSQLite = new CargarDatosConductorSQLite(this.dni);
                    descargarDatosConductorSql();
                    return;
                }

                if(!mensaje.equalsIgnoreCase("error")){
                    JSONObject jsonObjectDatos = jsonObject.getJSONObject("datos");
                    llenarDatosConductor(jsonObjectDatos);
                    if(cargando.isShowing()){
                        cargando.dismiss();
                    }
                }else{
                    asyncConductorSQLite = new CargarDatosConductorSQLite(this.dni);
                    descargarDatosConductorSql();
                }
            }catch (RuntimeException | JSONException e){
                Log.e("CargarDatosConductor", e.getLocalizedMessage());
            }
        }
    }

    private void descargarDatosConductorSql(){
        if(asyncConductorSQLite.getStatus() == AsyncTask.Status.PENDING || asyncConductorSQLite.getStatus() == AsyncTask.Status.FINISHED){
            asyncConductorSQLite.execute();
        }
    }
    /* Cargando datos del conductor */

    private void limpiarDatos(boolean todos){
        if(todos){
            txtDocumento.setText("");
        }

        txtFecha.setText(Funciones.obtenerFechaActual("dd/MM/yyyy"));
        txtFechaElegida.setText(Funciones.obtenerFechaActual("yyyy-MM-dd"));
        txtHora.setText(Funciones.obtenerFechaActual("HH:mm"));
        lblEstadoConductor.setText("");
        lblNombreConductor.setText("");
        txtEmpresa.setText("");
        txtProcedencia.setText("");
        txtPlaca.setText("");
        txtNumeroOcupantes.setText("");
        txtNumeroAsientos.setText("");
        txtPlacaContenedor.setText("");
        ltEstadoConductor.setVisibility(View.GONE);
        txtDocumento.requestFocus();
        codigo_proveedor=0; codigo_ruta_transporte=0; codigo_proveedor_empresa=0;
    }

    private void llenarDatosConductor(JSONObject jsonObjectDatos){
        try{
            binding.camaraQr.close();
            String estado = jsonObjectDatos.getString("estado");
            Log.e("llenarDatos", jsonObjectDatos.toString());
            if(tipoScan){
                if(!jsonObjectDatos.has("placa")){ //si no tiene placa por algún motivo, entonces abre el dialog de editar
                    tipoScan = false;
                    llenarDatosConductor(jsonObjectDatos);
                    return;
                }

                if(btnEditarRapida.hasOnClickListeners()){
                    btnEditarRapida.setOnClickListener(null);
                }

                if(btnAceptarRapida.hasOnClickListeners()){
                    btnAceptarRapida.setOnClickListener(null);
                }

                btnEditarRapida.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        editarData = true;
                        if(dialogRapidaBottomSheet.isShowing()){
                            dialogRapidaBottomSheet.dismiss();
                        }
                        tipoScan = false;
                        llenarDatosConductor(jsonObjectDatos);
                    }
                });

                btnAceptarRapida.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String numero_ocupantes_str = txtOcupantesRapida.getText().toString();
                        if(numero_ocupantes_str.isEmpty()  || Integer.parseInt(numero_ocupantes_str) < 0){
                            Funciones.mensaje("error", contexto, "El número de ocupantes no es válido");
                            return;
                        }
                        String placa_contenedor_str = txtPlacaContenedorRapida.getText().toString();
                        if(checkContenedorRapida.isChecked()){
                            if(placa_contenedor_str.isEmpty()){
                                Funciones.mensaje("advertencia", contexto, "Por favor ingrese la placa del contenedor del vehículo");
                                return;
                            }else{
                                if(placa_contenedor_str.length() < 6){
                                    Funciones.mensaje("advertencia", contexto, "Por favor revisa la placa del contenedor, hay menos de 6 dígitos ");
                                    return;
                                }
                            }
                        }

                        boolean confirmar = Funciones.mensajeConfirmacion("pregunta", contexto, "Confirmar registro de entrada rápida");
                        try{
                            if(confirmar){
                                Fundo fundo = new Fundo();
                                fundo.setCodigo(0);
                                fundo.setFecha(Funciones.obtenerFechaActual("dd/MM/yyyy"));
                                fundo.setHora(Funciones.obtenerFechaActual("HH:mm"));
                                fundo.setDni(jsonObjectDatos.getString("dni_conductor"));
                                fundo.setNombre(jsonObjectDatos.getString("nombre_conductor"));
                                fundo.setCodigo_categoria_personal(0);
                                fundo.setNombre_empresa(jsonObjectDatos.getString("nombre_empresa"));
                                fundo.setCodigo_tipo_vehiculo(0);
                                fundo.setProcedencia(jsonObjectDatos.getString("procedencia_fundo"));
                                fundo.setNum_asientos(Integer.parseInt(jsonObjectDatos.getString("numero_asientos")));
                                fundo.setNum_ocupantes(Integer.parseInt(numero_ocupantes_str));
                                fundo.setMotivo("");
                                if(checkContenedorRapida.isChecked()){
                                    fundo.setContenedor("C");
                                    fundo.setPlaca_entrada(placa_contenedor_str);
                                }else{
                                    fundo.setContenedor("");
                                    fundo.setPlaca_entrada(jsonObjectDatos.getString("placa"));
                                }
                                fundo.setEntrada_salida("S");
                                fundo.setCodigo_empresa(Integer.parseInt(sesion.getCodigoEmpresa()));
                                fundo.setCodigo_usuario(Integer.parseInt(sesion.getCodigoUsuario()));
                                fundo.setEstado_entrada("E");
                                fundo.setHora_manual(Funciones.obtenerFechaActual("HH:mm"));
                                fundo.setCodigo_ruta(0);
                                fundo.setCodigo_entrada(0);
                                fundo.setFecha_manual(Funciones.obtenerFechaActual("dd/MM/yyyy"));
                                fundo.setSincronizado("N");
                                fundo.setCodigo_off_line(Funciones.crearCodigoOffLine(sesion.getCodigoEmpresa(), sesion.getCodigoUsuario()));
                                fundo.setCodigo_caseta(0);
                                if(jsonObjectDatos.has("codigo_proveedor")){
                                    fundo.setCodigo_proveedor(jsonObjectDatos.getInt("codigo_proveedor"));
                                }else{
                                    fundo.setCodigo_proveedor(0);
                                }
                                if(jsonObjectDatos.has("codigo_proveedor_empresa")){
                                    fundo.setCodigo_proveedor_empresa(jsonObjectDatos.getInt("codigo_proveedor_empresa"));
                                }else{
                                    fundo.setCodigo_proveedor_empresa(0);
                                }
                                if(jsonObjectDatos.has("codigo_ruta_transporte")){
                                    fundo.setCodigo_ruta_transporte(jsonObjectDatos.getInt("codigo_ruta_transporte"));
                                }else{
                                    fundo.setCodigo_ruta_transporte(0);
                                }
                                if(sesion.getPinesEmpresa().contains("[]") || sesion.getPinesEmpresa().isEmpty()){
                                    fundo.setPin("--------");
                                }else{
                                    fundo.setPin(sesion.getPinUsuario());
                                }

                                asyncSql = new RegistrarSql(fundo);
                                if(asyncSql.getStatus() == AsyncTask.Status.PENDING || asyncSql.getStatus() == AsyncTask.Status.FINISHED){
                                    asyncSql.execute();
                                }
                            }
                        }catch (JSONException|RuntimeException e){
                            Log.e("confirmarExcp", e.getLocalizedMessage());
                        }
                    }
                });


                dialogRapidaBottomSheet.show();
                lblNombreConductorRapida.setText(jsonObjectDatos.getString("nombre_conductor"));
                lblDniConductorRapida.setText(jsonObjectDatos.getString("dni_conductor"));
                switch (estado){
                    case "A":
                        lblEstadoConductorRapida.setText("ACTIVO");
                        break;
                    case "I":
                        lblEstadoConductorRapida.setText("INACTIVO");
                        break;
                    default:
                        lblEstadoConductorRapida.setText(estado);
                        break;
                }

                if(lblEstadoConductorRapida.getText().toString().equalsIgnoreCase("ACTIVO")){
                    lblEstadoConductorRapida.setTextColor(contexto.getColor(R.color.colorPrimary));
                    ltEstadoConductorRapida.setAnimation(R.raw.check);
                }else{
                    lblEstadoConductorRapida.setTextColor(contexto.getColor(R.color.salida));
                    ltEstadoConductorRapida.setAnimation(R.raw.advertencia);
                }
                lblEstadoConductorRapida.setVisibility(View.VISIBLE);
                ltEstadoConductorRapida.playAnimation();

                lblPlacaConductorRapida.setText(jsonObjectDatos.getString("placa"));

            } else{
                dialogBottomSheet.show();
                if(jsonObjectDatos.has("codigo_proveedor")){
                    codigo_proveedor = jsonObjectDatos.getInt("codigo_proveedor");
                }else{
                    codigo_proveedor = 0;
                }
                if(jsonObjectDatos.has("codigo_proveedor_empresa")){
                    codigo_proveedor_empresa =jsonObjectDatos.getInt("codigo_proveedor_empresa");
                }else{
                    codigo_proveedor_empresa = 0;
                }
                if(jsonObjectDatos.has("codigo_ruta_transporte")){
                    codigo_ruta_transporte = jsonObjectDatos.getInt("codigo_ruta_transporte");
                }else{
                    codigo_ruta_transporte = 0;
                }
                lblNombreConductor.setText(jsonObjectDatos.getString("nombre_conductor"));
                txtDocumento.setText(jsonObjectDatos.getString("dni_conductor"));

                switch (estado){
                    case "A":
                        lblEstadoConductor.setText("ACTIVO");
                        break;
                    case "I":
                        lblEstadoConductor.setText("INACTIVO");
                        break;
                    default:
                        lblEstadoConductor.setText(estado);
                        break;
                }

                if(lblEstadoConductor.getText().toString().equalsIgnoreCase("ACTIVO")){
                    lblEstadoConductor.setTextColor(ContextCompat.getColor(contexto, R.color.colorPrimary));
                    ltEstadoConductor.setAnimation(R.raw.check);
                }else{
                    lblEstadoConductor.setTextColor(ContextCompat.getColor(contexto, R.color.salida));
                    ltEstadoConductor.setAnimation(R.raw.advertencia);
                }
                ltEstadoConductor.setVisibility(View.VISIBLE);
                ltEstadoConductor.playAnimation();

                if(jsonObjectDatos.has("numero_asientos")){
                    txtNumeroAsientos.setText(jsonObjectDatos.getString("numero_asientos"));
                }else{
                    txtNumeroAsientos.setText("");
                }

                if(jsonObjectDatos.has("placa")){
                    txtPlaca.setText(jsonObjectDatos.getString("placa"));
                }else{
                    txtPlaca.setText("");
                }

                txtEmpresa.setText(jsonObjectDatos.getString("nombre_empresa"));
                txtProcedencia.setText(jsonObjectDatos.getString("procedencia_fundo"));
            }
        }catch (JSONException | RuntimeException e ){
            Log.e("llenarDAtosConductor", e.getLocalizedMessage());
        }
    }

    private boolean validarDatos(){
        if(txtDocumento.getText().toString().isEmpty()){
            Funciones.mensaje("advertencia", contexto, "Por favor ingrese el número de documento");
            return false;
        }

        if(lblNombreConductor.getText().toString().isEmpty()){
            Funciones.mensaje("advertencia", contexto, "Por favor ingrese el número de documento y busque al conductor");
            return false;
        }

        if(txtFecha.getText().toString().isEmpty()){
            Funciones.mensaje("advertencia", contexto, "Por favor ingrese la fecha");
            return false;
        }

        if(txtFechaElegida.getText().toString().isEmpty()){
            Funciones.mensaje("advertencia", contexto, "Por favor ingrese la fecha");
            return false;
        }

        if(txtHora.getText().toString().isEmpty()){
            Funciones.mensaje("advertencia", contexto, "Por favor ingrese la hora");
            return false;
        }

        if(txtEmpresa.getText().toString().isEmpty()){
            Funciones.mensaje("advertencia", contexto, "Por favor ingrese la empresa");
            return false;
        }

        if(txtProcedencia.getText().toString().isEmpty()){
            Funciones.mensaje("advertencia", contexto, "Por favor ingrese la procedencia");
            return false;
        }

        if(txtPlaca.getText().toString().isEmpty()){
            Funciones.mensaje("advertencia", contexto, "Por favor ingrese la placa del vehículo");
            return false;
        }

        if(txtNumeroAsientos.getText().toString().isEmpty() || txtNumeroAsientos.getText().toString().equalsIgnoreCase("0")){
            Funciones.mensaje("advertencia", contexto, "Por favor ingrese un número de asientos válido");
            return false;
        }

        if(txtNumeroOcupantes.getText().toString().isEmpty()){
            Funciones.mensaje("advertencia", contexto, "Por favor ingrese un número de ocupantes válido");
            return false;
        }

        if(cbContenedor.isChecked()){
            if(txtPlacaContenedor.getText().toString().isEmpty()){
                Funciones.mensaje("advertencia", contexto, "Por favor ingrese la placa del contenedor del vehículo");
                return false;
            }else{
                if(txtPlacaContenedor.getText().toString().length() < 6){
                    Funciones.mensaje("advertencia", contexto, "Por favor revisa la placa del contenedor, hay menos de 6 dígitos ");
                    return false;
                }
            }
        }

        return true;
    }

    /* Registrando entrada en SQL */
    public class RegistrarSql extends AsyncTask<Void, Void, Boolean>{
        private Fundo fundo;

        public RegistrarSql(Fundo fundo) {
            this.fundo = fundo;
        }

        @Override
        protected void onPreExecute() {
            cargando = Funciones.cargando(contexto, "Registrando salida...", "");
            limpiarDatos(true);
            btnRegistro.setEnabled(false);
            super.onPreExecute();
        }

        @Override
        protected Boolean doInBackground(Void... voids) {
            boolean resultado = false;
            try {
                resultado = this.fundo.agregarFundo();
            } catch (RuntimeException e) {
                e.printStackTrace();
                Log.e("Error sqlite", "registrarSalidaSQL: "+e.getLocalizedMessage());
            }

            return resultado;
        }

        @Override
        protected void onPostExecute(Boolean resultado) {
            try {
                if(resultado){
                    asyncSalida = new RegistrarSalida(fundo, String.valueOf(fundo.obtenerMaximoCodigoEntrada()));
                    if(asyncSalida.getStatus() == AsyncTask.Status.PENDING || asyncSalida.getStatus() == AsyncTask.Status.FINISHED){
                        asyncSalida.execute();
                    }
                }
            } catch (RuntimeException e) {
                e.printStackTrace();
            }
            super.onPostExecute(resultado);
        }
    }
    /* Registrando entrada en SQL */

    /* Registrando salida en servidor */
    public class RegistrarSalida extends AsyncTask<Void, Void, String>{
        private Fundo fundo;
        private String maximo_codigo;

        public RegistrarSalida(Fundo fundo, String maximo_codigo) {
            this.fundo = fundo;
            this.maximo_codigo = maximo_codigo;
        }

        @Override
        protected String doInBackground(Void... voids) {
            String resultado = "";
            try {
                String fecha_enviar = fundo.getFecha_manual();
                String dia = fecha_enviar.substring(0,2);
                String mes = fecha_enviar.substring(3,5);
                String anio = fecha_enviar.substring(6,10);

                String urlListado = Funciones.URL_WS + "fundo.agregar.salida.movil.php";
                HashMap parametros = new HashMap<String, String>();
                parametros.put("p_dni", this.fundo.getDni());
                parametros.put("p_placa", this.fundo.getPlaca_entrada());
                parametros.put("p_hora_manual", this.fundo.getHora_manual());
                parametros.put("p_nombre_conductor", this.fundo.getNombre());
                parametros.put("p_numero_ocupantes", String.valueOf(this.fundo.getNum_ocupantes()));
                parametros.put("p_numero_asientos", String.valueOf(this.fundo.getNum_asientos()));
                parametros.put("p_codigo_empresa_sesion", String.valueOf(this.fundo.getCodigo_empresa()));
                parametros.put("p_usuario_sesion", String.valueOf(this.fundo.getCodigo_usuario()));
                parametros.put("p_procedencia", this.fundo.getProcedencia());
                parametros.put("p_contenedor", this.fundo.getContenedor());
                parametros.put("p_nombre_empresa", this.fundo.getNombre_empresa());
                parametros.put("p_fecha_manual", anio+"-"+mes+"-"+dia);
                parametros.put("p_codigo_off_line", String.valueOf(this.fundo.getCodigo_off_line()));
                parametros.put("p_pin", String.valueOf(this.fundo.getPin()));
                parametros.put("p_maximo_codigo", this.maximo_codigo);
                parametros.put("p_codigo_proveedor", String.valueOf(this.fundo.getCodigo_proveedor()));
                parametros.put("p_codigo_ruta_transporte", String.valueOf(this.fundo.getCodigo_ruta_transporte()));
                parametros.put("p_codigo_proveedor_empresa", String.valueOf(this.fundo.getCodigo_proveedor_empresa()));
                resultado = new Funciones().getHttpContent(urlListado, parametros);
            } catch (RuntimeException e) {
                e.printStackTrace();
                Funciones.snack(fragmentoActual, contexto, "error: "+e.getLocalizedMessage(), "rojo", "");
                reanudarCamaraScan();
            }

            return resultado;
        }

        @Override
        protected void onPostExecute(String result) {
            limpiarDatos(true);
            btnRegistro.setEnabled(true);
            if(cargando.isShowing()){
                cargando.dismiss();
            }

            if(result.equalsIgnoreCase("******-----****")){
                Funciones.snack(fragmentoActual, contexto, "Registro por sincronizar", "azul", "");
                reanudarCamaraScan();
                return;
            }

            try {
                JSONObject jsonObject = new JSONObject(result);
                String mensaje = jsonObject.getString("mensaje");
                String datos = jsonObject.getString("datos");
                if(mensaje.equalsIgnoreCase("error")){
                    boolean ok = Funciones.mensaje("error", contexto, datos);
                    if(ok){
                        reanudarCamaraScan();
                    }
                    return;
                }else{
                    JSONArray jsonDatos = jsonObject.getJSONArray("datos");
                    int numeroEntradas = jsonDatos.length(), progreso = 0, porcentaje = 0;
                    Funciones.barraCargando.setProgress(0);
                    Funciones.barraCargando.showContextMenu();
                    Funciones.barraCargando.setMax(numeroEntradas);
                    Funciones.mensajito = "Sincronizando datos: 0%";

                    for (int i = 0; i < numeroEntradas; i++) {
                        JSONObject data = jsonDatos.getJSONObject(i);
                        Fundo item = new Fundo();
                        String ocupantes = data.getString("numero_ocupantes");
                        if(ocupantes.equalsIgnoreCase("") || ocupantes.isEmpty()){
                            ocupantes = "0";
                        }
                        item.setCodigo((data.getInt("codigo_entrada_fundo")));
                        item.setFecha(data.getString("fecha_entrada"));
                        item.setFecha_manual(data.getString("fecha_manual"));
                        item.setHora(data.getString("hora_entrada"));
                        item.setDni(data.getString("dni"));
                        item.setNombre(data.getString("conductor"));
                        item.setCodigo_categoria_personal((data.getInt("codigo_categoria_personal")));
                        item.setNombre_empresa(data.getString("nombre_empresa"));
                        item.setCodigo_tipo_vehiculo((data.getInt("codigo_tipo_vehiculo")));
                        item.setPlaca_entrada(data.getString("placa"));
                        item.setProcedencia(data.getString("procedencia_fundo"));
                        item.setNum_asientos(Integer.parseInt(data.getString("numero_asientos")));
                        item.setNum_ocupantes(Integer.parseInt(ocupantes));
                        item.setMotivo(data.getString("motivo_fundo"));
                        item.setContenedor(data.getString("contenedor"));
                        item.setEntrada_salida(data.getString("entrada_salida_fundo"));
                        item.setCodigo_empresa((data.getInt("codigo_empresa")));
                        item.setCodigo_usuario((data.getInt("codigo_usuario")));
                        item.setEstado_entrada(data.getString("estado_entrada_fundo"));
                        item.setHora_manual(data.getString("hora_manual"));
                        item.setCodigo_ruta(data.getInt("codigo_ruta"));
                        item.setCodigo_entrada(data.getInt("codigo_ruta"));
                        item.setSincronizado("S");
                        item.setCodigo_off_line(data.getString("codigo_off"));
                        item.setMano_obra(data.getString("mano_obra"));
                        item.setCodigo_proveedor(data.getInt("codigo_proveedor"));
                        item.setCodigo_proveedor_empresa(data.getInt("codigo_proveedor_empresa"));
                        item.setCodigo_ruta_transporte(data.getInt("codigo_ruta_transporte"));
                        item.agregarFundo();

                        JSONArray jsonVisitas = (JSONArray) data.get("visitas_lista");

                        for (int j = 0; j < jsonVisitas.length(); j++){
                            JSONObject dataVisita = jsonVisitas.getJSONObject(j);
                            if(!dataVisita.getString("dni").equalsIgnoreCase("DNI no encontrado")){
                                Visita visita = new Visita();
                                visita.setCodigo_visita(dataVisita.getInt("codigo_visita_fundo"));
                                visita.setDni(dataVisita.getString("dni"));
                                visita.setCodigo_entrada(dataVisita.getInt("codigo_entrada_fundo"));
                                visita.setComentario(dataVisita.getString("comentario"));
                                visita.setSincronizado("S");
                                visita.setCodigo_off_line("");
                                visita.agregarVisita(visita);
                            }

                        }

                        JSONArray jsonManoObra = (JSONArray) data.get("manos_obra_lista");

                        for (int j = 0; j < jsonManoObra.length(); j++){
                            JSONObject dataManosObra = jsonManoObra.getJSONObject(j);
                            ManoObraEntrada manoObra = new ManoObraEntrada();
                            manoObra.setCodigo_entrada(dataManosObra.getInt("codigo_entrada"));
                            manoObra.setCodigo_mano_obra(dataManosObra.getInt("codigo_mano_obra"));
                            manoObra.setNumero_trabajadores(dataManosObra.getInt("numero_trabajadores"));
                            manoObra.setCodigo_off_line(dataManosObra.getString("off_line"));
                            manoObra.setCodigo_off_line_fundo(dataManosObra.getString("off_line_fundo"));
                            manoObra.setSincronizado("S");
                            manoObra.agregarManoObraEntrada();
                        }

                        Funciones.barraCargando.setProgress(progreso++);
                        porcentaje = 100 * progreso / numeroEntradas;
                        porcentaje = Math.min(porcentaje, 100);
                        Funciones.mensajito = "Sincronizando datos: "+ porcentaje +"%";
                    }

                    new Fundo().eliminarNoSincronizadosAll();
                    Funciones.mensajito = "";
                    boolean ok = Funciones.mensaje("exito", contexto, "Entrada registrada");
                    if(ok){
                        reanudarCamaraScan();
                    }
                }
            } catch (JSONException e) {
                if(dialogBottomSheet.isShowing()){
                    dialogBottomSheet.dismiss();
                }
                e.printStackTrace();
            }
            super.onPostExecute(result);
        }
    }
    /* Registrando salida en servidor */

    private void reanudarCamaraScan(){
        if(dialogBottomSheet.isShowing()){
            dialogBottomSheet.dismiss();
        }
        if(dialogRapidaBottomSheet.isShowing()){
            dialogRapidaBottomSheet.dismiss();
        }
        btnRegistro.setEnabled(true);
        if(cargando.isShowing()){
            cargando.dismiss();
        }
        limpiarDatos(true);
        tipoScan = binding.swEntradaRapida.isChecked();
        binding.camaraQr.open();
        codigoDetectado = false;
        contador = 0;
    }
}
