package pe.proolmos.proolmoscontrol.Sesion;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.util.Log;

import pe.proolmos.proolmoscontrol.LoginActivity;
import pe.proolmos.proolmoscontrol.Negocio.Empresa;
import pe.proolmos.proolmoscontrol.Negocio.Fundo;
import pe.proolmos.proolmoscontrol.Negocio.ManoObra;
import pe.proolmos.proolmoscontrol.Negocio.ManoObraEntrada;
import pe.proolmos.proolmoscontrol.Negocio.Persona;
import pe.proolmos.proolmoscontrol.Negocio.Sancionado;
import pe.proolmos.proolmoscontrol.Negocio.Visita;

public class SesionPreferences {
    SharedPreferences preferencias;
    Editor editor;
    int privado = 0;
    Context proOlmos;

    private static final String NOMBRE_PREFERENCIA = "proOlmos";
    private static final String INICIO_SESION = "inicioSesion";
    private static final String TIPO_LOGIN = "tipo";
    private static final String NOMBRE_USUARIO = "nombreUsuario";
    private static final String APELLIDO_PATERNO = "paterno";
    private static final String APELLIDO_MATERNO = "materno";
    private static final String NOMBRE = "nombre";
    private static final String ESTADO = "estado";
    private static final String CODIGO_USUARIO = "codigo_usuario";
    private static final String CARGO = "cargo";
    private static final String CODIGO_EMPRESA = "codigo_empresa";
    private static final String EMPRESA = "empresa";
    private static final String SINCRONIZADO_PERSONA = "sinc_persona";
    private static final String SINCRONIZADO_ENTRADA = "sinc_entrada";
    private static final String SINCRONIZADO_GARITA = "sinc_garita";
    private static final String SINCRONIZADO_EMPRESA = "sinc_empresa";
    private static final String SINCRONIZADO_SANCIONADO = "sinc_sancionado";
    private static final String SINCRONIZADO_VISITA = "sinc_visita";
    private static final String SINCRONIZADO_TODO = "sinc_todo";
    private static final String PINES_EMPRESA = "PINES";
    private static final String PIN_USUARIO = "PIN";

    public SesionPreferences(Context context){
        this.proOlmos = context;
        preferencias = proOlmos.getSharedPreferences(NOMBRE_PREFERENCIA,privado);
        editor = preferencias.edit();
    }

    public void setPinUsuario(String pin){
        editor.putString(PIN_USUARIO, pin);
        editor.commit();
    }

    public String getPinUsuario(){
        return preferencias.getString(PIN_USUARIO,"");
    }

    public void setTipoLogin(String tipo){
        editor.putString(TIPO_LOGIN, tipo);
        editor.commit();
    }

    public String getTipoLogin(){
        return preferencias.getString(TIPO_LOGIN,"");
    }

    public void setPinesEmpresa(String pines){
        editor.putString(PINES_EMPRESA, pines);
        editor.commit();
    }

    public String getPinesEmpresa(){
        return preferencias.getString(PINES_EMPRESA, "");
    }

    public void setNombreUsuario(String nombreUsuario){
        editor.putString(NOMBRE_USUARIO, nombreUsuario);
        editor.commit();
    }

    public String getNombreUsuario(){
        return preferencias.getString(NOMBRE_USUARIO,"");
    }

    public void setApellidoPaterno(String paterno){
        editor.putString(APELLIDO_PATERNO, paterno);
        editor.commit();
    }

    public String getApellidoPaterno(){
        return preferencias.getString(APELLIDO_PATERNO,"");
    }

    public void setApellidoMaterno(String materno){
        editor.putString(APELLIDO_MATERNO, materno);
        editor.commit();
    }

    public String getApellidoMaterno(){
        return preferencias.getString(APELLIDO_MATERNO,"");
    }

    public void setNombre(String nombre){
        editor.putString(NOMBRE, nombre);
        editor.commit();
    }

    public String getNombre(){
        return preferencias.getString(NOMBRE,"");
    }

    public void setEstado(String estado){
        editor.putString(ESTADO, estado);
        editor.commit();
    }

    public String getEstado(){
        return preferencias.getString(ESTADO,"");
    }

    public void setCodigoUsuario(String codigoUsuario){
        editor.putString(CODIGO_USUARIO, codigoUsuario);
        editor.commit();
    }

    public String getCodigoUsuario(){
        return preferencias.getString(CODIGO_USUARIO,"");
    }

    public void setCargo(String cargo){
        editor.putString(CARGO, cargo);
        editor.commit();
    }

    public String getCargo(){
        return preferencias.getString(CARGO,"");
    }

    public void setCodigoEmpresa(String codigoEmpresa){
        editor.putString(CODIGO_EMPRESA, codigoEmpresa);
        editor.commit();
    }

    public String getCodigoEmpresa(){
        return preferencias.getString(CODIGO_EMPRESA, "");
    }

    public void setEmpresa(String empresa){
        editor.putString(EMPRESA, empresa);
        editor.commit();
    }

    public String getEmpresa(){
        return preferencias.getString(EMPRESA,"");
    }

    public void iniciarSesion(Boolean estado){
        editor.putBoolean(INICIO_SESION,estado);
        editor.commit();
    }

    public boolean sesionIniciada(){
        return preferencias.getBoolean(INICIO_SESION,false);
    }

    public void reenviarLogin(){
        Intent i = new Intent(proOlmos, LoginActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
        proOlmos.startActivity(i);
    }

    public void setSincronizadoEmpresa(){
        editor.putBoolean(SINCRONIZADO_EMPRESA, true);
        editor.commit();
    }

    public boolean sincronizadoEmpresa(){
        return preferencias.getBoolean(SINCRONIZADO_EMPRESA,false);
    }

    public void setSincronizadoPersona(){
        editor.putBoolean(SINCRONIZADO_PERSONA, true);
        editor.commit();
    }

    public void unSetSincronizadoPersona(){
        editor.putBoolean(SINCRONIZADO_PERSONA, false);
        editor.commit();
    }

    public boolean sincronizadoPersona(){
        return preferencias.getBoolean(SINCRONIZADO_PERSONA,false);
    }

    public void setSincronizadoEntrada(){
        editor.putBoolean(SINCRONIZADO_ENTRADA, true);
        editor.commit();
    }

    public void unSetSincronizadoEntrada(){
        editor.putBoolean(SINCRONIZADO_ENTRADA, false);
        editor.commit();
    }

    public boolean sincronizadoEntrada(){
        return preferencias.getBoolean(SINCRONIZADO_ENTRADA,false);
    }

    public void setSincronizadoSancionado(){
        editor.putBoolean(SINCRONIZADO_SANCIONADO, true);
        editor.commit();
    }

    public boolean sincronizadoSancionado(){
        return preferencias.getBoolean(SINCRONIZADO_SANCIONADO,false);
    }

    public void setSincronizadoVisita(){
        editor.putBoolean(SINCRONIZADO_VISITA, true);
        editor.commit();
    }

    public boolean sincronizadoVisita(){
        return preferencias.getBoolean(SINCRONIZADO_VISITA,false);
    }

    public void setSincronizadoGarita(){
        editor.putBoolean(SINCRONIZADO_GARITA, true);
        editor.commit();
    }

    public boolean sincronizadoGarita(){
        return preferencias.getBoolean(SINCRONIZADO_GARITA, false);
    }

    public void setSincronizadoTodo(){
        editor.putBoolean(SINCRONIZADO_TODO, true);
        editor.commit();
    }

    public boolean sincronizadoTodo(){
        return preferencias.getBoolean(SINCRONIZADO_TODO,false);
    }



    public void cerrarSesion(){
        editor.clear();
        editor.commit();
        Empresa empresa = new Empresa();
        empresa.eliminarEmpresas();

        Persona persona = new Persona();
        persona.eliminarPersonas();

        Sancionado sancionado = new Sancionado();
        sancionado.eliminarSancionados();

        Fundo fundo = new Fundo();
        fundo.eliminarDatosFundo();

        Visita visita = new Visita();
        visita.eliminarDatosVisitas();

        new ManoObraEntrada().eliminarDatosManoObraEntrada();
        new ManoObra().eliminarManoObras();
        this.reenviarLogin();
    }

    public void validarSesion(){
        if(!this.sesionIniciada()){
            reenviarLogin();
        }
    }
}