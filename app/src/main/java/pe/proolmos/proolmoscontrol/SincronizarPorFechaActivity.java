package pe.proolmos.proolmoscontrol;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import pe.proolmos.proolmoscontrol.Negocio.Fundo;
import pe.proolmos.proolmoscontrol.Negocio.Sincronizando;
import pe.proolmos.proolmoscontrol.Sesion.SesionPreferences;
import pe.proolmos.proolmoscontrol.databinding.ActivitySincronizarPorFechaBinding;
import pe.proolmos.proolmoscontrol.datos.AccesoDatos;
import pe.proolmos.proolmoscontrol.util.Funciones;

public class SincronizarPorFechaActivity extends AppCompatActivity {

    private ActivitySincronizarPorFechaBinding binding;
    SesionPreferences sesion;
    public static ArrayList<Sincronizando> listaDatos;
    SincronizandoAdapter adaptador;
    Context contexto;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this,R.layout.activity_sincronizar_por_fecha);
        Funciones.habilitarDirectivasInternet();
        contexto = this;
        sesion = new SesionPreferences(this);
        sesion.validarSesion();
        ActionBar actionBar = getSupportActionBar();
        actionBar.hide();
        AccesoDatos.aplicacion = this;

        listaDatos = new ArrayList<Sincronizando>();
        binding.rvListadoEntrada.setLayoutManager(new LinearLayoutManager(contexto));
        adaptador = new SincronizandoAdapter(contexto, listaDatos, sesion, binding.SincronizarPorFechaActivity, this);
        binding.rvListadoEntrada.setAdapter(adaptador);
        funcionalidades();
        cargarRecyclerView();
    }

    private void funcionalidades(){
        binding.btnRetroceder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SincronizarPorFechaActivity.this.finish();
            }
        });

        binding.swlListadoEntrada.setColorSchemeResources(R.color.colorPrimary, R.color.colorAccent, R.color.consultaSalida, R.color.registroEntrada);
        binding.swlListadoEntrada.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                cargarRecyclerView();
            }
        });
    }

    private void cargarRecyclerView(){
        JSONArray jsonArray = new Fundo().obtenerNoSincronizadosDia();
        int numeroEntradas = jsonArray.length();
        if(numeroEntradas == 0){
            binding.linearNoData.setVisibility(View.VISIBLE);
            binding.rvListadoEntrada.setVisibility(View.GONE);
        }else{
            binding.linearNoData.setVisibility(View.GONE);
            binding.rvListadoEntrada.setVisibility(View.VISIBLE);
            try{
                listaDatos.clear();
                for(int i=0; i<numeroEntradas; i++){
                    JSONObject data = jsonArray.getJSONObject(i);
                    Sincronizando sinc = new Sincronizando();
                    sinc.setAvance(0);
                    sinc.setPorcentaje(0);
                    sinc.setFecha(data.getString("fecha"));
                    sinc.setTotal(data.getInt("total"));
                    listaDatos.add(sinc);
                }
                binding.rvListadoEntrada.setLayoutManager(new LinearLayoutManager(contexto));
                adaptador = new SincronizandoAdapter(contexto, listaDatos, sesion, binding.SincronizarPorFechaActivity, this);
                binding.rvListadoEntrada.setAdapter(adaptador);
                adaptador.setListaDatos(listaDatos);
            }catch (RuntimeException| JSONException e){
                Log.e("runJson", e.getLocalizedMessage());
            }
        }
        binding.swlListadoEntrada.setRefreshing(false);
    }
}