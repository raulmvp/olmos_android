package pe.proolmos.proolmoscontrol;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import pe.proolmos.proolmoscontrol.Negocio.Fundo;
import pe.proolmos.proolmoscontrol.Negocio.ManoObraEntrada;
import pe.proolmos.proolmoscontrol.Negocio.Sincronizando;
import pe.proolmos.proolmoscontrol.Negocio.Visita;
import pe.proolmos.proolmoscontrol.Sesion.SesionPreferences;
import pe.proolmos.proolmoscontrol.util.Funciones;

public class SincronizandoAdapter extends RecyclerView.Adapter<SincronizandoAdapter.ViewHolder> {

    private Context contexto;
    public static ArrayList<Sincronizando> listaDatos;
    View vista;
    SesionPreferences sesion;
    View view;
    CargarListaFundos asynFundos;
    Activity activity;

    public SincronizandoAdapter(Context context, ArrayList<Sincronizando> listaDatos, SesionPreferences sesion, View view, Activity activity) {
        this.contexto = context;
        this.listaDatos = listaDatos;
        this.sesion = sesion;
        this.view = view;
        this.activity = activity;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_sincronizacion, parent, false);
        vista = v;
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    public void setListaDatos(ArrayList<Sincronizando> mlistaDatos) {
        this.listaDatos = mlistaDatos;
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        final Sincronizando item = listaDatos.get(position);
        holder.lblFecha.setText(item.getFecha());
        String total = "0/"+ item.getTotal();
        String sincronizar = "Por sincronizar: "+item.getTotal();
        holder.lblTotal.setText(sincronizar);
        holder.lblAvance.setText(total);
        holder.lblPorcentaje.setText("0%");
        holder.progressBar.setProgress(0);
        holder.progressBar.showContextMenu();
        holder.progressBar.setMax(item.getTotal());
        holder.lblSincronizar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //asyncTask que reciba los textview -> lblAvance,lblPorcentaje, lySincronizando, lblSincronizar  y el progressbar
                sincronizarEntradas(item.getFecha(), true);
            }
        });
    }

    @Override
    public int getItemCount() {
        return listaDatos.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView lblFecha, lblTotal, lblAvance, lblPorcentaje, lblSincronizar;
        ProgressBar progressBar;
        View lySincronizando;
        public ViewHolder(View itemView) {
            super(itemView);
            lblFecha = (TextView) itemView.findViewById(R.id.lblFechaASincronizar);
            lblTotal = (TextView) itemView.findViewById(R.id.lblCantidadPorSincronizar);
            lblAvance = (TextView) itemView.findViewById(R.id.lblAvance);
            lblPorcentaje = (TextView) itemView.findViewById(R.id.lblPorcentaje);
            lblSincronizar = (TextView) itemView.findViewById(R.id.lblSincronizar);
            progressBar = (ProgressBar) itemView.findViewById(R.id.progresoSincronizando);
            lySincronizando = (View) itemView.findViewById(R.id.lySincronizando);
        }
    }

    private void sincronizarEntradas(String fecha, boolean preguntar){

        if(preguntar){
            boolean ok = Funciones.mensajeConfirmacion("pregunta", contexto, contexto.getString(R.string.mensaje_sincronizar_activity_recordatorio));
            if(ok){
                asynFundos = new CargarListaFundos(fecha);
                if(asynFundos.getStatus() == AsyncTask.Status.PENDING || asynFundos.getStatus() == AsyncTask.Status.FINISHED){
                    asynFundos.execute();
                }
            }
        }else{
            if(asynFundos.getStatus() == AsyncTask.Status.RUNNING){
                asynFundos.cancel(true);
            }
            asynFundos = new CargarListaFundos(fecha);
            if(asynFundos.getStatus() == AsyncTask.Status.PENDING || asynFundos.getStatus() == AsyncTask.Status.FINISHED){
                asynFundos.execute();
            }
        }
    }


    /* Cargando fundos del servidor */

    public class CargarListaFundos extends AsyncTask<Void, Void, Integer> {

        private JSONArray detalle;
        private int maximo_codigo;
        Dialog cargando;
        private String fecha_manual;

        public CargarListaFundos(String fecha_manual) {
            this.fecha_manual = fecha_manual;
        }

        @Override
        protected void onPreExecute() {
            this.detalle = new Fundo().obtenerNoSincronizadosPorFechaManual(this.fecha_manual);
            this.maximo_codigo = new Fundo().obtenerMaximoCodigoEntrada();
            this.cargando = new Dialog(contexto);
            this.cargando = Funciones.cargando(contexto, "","sinc");
            Funciones.mensajito = contexto.getString(R.string.mensaje_comunicacion);
            super.onPreExecute();
        }

        @Override
        protected Integer doInBackground(Void... params) {
            Integer retorno = 0;
            try {
                String urlListado = Funciones.URL_WS + "fundo.sincronizar.movil.php";
                HashMap parametros = new HashMap<String, String>();
                parametros.put("p_detalle", detalle.toString());
                parametros.put("p_codigo", String.valueOf(this.maximo_codigo));
                parametros.put("p_empresa",sesion.getCodigoEmpresa());
                parametros.put("p_lista_completa", "N");
                String resultado = new Funciones().getHttpContent(urlListado, parametros);
                cargarFundos(resultado, this.cargando, this.fecha_manual, detalle);
                retorno = 1; // Satisfactoriamente
            } catch (Exception e) {
                e.printStackTrace();
            }
            return retorno;
        }

        @Override
        protected void onPostExecute(Integer retorno) {
            if (retorno == 1) {
                if(cargando.isShowing()){
                    cargando.dismiss();
                }
                if(new Fundo().obtenerNoSincronizadosPorFechaManual(fecha_manual).length() > 0){
                    asynFundos.cancel(true);
                    sincronizarEntradas(fecha_manual, false);
                    return;
                }else{
                    boolean ok = Funciones.mensaje("exito", contexto, "Genial! se han sincronizado los datos del \""+fecha_manual+"\"");
                    if(ok){
                        activity.recreate();
                    }
//                    JSONArray jsonArray = new Fundo().obtenerNoSincronizadosDia();
//                    listaDatos.clear();
//                    int numeroEntradas = jsonArray.length();
//                    try{
//                        for(int i=0; i<numeroEntradas; i++){
//                            JSONObject data = jsonArray.getJSONObject(i);
//                            Sincronizando sinc = new Sincronizando();
//                            sinc.setAvance(0);
//                            sinc.setPorcentaje(0);
//                            sinc.setFecha(data.getString("fecha"));
//                            sinc.setTotal(data.getInt("total"));
//                            listaDatos.add(sinc);
//                        }
//                    }catch (RuntimeException| JSONException e){
//                        Log.e("runtimeADa", e.getLocalizedMessage());
//                    }
//                    notifyDataSetChanged();
                }
            } else {
                Funciones.snack(view, contexto, "Parece que hubo un error", "rojo", "ok");
            }
        }
    }

    private void cargarFundos(String resultado, Dialog cargando, String fecha_manual, JSONArray listaParaSincronizar) {
        try {
            if(resultado.equalsIgnoreCase("******-----****")){
                Funciones.snack(view, contexto, contexto.getString(R.string.mensaje_sin_comunicacion), "azul", "ok");
                if(cargando.isShowing()){
                    cargando.dismiss();
                }
                asynFundos.cancel(true);
                return;
            }

            JSONObject json = new JSONObject(resultado);
            String mensaje = json.getString("mensaje");

            if(mensaje.equalsIgnoreCase("error")){
                String mensajeError = json.getString("datos");
                Funciones.mensajito = "";
                if(cargando.isShowing()){
                    cargando.dismiss();
                }
                Funciones.mensaje("error", contexto, mensajeError);
                return;
            }


            JSONArray jsonDatos = json.getJSONArray("datos");

            int numeroEntradas = jsonDatos.length(), progreso = 0, porcentaje = 0;
            Funciones.barraCargando.setProgress(0);
            Funciones.barraCargando.showContextMenu();
            Funciones.mensajito = "Sincronizando entradas y salidas: 0%";
            Funciones.barraCargando.setMax(numeroEntradas);

            sesion.unSetSincronizadoEntrada();

            for (int i = 0; i < jsonDatos.length(); i++) {
                JSONObject data = jsonDatos.getJSONObject(i);

                Fundo item = new Fundo();
                String ocupantes = data.getString("numero_ocupantes");
                if(ocupantes.equalsIgnoreCase("") || ocupantes.isEmpty()){
                    ocupantes = "0";
                }
                item.setCodigo((data.getInt("codigo_entrada_fundo")));
                item.setFecha(data.getString("fecha_entrada"));
                item.setFecha_manual(data.getString("fecha_manual"));
                item.setHora(data.getString("hora_entrada"));
                item.setDni(data.getString("dni"));
                item.setNombre(data.getString("conductor"));
                item.setCodigo_categoria_personal((data.getInt("codigo_categoria_personal")));
                item.setNombre_empresa(data.getString("nombre_empresa"));
                item.setCodigo_tipo_vehiculo((data.getInt("codigo_tipo_vehiculo")));
                item.setPlaca_entrada(data.getString("placa"));
                item.setProcedencia(data.getString("procedencia_fundo"));
                item.setNum_asientos(Integer.parseInt(data.getString("numero_asientos")));
                item.setNum_ocupantes(Integer.parseInt(ocupantes));
                item.setMotivo(data.getString("motivo_fundo"));
                item.setContenedor(data.getString("contenedor"));
                item.setEntrada_salida(data.getString("entrada_salida_fundo"));
                item.setCodigo_empresa((data.getInt("codigo_empresa")));
                item.setCodigo_usuario((data.getInt("codigo_usuario")));
                item.setEstado_entrada(data.getString("estado_entrada_fundo"));
                item.setHora_manual(data.getString("hora_manual"));
                item.setCodigo_ruta(data.getInt("codigo_ruta"));
                item.setCodigo_entrada(data.getInt("codigo_ruta"));
                item.setSincronizado("S");
                item.setCodigo_off_line(data.getString("codigo_off"));
                item.setOrientacion(data.getString("orientacion"));
                item.setMano_obra(data.getString("mano_obra"));
                item.agregarFundo();

                JSONArray jsonVisitas = (JSONArray) data.get("visitas_lista");

                for (int j = 0; j < jsonVisitas.length(); j++){
                    JSONObject dataVisita = jsonVisitas.getJSONObject(j);
                    if(!dataVisita.getString("dni").equalsIgnoreCase("DNI no encontrado")){
                        Visita visita = new Visita();
                        visita.setCodigo_visita(dataVisita.getInt("codigo_visita_fundo"));
                        visita.setDni(dataVisita.getString("dni"));
                        visita.setCodigo_entrada(dataVisita.getInt("codigo_entrada_fundo"));
                        visita.setComentario(dataVisita.getString("comentario"));
                        visita.setSincronizado("S");
                        visita.setCodigo_off_line("");
                        visita.agregarVisita(visita);
                    }
                }

                JSONArray jsonManoObra = (JSONArray) data.get("manos_obra_lista");

                for (int j = 0; j < jsonManoObra.length(); j++){
                    JSONObject dataManosObra = jsonManoObra.getJSONObject(j);
                    ManoObraEntrada manoObra = new ManoObraEntrada();
                    manoObra.setCodigo_entrada(dataManosObra.getInt("codigo_entrada"));
                    manoObra.setCodigo_mano_obra(dataManosObra.getInt("codigo_mano_obra"));
                    manoObra.setNumero_trabajadores(dataManosObra.getInt("numero_trabajadores"));
                    manoObra.setCodigo_off_line(dataManosObra.getString("off_line"));
                    manoObra.setCodigo_off_line_fundo(dataManosObra.getString("off_line_fundo"));
                    manoObra.setSincronizado("S");
                    manoObra.agregarManoObraEntrada();
                }

                Funciones.barraCargando.setProgress(progreso++);
                porcentaje = 100 * progreso / numeroEntradas;
                Funciones.mensajito = "Sincronizando entradas y salidas: "+ porcentaje +"%";
            }

            new Fundo().eliminarNoSincronizados(listaParaSincronizar);
            sesion.setSincronizadoEntrada();
        }catch (RuntimeException|JSONException e){
            Log.e("excp", e.getLocalizedMessage());
        }
    }

    /* Cargando fundos del servidor */

}
