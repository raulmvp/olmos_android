package pe.proolmos.proolmoscontrol.util;

import android.annotation.TargetApi;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkCapabilities;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.StrictMode;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;

import com.airbnb.lottie.LottieAnimationView;
import com.google.android.material.snackbar.Snackbar;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.net.ssl.HttpsURLConnection;

import androidx.annotation.IntRange;
import androidx.core.content.ContextCompat;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import pe.proolmos.proolmoscontrol.Negocio.DetalleTransporte;
import pe.proolmos.proolmoscontrol.Negocio.ManoObra;
import pe.proolmos.proolmoscontrol.Negocio.Proveedor;
import pe.proolmos.proolmoscontrol.Negocio.ProveedorEmpresa;
import pe.proolmos.proolmoscontrol.Negocio.Vehiculo;
import pe.proolmos.proolmoscontrol.R;
import pe.proolmos.proolmoscontrol.Sesion.SesionPreferences;

public class Funciones {
    //URL de conexión a través del localhost + ngrok
//    public static final String host = "https://8a05f6e3d5da.ngrok.io";
//    public static final String URL_WS = host+"/chino-server/movil.proolmos.pe/controlador/";
    //URL de conexión a través del localhost + ngrok
    //URL de conexión con google
    public static final String URL_WS = "https://movil.proolmos.pe/controlador/";
//    public static final String URL_WS = "https://movil.dev.proolmos.pe/controlador/";
    //URL de conexión con google
    public String getHttpContent(String requestURL, HashMap<String, String> postDataParams) {

        URL url;
        String response = "";
        try {
            url = new URL(requestURL);

            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(110000);
            conn.setConnectTimeout(110000);
            conn.setRequestMethod("POST");
            conn.setDoInput(true);
            conn.setDoOutput(true);

            OutputStream os = conn.getOutputStream();

            if (postDataParams != null){
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                writer.write(getPostDataString(postDataParams));
                writer.flush();
                writer.close();
            }
            os.close();
            int responseCode=conn.getResponseCode();
            System.out.println("RRRR: " + responseCode);
            if (responseCode == HttpsURLConnection.HTTP_OK) {
                String line;
                BufferedReader br=new BufferedReader(new InputStreamReader(conn.getInputStream(), "iso-8859-1"), 1024);
                while ((line=br.readLine()) != null) {
                    response+=line;
                }
            } else {
                response="******-----****";
            }

        }catch (FileNotFoundException e2){
            e2.printStackTrace();
        } catch (Exception e) {
            response="******-----****";
            return response;
        }
        return response;
    }

    private String getPostDataString(HashMap<String, String> params) throws UnsupportedEncodingException {
        StringBuilder result = new StringBuilder();
        boolean first = true;
        for(Map.Entry<String, String> entry : params.entrySet()){
            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(entry.getValue(), "UTF-8"));
        }

        return result.toString();
    }

    @IntRange(from = 0, to = 3)
    public static int obtenerTipoConexion(Context context) {
        int result = 0; // Retorna el valor de la conexión-> 0: ninguna; 1: datos móviles; 2: wifi
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (cm != null) {
                NetworkCapabilities capabilities = cm.getNetworkCapabilities(cm.getActiveNetwork());
                if (capabilities != null) {
                    if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI)) {
                        result = 2;
                    } else if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR)) {
                        result = 1;
                    } else if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_VPN)) {
                        result = 3;
                    }
                }
            }
        } else {
            if (cm != null) {
                NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
                if (activeNetwork != null) {
                    // connected to the internet
                    if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI) {
                        result = 2;
                    } else if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE) {
                        result = 1;
                    } else if (activeNetwork.getType() == ConnectivityManager.TYPE_VPN) {
                        result = 3;
                    }
                }
            }
        }
        String tipoConexion = "";
        switch (result){
            case 0:
                tipoConexion = "No hay conexion";
                break;
            case 1:
                tipoConexion = "Datos móviles";
                break;
            case 2:
                tipoConexion = "Wifi";
                break;
            default:
                tipoConexion = "VPN";
                break;
        }
        return result;
    }


    public static String crearCodigoOffLine(String codigo_empresa, String codigo_usuario){
        long time= System.currentTimeMillis();
        return toMd5(String.valueOf(time + codigo_empresa + codigo_usuario));
    }

    public static String obtenerFechaActual(String formato){
        Date fecha = new Date();
        SimpleDateFormat formatoFecha = new SimpleDateFormat(formato);
        return formatoFecha.format(fecha);
    }

    public static String formatearNumero(double numero){
        DecimalFormatSymbols simbolos = new DecimalFormatSymbols();
        simbolos.setDecimalSeparator('.');
        simbolos.setGroupingSeparator(',');

        DecimalFormat formato = new DecimalFormat("###,###.00", simbolos);

        return formato.format(numero);
    }

    public static boolean isOnlineNet() {
        String link = "http://www.google.com.pe";
        URL url = null;
        try {
            url = new URL(link);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.connect();

            int resCode = conn.getResponseCode();

            if (resCode == 200){
                return true;
            }
            //System.out.println("Response code:===========" + resCode);
        } catch (MalformedURLException e) {
            return false;
            //e.printStackTrace();
        }catch (IOException e2){
            //System.out.println(e2.);
            return false;
        }
        return false;

    }

    public static String decodeBase64(String coded){
        byte[] valueDecoded= new byte[0];
        try {
            valueDecoded = Base64.decode(coded.getBytes("UTF-8"), Base64.DEFAULT);
        } catch (UnsupportedEncodingException e) {
        }
        return new String(valueDecoded);
    }

    public static String toMd5(String pass) {
        String password = null;
        MessageDigest mdEnc;
        try {
            mdEnc = MessageDigest.getInstance("MD5");
            mdEnc.update(pass.getBytes(), 0, pass.length());
            pass = new BigInteger(1, mdEnc.digest()).toString(16);
            while (pass.length() < 32) {
                pass = "0" + pass;
            }
            password = pass;
        } catch (NoSuchAlgorithmException e1) {
            e1.printStackTrace();
        }
        return password;
    }

    public static void habilitarDirectivasInternet(){
        /*Si la versión del SO es mayor a la versión de GINGERBARD, entonces habilita una politica especial para conectarse a internet*/
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.GINGERBREAD) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
        /*Si la versión del SO es mayor a la versión de GINGERBARD, entonces habilita una politica especial para conectarse a internet*/
    }

    public static boolean validarEmail(String email) {
        String emailPattern = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$";
        Pattern pattern = Pattern.compile(emailPattern);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    private static boolean mResult;
    private static String mDate, mSelectedDate;

   public static boolean mensajeConfirmacion(String tipo, final Context context, String mensaje){

       final Handler handler = new Handler() {
           @Override
           public void handleMessage(Message mesg) {
               throw new RuntimeException();
           }
       };

        final Dialog myDialog = new Dialog(context);
        TextView txtMensajeConfirmacion;
        Button btnAceptarConfirmacion, btnCancelarAdvertencia;
        LottieAnimationView animacion;

        myDialog.setContentView(R.layout.mensaje_confirmacion);
        txtMensajeConfirmacion = (TextView) myDialog.findViewById(R.id.txtMensajeConfirmacion);
        txtMensajeConfirmacion.setText(mensaje);
        btnAceptarConfirmacion = (Button) myDialog.findViewById(R.id.btnAceptarConfirmacion);
        btnCancelarAdvertencia = (Button) myDialog.findViewById(R.id.btnCancelarAdvertencia);
        animacion = (LottieAnimationView) myDialog.findViewById(R.id.ltTipoConfirmacion);

        switch (tipo){
            case "pregunta":
                animacion.setAnimation(R.raw.pregunta);
                animacion.loop(false);
                break;
            case "sesion":
                animacion.loop(true);
                //animacion.setAnimation(R.raw.cerrar_lock);
                break;
        }

        btnAceptarConfirmacion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mResult = true;
                handler.sendMessage(handler.obtainMessage());
                myDialog.dismiss();
            }
        });

        btnCancelarAdvertencia.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mResult = false;
                handler.sendMessage(handler.obtainMessage());
                myDialog.dismiss();
            }
        });

        myDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                mResult = false;
                handler.sendMessage(handler.obtainMessage());
                myDialog.dismiss();
            }
        });

        myDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        myDialog.setCanceledOnTouchOutside(false);
        myDialog.setCancelable(false);
        myDialog.show();

        try {
            Looper.loop();
        }
        catch(RuntimeException e2) {

        }

        return mResult;
    }

    public static ProgressBar barraCargando;
    public static String mensajito = "";
    static Handler handler = new Handler();
    static int delay = 1000; //milliseconds

    public static Dialog cargando (final Context context, String mensaje, final String tipo){
        final Dialog dialog = new Dialog(context);

        LottieAnimationView img;
        final TextView txtMensaje;

        dialog.setContentView(R.layout.loading);
        img = (LottieAnimationView) dialog.findViewById(R.id.ltCargandoLoading);
        txtMensaje = (TextView) dialog.findViewById(R.id.txtMensajeLoading);
        barraCargando = (ProgressBar) dialog.findViewById(R.id.progressBarLoading);

        if(!mensaje.isEmpty()){
            mensajito = mensaje;
            txtMensaje.setText(mensajito);
        }

        handler.postDelayed(new Runnable(){
            public void run(){
                txtMensaje.setText(mensajito);
                handler.postDelayed(this, delay);
            }
        }, delay);

        if(!tipo.isEmpty()){
            img.setAnimation(R.raw.sincronizando);
            barraCargando.setProgress(0);
            barraCargando.setVisibility(View.VISIBLE);
        }else{
            barraCargando.setVisibility(View.GONE);
        }

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);

        if(!dialog.isShowing()){
            dialog.show();
        }
        return dialog;
    }

    public static boolean calendario(final Context context, final TextView txtFecha, final TextView txtFechaElegida, String fecha_inicio){
        final Handler handler = new Handler() {
            @Override
            public void handleMessage(Message mesg) {
                throw new RuntimeException();
            }
        };

        final Dialog myDialog = new Dialog(context);
        Button btnAceptar;
        final DatePicker picker;

        myDialog.setContentView(R.layout.dialog_calendario);
        btnAceptar = (Button) myDialog.findViewById(R.id.btnAceptar);
        picker = (DatePicker) myDialog.findViewById(R.id.dpCalendar);

        if(!fecha_inicio.isEmpty()){
            int mesInicio = Integer.parseInt(fecha_inicio.substring(3,5)) - 1;
            picker.init(Integer.parseInt(fecha_inicio.substring(6)), mesInicio, Integer.parseInt(fecha_inicio.substring(0,2)), new DatePicker.OnDateChangedListener() {
                @Override
                public void onDateChanged(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                    String mes, dia;
                    int mesSele = picker.getMonth()+1, diaSele = picker.getDayOfMonth();

                    if(mesSele <= 9){
                        mes = "0"+mesSele;
                    }else{
                        mes = String.valueOf(mesSele);
                    }

                    if(diaSele <= 9){
                        dia = "0"+diaSele;
                    }else {
                        dia = String.valueOf(diaSele);
                    }

                    mDate = dia + "/" + mes + "/" + picker.getYear();
                    mSelectedDate = picker.getYear() + "-" + mes + "-" + dia;
                    txtFecha.setText(mDate);
                    txtFechaElegida.setText(mSelectedDate);
                }
            });
        }

        btnAceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String mes, dia;
                int mesSele = picker.getMonth()+1, diaSele = picker.getDayOfMonth();

                if(mesSele <= 9){
                    mes = "0"+mesSele;
                }else{
                    mes = String.valueOf(mesSele);
                }

                if(diaSele <= 9){
                    dia = "0"+diaSele;
                }else {
                    dia = String.valueOf(diaSele);
                }

                mDate = dia + "/" + mes + "/" + picker.getYear();
                mSelectedDate = picker.getYear() + "-" + mes + "-" + dia;
                txtFechaElegida.setText(mSelectedDate);
                txtFecha.setText(mDate);
                mResult = true;
                handler.sendMessage(handler.obtainMessage());
                myDialog.dismiss();
            }
        });

        myDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        myDialog.setCanceledOnTouchOutside(false);
        myDialog.setCancelable(false);
        myDialog.show();

        try {
            Looper.loop();
        }
        catch(RuntimeException e2) {

        }

        return mResult;
    }

    public static boolean reloj(final Context context, final TextView txtHora){
        final Handler handler = new Handler() {
            @Override
            public void handleMessage(Message mesg) {
                throw new RuntimeException();
            }
        };

        final Dialog myDialog = new Dialog(context);
        Button btnAceptar;
        final TimePicker picker;

        myDialog.setContentView(R.layout.dialog_reloj);
        btnAceptar = (Button) myDialog.findViewById(R.id.btnAceptar);
        picker = (TimePicker) myDialog.findViewById(R.id.tpReloj);

        btnAceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String hora, minuto;
                int horaSele = picker.getHour(), minSele = picker.getMinute();
                if(horaSele <= 9){
                    hora = "0"+horaSele;
                }else{
                    hora = String.valueOf(horaSele);
                }

                if(minSele <= 9){
                    minuto = "0"+minSele;
                }else{
                    minuto = String.valueOf(minSele);
                }

                mDate = hora + ":" + minuto;
                txtHora.setText(mDate);
                mResult = true;
                handler.sendMessage(handler.obtainMessage());
                myDialog.dismiss();
            }
        });

        myDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        myDialog.setCanceledOnTouchOutside(false);
        myDialog.setCancelable(false);
        myDialog.show();

        try {
            Looper.loop();
        }
        catch(RuntimeException e2) {

        }

        return mResult;
    }

    public static boolean mensaje(final String tipo, final Context context, String mensaje){
        final Handler handler = new Handler() {
            @Override
            public void handleMessage(Message mesg) {
                throw new RuntimeException();
            }
        };

        final Dialog myDialog = new Dialog(context);
        TextView txtMensaje;
        Button btnAceptar;
        LottieAnimationView img;

        myDialog.setContentView(R.layout.dialog_mensaje);
        img = (LottieAnimationView)myDialog.findViewById(R.id.imgTipoMensaje);

        switch (tipo){
            case "error":
                img.setAnimation(R.raw.error);
                break;
            case "exito":
                img.setAnimation(R.raw.check);
                break;
            case "advertencia":
                img.setAnimation(R.raw.advertencia);
                break;
            case "info":
                img.setAnimation(R.raw.info);
                break;

        }
        txtMensaje = (TextView) myDialog.findViewById(R.id.txtMensaje);
        txtMensaje.setText(mensaje);
        btnAceptar = (Button) myDialog.findViewById(R.id.btnAceptar);


        btnAceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mResult = true;
                handler.sendMessage(handler.obtainMessage());
                myDialog.dismiss();
            }
        });

        myDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        myDialog.setCanceledOnTouchOutside(false);
        myDialog.setCancelable(false);
        myDialog.show();

        try {
            Looper.loop();
        }
        catch(RuntimeException e2) {

        }

        return mResult;
    }

    public static void selectedItemSpinner(Spinner spinner, String itemSelection){
        int position = 0;
        for (int i=0; i<spinner.getCount(); i++){
            if (spinner.getItemAtPosition(i).equals(itemSelection)){
                position = i;
                break;
            }
        }
        spinner.setSelection(position);
    }

    public static void snack(View vista, Context context, String mensaje, String color, String tipo){
        try {
            Snackbar snackbar;
            switch (tipo){
                case "ok":
                    snackbar = Snackbar.make(vista, mensaje, Snackbar.LENGTH_INDEFINITE).setAction(android.R.string.ok, new View.OnClickListener() {
                        @TargetApi(Build.VERSION_CODES.M)
                        @Override
                        public void onClick(View v) {

                        }
                    });
                    break;

                default:
                    snackbar = Snackbar.make(vista, mensaje, Snackbar.LENGTH_SHORT);
                    break;
            }

            View snackBarView = snackbar.getView();
            TextView textView = (TextView) snackBarView.findViewById(com.google.android.material.R.id.snackbar_text);
            textView.setTextColor(ContextCompat.getColor(context, R.color.colorBlanco));
            textView.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
            TextView okButton = (TextView)snackBarView.findViewById(com.google.android.material.R.id.snackbar_action);
            okButton.setTextColor(ContextCompat.getColor(context, R.color.colorBlanco));

            switch (color){
                case "azul":
                    snackBarView.setBackgroundColor(ContextCompat.getColor(context,R.color.consultaEntrada));
                    break;
                case "rojo":
                    snackBarView.setBackgroundColor(ContextCompat.getColor(context, R.color.salida));
                    break;
                case "verde":
                    snackBarView.setBackgroundColor(ContextCompat.getColor(context, R.color.colorPrimary));
                    break;
                case "amarillo":
                    snackBarView.setBackgroundColor(ContextCompat.getColor(context, R.color.consultaSalida));
                    break;
            }
            snackbar.show();
        }catch (RuntimeException e){

        }
    }

    /*renderizar la imagen*/
    public static Bitmap renderizarImagen(Resources res, int resId,
                                          int reqWidth, int reqHeight) {

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(res, resId, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeResource(res, resId, options);
    }

    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }
    /*renderizar la imagen*/

    public static ArrayList<ManoObra> listaManoObra = new ArrayList<ManoObra>();

    /* Cargar lista mano obra */
    public static void cargarManoObra(SesionPreferences sesion){

        class CargarManoObra extends AsyncTask<Void, Void, Integer> {
            @Override
            protected Integer doInBackground(Void... params) {
                Integer retorno = 0;
                try {
                    String urlListado = URL_WS + "fundo.cargar.mano.obra.movil.controlador.php";
                    HashMap parametros = new HashMap<String, String>();
                    parametros.put("codigo_empresa", sesion.getCodigoEmpresa());
                    String resultado = new Funciones().getHttpContent(urlListado, parametros);
                    cargarManosDeObra(resultado);
                    retorno = 1; //Satisfactoriamente
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return retorno;
            }

            @Override
            protected void onPostExecute(Integer retorno) {
                if(retorno != 1){
                    cargarManoObraSqlite();
                }
            }
        }

        new CargarManoObra().execute();
    }

    private static  void cargarManoObraSqlite(){
        listaManoObra.clear();
        listaManoObra = new ManoObra().cargarDatosManoObra(listaManoObra);
    }
    private static void cargarManosDeObra(String resultado) {
        try {
            if(resultado.equalsIgnoreCase("******-----****")){
                cargarManoObraSqlite();
                return;
            }

            JSONObject json = new JSONObject(resultado);
            String mensaje = json.getString("mensaje");

            if(mensaje.equalsIgnoreCase("error")){
                String mensajeError = json.getString("datos");
                Log.e("errorManosObras", mensajeError);
                cargarManoObraSqlite();
                return;
            }

            JSONArray jsonArray = json.getJSONArray("datos");
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonData = jsonArray.getJSONObject(i);
                ManoObra item = new ManoObra();
                item.setCodigo(jsonData.getInt("codigo_tipo_mano_obra"));
                item.setNombre(jsonData.getString("man_obra"));
                item.setEstado(jsonData.getString("estado"));
                item.agregarManoObra();
            }

            cargarManoObraSqlite();

        }catch (Exception e){
            Log.e("Error: " , e.getMessage());
        }
    }
    /* Cargar lista mano obra */

    /* Cargar lista pines */
    public static void cargarPines(SesionPreferences sesion){

        class CargarManoObra extends AsyncTask<Void, Void, Integer> {
            @Override
            protected Integer doInBackground(Void... params) {
                Integer retorno = 0;
                try {
                    String urlListado = URL_WS + "fundo.cargar.pines.movil.controlador.php";
                    HashMap parametros = new HashMap<String, String>();
                    parametros.put("codigo_empresa", sesion.getCodigoEmpresa());
                    String resultado = new Funciones().getHttpContent(urlListado, parametros);
                    cargarPines(resultado, sesion);
                    retorno = 1; //Satisfactoriamente
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return retorno;
            }
        }

        new CargarManoObra().execute();
    }

    private static void cargarPines(String resultado, SesionPreferences sesion) {
        try {
            if(resultado.equalsIgnoreCase("******-----****")){
                return;
            }

            JSONObject json = new JSONObject(resultado);
            String mensaje = json.getString("mensaje");

            if(mensaje.equalsIgnoreCase("error")){
                return;
            }

            JSONArray jsonArray = json.getJSONArray("datos");
            sesion.setPinesEmpresa(String.valueOf(jsonArray));
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonData = jsonArray.getJSONObject(i);
            }
        }catch (Exception e){
            Log.e("Error: " , e.getMessage());
        }
    }
    /* Cargar lista pines */



    /* Cargar lista Vehículos */
    public static void descargarVehiculos(SesionPreferences sesion){

        class CargarVehiculos extends AsyncTask<Void, Void, Integer> {
            @Override
            protected Integer doInBackground(Void... params) {
                Integer retorno = 0;
                try {
                    String urlListado = URL_WS + "fundo.cargar.vehiculos.movil.controlador.php";
                    HashMap parametros = new HashMap<String, String>();
                    parametros.put("codigo_empresa", sesion.getCodigoEmpresa());
                    String resultado = new Funciones().getHttpContent(urlListado, parametros);
                    Log.e("descargando", "vehiculos");
                    cargarVehiculos(resultado);
                    retorno = 1; //Satisfactoriamente
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return retorno;
            }

            @Override
            protected void onPostExecute(Integer retorno) {
                Log.e("descargando", "vehiculos finalizado");
            }
        }

        new CargarVehiculos().execute();
    }

    private static void cargarVehiculos(String resultado) {
        try {
            if(resultado.equalsIgnoreCase("******-----****")){
                return;
            }

            JSONObject json = new JSONObject(resultado);
            String mensaje = json.getString("mensaje");

            if(mensaje.equalsIgnoreCase("error")){
                String mensajeError = json.getString("datos");
                Log.e("cargarVehiculos", mensajeError);
                return;
            }

            JSONArray jsonArray = json.getJSONArray("datos");
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonData = jsonArray.getJSONObject(i);
                Vehiculo item = new Vehiculo();
                item.setCodigo_vehiculo(jsonData.getInt("codigo_vehiculo"));
                item.setCodigo_tipo_vehiculo(jsonData.getInt("codigo_tipo_vehiculo"));
                item.setCapacidad(jsonData.getInt("capacidad"));
                item.setPlaca(jsonData.getString("placa"));
                item.setEstado(jsonData.getString("estado"));
                item.agregarVehiculo();
            }

        }catch (Exception e){
            Log.e("Error: " , e.getMessage());
        }
    }
    /* Cargar lista Vehículos */

    /* Cargar lista ProveedorEmpresa */
    public static void descargarProveedorEmpresa(SesionPreferences sesion){

        class CargarProveedorEmpresa extends AsyncTask<Void, Void, Integer> {
            @Override
            protected Integer doInBackground(Void... params) {
                Integer retorno = 0;
                try {
                    String urlListado = URL_WS + "fundo.cargar.proveedor.empresa.movil.controlador.php";
                    HashMap parametros = new HashMap<String, String>();
                    parametros.put("codigo_empresa", sesion.getCodigoEmpresa());
                    String resultado = new Funciones().getHttpContent(urlListado, parametros);
                    Log.e("descargando", "proveedor empresa");
                    cargarProveedorEmpresa(resultado);
                    retorno = 1; //Satisfactoriamente
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return retorno;
            }

            @Override
            protected void onPostExecute(Integer retorno) {
                Log.e("descargando", "proveedor empresa finalizado");
            }
        }

        new CargarProveedorEmpresa().execute();
    }

    private static void cargarProveedorEmpresa(String resultado) {
        try {
            if(resultado.equalsIgnoreCase("******-----****")){
                return;
            }

            JSONObject json = new JSONObject(resultado);
            String mensaje = json.getString("mensaje");

            if(mensaje.equalsIgnoreCase("error")){
                String mensajeError = json.getString("datos");
                Log.e("cargarProveedorEmpresa", mensajeError);
                return;
            }

            JSONArray jsonArray = json.getJSONArray("datos");
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonData = jsonArray.getJSONObject(i);
                ProveedorEmpresa item = new ProveedorEmpresa();
                item.setCodigo_proveedor_empresa(jsonData.getInt("codigo_proveedor_empresa"));
                item.setCodigo_proveedor(jsonData.getInt("codigo_proveedor"));
                item.setCodigo_conductor(jsonData.getInt("codigo_conductor"));
                item.setCodigo_vehiculo(jsonData.getInt("codigo_vehiculo"));
                item.setCodigo_ruta_transporte(jsonData.getInt("codigo_ruta_transporte"));
                item.setEstado(jsonData.getString("estado"));
                item.agregarProveedorEmpresa();
            }

        }catch (Exception e){
            Log.e("Error: " , e.getMessage());
        }
    }
    /* Cargar lista ProveedorEmpresa */

    /* Cargar lista proveedor */
    public static void descargarProveedor(SesionPreferences sesion){

        class CargarProveedor extends AsyncTask<Void, Void, Integer> {
            @Override
            protected Integer doInBackground(Void... params) {
                Integer retorno = 0;
                try {
                    String urlListado = URL_WS + "fundo.cargar.proveedor.movil.controlador.php";
                    HashMap parametros = new HashMap<String, String>();
                    parametros.put("codigo_empresa", sesion.getCodigoEmpresa());
                    String resultado = new Funciones().getHttpContent(urlListado, parametros);
                    Log.e("descargando", "proveedor");
                    cargarProveedor(resultado);
                    retorno = 1; //Satisfactoriamente
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return retorno;
            }

            @Override
            protected void onPostExecute(Integer retorno) {
                Log.e("descargando", "proveedor finalizado");
            }
        }

        new CargarProveedor().execute();
    }

    private static void cargarProveedor(String resultado) {
        try {
            if(resultado.equalsIgnoreCase("******-----****")){
                return;
            }

            JSONObject json = new JSONObject(resultado);
            String mensaje = json.getString("mensaje");

            if(mensaje.equalsIgnoreCase("error")){
                String mensajeError = json.getString("datos");
                Log.e("cargarProveedor", mensajeError);
                return;
            }

            JSONArray jsonArray = json.getJSONArray("datos");
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonData = jsonArray.getJSONObject(i);
                Proveedor item = new Proveedor();
                item.setCodigo_proveedor(jsonData.getInt("codigo_proveedor"));
                item.setRuc(jsonData.getString("ruc"));
                item.setRazon_social(jsonData.getString("razon_social"));
                item.agregarProveedor();
            }

        }catch (Exception e){
            Log.e("Error: " , e.getMessage());
        }
    }
    /* Cargar lista proveedor */


    /* Cargar lista detalle transporte */
    public static void descargarTransporte(SesionPreferences sesion){

        class CargarDetalleTransporte extends AsyncTask<Void, Void, Integer> {
            @Override
            protected Integer doInBackground(Void... params) {
                Integer retorno = 0;
                try {
                    String urlListado = URL_WS + "fundo.cargar.detalle.transporte.movil.controlador.php";
                    HashMap parametros = new HashMap<String, String>();
                    parametros.put("codigo_empresa", sesion.getCodigoEmpresa());
                    String resultado = new Funciones().getHttpContent(urlListado, parametros);
                    Log.e("descargando", "detalle transporte");
                    cargarDetalleTransporte(resultado);
                    retorno = 1; //Satisfactoriamente
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return retorno;
            }

            @Override
            protected void onPostExecute(Integer retorno) {
                Log.e("descargando", "detalle transporte finalizado");
            }
        }

        new CargarDetalleTransporte().execute();
    }

    private static void cargarDetalleTransporte(String resultado) {
        try {
            if(resultado.equalsIgnoreCase("******-----****")){
                return;
            }

            JSONObject json = new JSONObject(resultado);
            String mensaje = json.getString("mensaje");

            if(mensaje.equalsIgnoreCase("error")){
                String mensajeError = json.getString("datos");
                Log.e("cargarDetalleTransporte", mensajeError);
                return;
            }

            JSONArray jsonArray = json.getJSONArray("datos");
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonData = jsonArray.getJSONObject(i);
                DetalleTransporte item = new DetalleTransporte();
                item.setCodigo_detalle_transporte(jsonData.getInt("codigo_detalle_transporte"));
                item.setCodigo_conductor(jsonData.getInt("codigo_conductor"));
                item.setCodigo_vehiculo(jsonData.getInt("codigo_vehiculo"));
                item.setEstado(jsonData.getString("estado"));
                item.agregarDetalleTransporte();
            }

        }catch (Exception e){
            Log.e("Error: " , e.getMessage());
        }
    }
    /* Cargar lista detalle transporte */
}