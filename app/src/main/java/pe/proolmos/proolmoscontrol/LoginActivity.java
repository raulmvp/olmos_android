package pe.proolmos.proolmoscontrol;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ScrollView;
import android.widget.Spinner;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.widget.NestedScrollView;
import androidx.databinding.DataBindingUtil;
import pe.proolmos.proolmoscontrol.Negocio.Empresa;
import pe.proolmos.proolmoscontrol.Sesion.SesionPreferences;
import pe.proolmos.proolmoscontrol.databinding.ActivityLoginBinding;
import pe.proolmos.proolmoscontrol.datos.AccesoDatos;
import pe.proolmos.proolmoscontrol.util.Funciones;

import static android.view.View.INVISIBLE;
import static android.view.View.VISIBLE;


public class LoginActivity extends AppCompatActivity {

    ArrayList<Empresa> listaEmpresa;

    private boolean cargandoEmpresas;
    public static Dialog cargando;
    CargarListaEmpresas asyncEmpresas;

    private ActivityLoginBinding mBinding;
    ScrollView scroll;
    FrameLayout btn;
    Animation arribaAbajo,derechaIzquierda,abajoArriba, fadeOut, fadeIn;
    NestedScrollView lyLogin;

    Dialog myDialog;
    View rootView;
    EditText txtEmailLogin, txtClaveLogin;
    String result;
    Spinner spEmpresas;

    SesionPreferences sesion;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        mBinding = DataBindingUtil.setContentView(this,R.layout.activity_login);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        Funciones.habilitarDirectivasInternet();

        sesion = new SesionPreferences(LoginActivity.this);

        if(sesion.sesionIniciada()){
            Intent sesionIniciada;
            if(sesion.sincronizadoTodo()){
                sesionIniciada = new Intent(LoginActivity.this,MainActivity.class);
            }else{
                sesionIniciada = new Intent(LoginActivity.this,OnBoardingActivity.class);
            }
            startActivity(sesionIniciada);
            this.finish();
        }


        AccesoDatos.aplicacion = this;

        cargandoEmpresas = false;

        mBinding.logoLogin.setImageBitmap(Funciones.renderizarImagen(getResources(),R.drawable.logo_blanco,150,150));

        scroll = (ScrollView)findViewById(R.id.scroll1);
        btn = (FrameLayout)findViewById(R.id.btnIniciarSesion);
        lyLogin = (NestedScrollView)findViewById(R.id.LyLogin);

        /*Animaciones de entrada del login*/
        arribaAbajo= AnimationUtils.loadAnimation(this,R.anim.dearribaabajocompleto);
        derechaIzquierda = AnimationUtils.loadAnimation(this,R.anim.derechaizquierda);
        abajoArriba = AnimationUtils.loadAnimation(this,R.anim.deabajoarribacompleto);
        fadeOut = AnimationUtils.loadAnimation(this, R.anim.fadeout);
        fadeIn = AnimationUtils.loadAnimation(this, R.anim.fadein);

        scroll.setAnimation(arribaAbajo);
        btn.setAnimation(derechaIzquierda);
        fadeOut.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                lyLogin.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        /*Animaciones de entrada del login*/

        myDialog = new Dialog(this);
        rootView = (View) findViewById(R.id.activityLogin);
        txtEmailLogin = (EditText) findViewById(R.id.txtEmailLogin);
        txtClaveLogin = (EditText) findViewById(R.id.txtClaveLogin);
        spEmpresas = (Spinner) findViewById(R.id.spEmpresa);

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(txtEmailLogin.getText().toString().isEmpty()){
                    txtEmailLogin.requestFocus();
                    txtEmailLogin.setError("Ingrese su usuario");
                    return;
                }
                if(txtClaveLogin.getText().toString().isEmpty()){
                    txtClaveLogin.requestFocus();
                    txtClaveLogin.setError("Ingrese su contraseña");
                    return;
                }
                cargar(view);
                new UserLoginTask().execute();
            }
        });

        int tipoConexion = Funciones.obtenerTipoConexion(LoginActivity.this);
        if(tipoConexion > 0){
            cargando = new Dialog(LoginActivity.this);
            asyncEmpresas = new CargarListaEmpresas();
            descargarDatosEmpresa();
        }else{
            Funciones.snack(rootView, LoginActivity.this, "No hay conexión con nuestro servidor.", "rojo", "");
        }
    }

    /*Animación botón iniciar sesión*/
    public void cargar(View view) {
        animateButtonWidth();
        fadeOutTextAndShowProgressDialog();
    }

    private void animateButtonWidth() {
        ValueAnimator anim = ValueAnimator.ofInt(mBinding.btnIniciarSesion.getMeasuredWidth(), getFabWidth());

        anim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                int val = (Integer) valueAnimator.getAnimatedValue();
                ViewGroup.LayoutParams layoutParams = mBinding.btnIniciarSesion.getLayoutParams();
                layoutParams.width = val;
                mBinding.btnIniciarSesion.requestLayout();
            }
        });
        anim.setDuration(250);
        anim.start();
    }

    private int getFabWidth() {
        return (int) getResources().getDimension(R.dimen.fab_size_tablet);
    }

    private void fadeOutTextAndShowProgressDialog() {
        mBinding.text.animate().alpha(0f)
                .setDuration(250)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        showProgressDialog();
                    }
                })
                .start();
    }

    private void showProgressDialog() {
        mBinding.progressBar.setAlpha(1f);
        mBinding.progressBar
                .getIndeterminateDrawable()
                .setColorFilter(Color.parseColor("#ffffff"), PorterDuff.Mode.SRC_IN);
        mBinding.progressBar.setVisibility(VISIBLE);
    }


    private void revealButton() {
        mBinding.btnIniciarSesion.setElevation(0f);

        mBinding.reveal.setVisibility(VISIBLE);

        int cx = mBinding.reveal.getWidth();
        int cy = mBinding.reveal.getHeight();


        int x = (int) (getFabWidth() / 2 + mBinding.btnIniciarSesion.getX());
        int y = (int) (getFabWidth() / 2 + mBinding.btnIniciarSesion.getY());

        float finalRadius = Math.max(cx, cy) * 1.2f;

        Animator reveal = ViewAnimationUtils
                .createCircularReveal(mBinding.reveal, x, y, getFabWidth(), finalRadius);

        reveal.setDuration(350);
        reveal.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                reset(animation);
//                finish();
            }

            private void reset(Animator animation) {
                super.onAnimationEnd(animation);
                mBinding.reveal.setVisibility(INVISIBLE);
                mBinding.text.setVisibility(VISIBLE);
                mBinding.text.setAlpha(1f);
                mBinding.btnIniciarSesion.setElevation(4f);
                ViewGroup.LayoutParams layoutParams = mBinding.btnIniciarSesion.getLayoutParams();
                layoutParams.width = (int) (getResources().getDisplayMetrics().density * 300);
                mBinding.btnIniciarSesion.requestLayout();
            }
        });

        reveal.start();
    }

    private void fadeOutProgressDialog() {
        mBinding.progressBar.animate().alpha(0f).setDuration(200).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);

            }
        }).start();
    }
    /*Animación botón iniciar sesión*/

    //Iniciar Sesion
    public class UserLoginTask extends AsyncTask<Void, Void, Integer> {

        String email = txtEmailLogin.getText().toString();
        String password = txtClaveLogin.getText().toString();

        @Override
        protected void onPreExecute() {
            btn.setEnabled(false);
            super.onPreExecute();
        }

        @Override
        protected Integer doInBackground(Void... params) {
            Integer retorno = 0;
            try {
                String empresa = String.valueOf(listaEmpresa.get(spEmpresas.getSelectedItemPosition()).getCodigo());
                Funciones.habilitarDirectivasInternet();
                String URL_WS = Funciones.URL_WS + "sesion.iniciar.movil.php";
                HashMap parametros = new HashMap<String, String>();
                parametros.put("p_usuario", email);
                parametros.put("p_clave", Funciones.toMd5(password));
                parametros.put("p_empresa", empresa);
                String resultado = new Funciones().getHttpContent(URL_WS, parametros);
                result = resultado;
                cargarDatosUsuario(result,email);
                retorno = 1;
            }catch (Exception e){
                Log.e("error catch", e.getLocalizedMessage());
            }

            return retorno;
        }

        @Override
        protected void onPostExecute(Integer resultado) {
            btn.setEnabled(true);
            try{
                JSONObject jsonObject = new JSONObject(result);
                String mensaje = jsonObject.getString("mensaje");
                String datos = jsonObject.getString("datos");
                revealButton();
                fadeOutProgressDialog();

                if(mensaje.equalsIgnoreCase("error")){
                    Funciones.mensaje("error",LoginActivity.this,datos);
                    fadeOutProgressDialog();
                    txtClaveLogin.setText("");
                    txtClaveLogin.requestFocus();
                }else{
                    if(resultado==1){
                        if(Integer.parseInt(sesion.getCodigoEmpresa()) != listaEmpresa.get(spEmpresas.getSelectedItemPosition()).getCodigo()){
                            Boolean ok = Funciones.mensaje("advertencia", LoginActivity.this, "Datos de inicio incorrectos");
                            if(ok){
                                sesion.iniciarSesion(false);
                            }
                            return;
                        }
                        launchMain();
                    }else{
                        Funciones.mensaje("error",LoginActivity.this,"Ha ocurrido un error al cargar los datos de usuario");
                    }
                }
            }catch (Exception e){
                revealButton();
                fadeOutProgressDialog();
                Log.e("postCat", e.getLocalizedMessage());
            }
        }

        @Override
        protected void onCancelled() {
            revealButton();
            fadeOutProgressDialog();
        }

    }
    //Término de Sesión

    private void cargarDatosUsuario(String resultado, String correo){
        try{
            if(resultado.contains("******-----****")){

            }
            JSONObject jsonObject = new JSONObject(resultado);
            String mensaje = jsonObject.getString("mensaje");

            if(!mensaje.equalsIgnoreCase("error")){
                JSONObject jsonObjectDatos = jsonObject.getJSONObject("datos");
                sesion.iniciarSesion(true);
                sesion.setApellidoPaterno(String.valueOf(jsonObjectDatos.getString("pat")));
                sesion.setApellidoMaterno(String.valueOf(jsonObjectDatos.getString("mat")));
                sesion.setNombre(String.valueOf(jsonObjectDatos.getString("nomb")));
                sesion.setEstado(String.valueOf(jsonObjectDatos.getString("estado")));
                sesion.setCodigoUsuario(String.valueOf(jsonObjectDatos.getString("cod_usuario")));
                sesion.setCargo(String.valueOf(jsonObjectDatos.getString("cargo")));
                sesion.setCodigoEmpresa(String.valueOf(jsonObjectDatos.getString("codigo_empresa")));
                sesion.setEmpresa(String.valueOf(jsonObjectDatos.getString("emp")));
                sesion.setNombreUsuario(correo);
                sesion.setPinesEmpresa(String.valueOf(jsonObjectDatos.getJSONArray("pines")));
                JSONArray arrayPines = jsonObjectDatos.getJSONArray("pines");
                for (int i = 0; i < arrayPines.length(); i++) {
                    JSONObject jsonData = arrayPines.getJSONObject(i);
                }
            }
        }catch (Exception e){
            Log.e("catch cargar datos", e.getLocalizedMessage());
        }
    }

    private void launchMain(){
        Intent intent = new Intent(LoginActivity.this, OnBoardingActivity.class);
        startActivity(intent);
        LoginActivity.this.finish();
    }

    private void descargarDatosEmpresa(){
        try{
            cargando = Funciones.cargando(LoginActivity.this, "Obteniendo datos del servidor", "");
            if(asyncEmpresas.getStatus() == AsyncTask.Status.PENDING || asyncEmpresas.getStatus() == AsyncTask.Status.FINISHED){
                asyncEmpresas.execute();
            }
        }catch (RuntimeException e){
        }
    }

    private void validar(Dialog dialog){
        try{
            if(dialog.isShowing()){
                dialog.dismiss();
            }
            if(!cargandoEmpresas){
                Funciones.snack(rootView, LoginActivity.this, "No hay conexión con nuestro servidor.", "rojo", "ok");
                return;
            }else{
                int size = listaEmpresa.size();
                String arrayEmpresas[] = new String[size];
                for (int i = 0; i < size; i++){
                    arrayEmpresas[i] = listaEmpresa.get(i).getNombre();
                }
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(LoginActivity.this, R.layout.spinner_custom_item, arrayEmpresas);
                spEmpresas.setAdapter(adapter);
                spEmpresas.setVisibility(VISIBLE);
                mBinding.btnIniciarSesion.setVisibility(VISIBLE);
                spEmpresas.setAnimation(fadeIn);
                mBinding.btnIniciarSesion.setAnimation(fadeIn);
            }
        }catch (RuntimeException e){
        }
    }

    /* Cargando empresas del servidor */

    public class CargarListaEmpresas extends AsyncTask<Void, Void, Integer> {


        @Override
        protected Integer doInBackground(Void... params) {
            Integer retorno = 0;
            try {
                String urlListado = Funciones.URL_WS + "empresa.cargar.movil.controlador.php";
                HashMap parametros = new HashMap<String, String>();
                String resultado = new Funciones().getHttpContent(urlListado, parametros);
                cargarEmpresas(resultado);
                retorno = 1; //Satisfactoriamente
            } catch (Exception e) {
                e.printStackTrace();
            }
            return retorno;
        }

        @Override
        protected void onPostExecute(Integer retorno) {
            if (retorno == 1) {
                validar(cargando); //TODO
            } else {
                Log.e("error","cargarListaEmpresas");
            }
        }
    }

    private void cargarEmpresas(String resultado) {
        try {
            JSONObject json = new JSONObject(resultado);
            JSONArray jsonArray = json.getJSONArray("datos");
            listaEmpresa = new ArrayList<Empresa>();
            listaEmpresa.clear();
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonData = jsonArray.getJSONObject(i);

                Empresa item = new Empresa();
                item.setCodigo(jsonData.getInt("codigo_empresa"));
                item.setNombre(jsonData.getString("nombre_empresa"));
                item.setRuc(jsonData.getString("ruc"));
                item.setLatitud(jsonData.getDouble("lat"));
                item.setLongitud(jsonData.getDouble("long"));
                listaEmpresa.add(item);
                item.agregarEmpresa();
            }
            cargandoEmpresas = true;
            sesion.setSincronizadoEmpresa();
        }catch (Exception e){
            Log.e("Error: " , e.getMessage());
        }
    }

    /* Cargando empresas del servidor */
}
