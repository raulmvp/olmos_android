package pe.proolmos.proolmoscontrol;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import pe.proolmos.proolmoscontrol.Negocio.Caseta;
import pe.proolmos.proolmoscontrol.Negocio.CategoriaVehiculo;
import pe.proolmos.proolmoscontrol.Negocio.Empresa;
import pe.proolmos.proolmoscontrol.Negocio.Fundo;
import pe.proolmos.proolmoscontrol.Negocio.ManoObraEntrada;
import pe.proolmos.proolmoscontrol.Negocio.Persona;
import pe.proolmos.proolmoscontrol.Negocio.Sancionado;
import pe.proolmos.proolmoscontrol.Negocio.TipoPersonal;
import pe.proolmos.proolmoscontrol.Negocio.TipoVehiculo;
import pe.proolmos.proolmoscontrol.Negocio.Visita;
import pe.proolmos.proolmoscontrol.Sesion.SesionPreferences;
import pe.proolmos.proolmoscontrol.datos.AccesoDatos;
import pe.proolmos.proolmoscontrol.util.Funciones;

import android.os.Handler;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;


import com.airbnb.lottie.LottieAnimationView;
import com.andrognito.pinlockview.IndicatorDots;
import com.andrognito.pinlockview.PinLockListener;
import com.andrognito.pinlockview.PinLockView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;


public class MainActivity extends AppCompatActivity {
    private SesionPreferences sesion;
    View vista, btnEntrada, btnSalida, btnConsultaEntrada, btnConsultaSalida, imgLogo, lyPin, lyComprobado, header;
    TextView txtFundo;
    ImageView ivMenu;
    LottieAnimationView ltEntrada, ltSalida, ltConsultaEntrada, ltConsultaSalida;
    Context contexto;
    PinLockView pinMain;
    IndicatorDots pinIndicator;

    public static ArrayList<CategoriaVehiculo> listaCategoriasVehiculo;
    public static ArrayList<TipoVehiculo> listaTipoVehiculo;
    public static ArrayList<TipoPersonal> listaTipoPersonal;
    public static ArrayList<Empresa> listaEmpresa;
    public static ArrayList<Persona> listaPersona;
    public static ArrayList<Sancionado> listaSancionado;
    public static ArrayList<Caseta> listaCasetas;

    Animation izquierdaDerechaEntrada, derechaIzquierdaSalida, izquierdaDerechaConsultaEntrada, derechaIzquierdaConsultaSalida, fadeIn, fadeLogin;


    Persona persona;
    Sancionado sancionado;
    Fundo fundo;
    Visita visita;

    private boolean cargandoEmpresas, cargandoPersonas, cargandoFundos;
    public static Dialog cargando;
    CargarListaEmpresas asyncEmpresas;
    CargarListaPersonas asynPersonas;
    CargarListaFundos asynFundos;

    Animation shake;
    LottieAnimationView lockAnimation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        sesion = new SesionPreferences(MainActivity.this.getApplicationContext());
        sesion.validarSesion();
        sesion.setPinUsuario("");
        AccesoDatos.aplicacion = this;
        Funciones.cargarManoObra(sesion);
        Funciones.cargarPines(sesion);

        /**CARGAR DATOS INTERNET**/
        Funciones.descargarVehiculos(sesion);
        Funciones.descargarProveedorEmpresa(sesion);
        Funciones.descargarProveedor(sesion);
        Funciones.descargarTransporte(sesion);
        /**CARGAR DATOS INTERNET**/
        /**LOG DE BD**/
        new Fundo().obtenerNoSincronizados();
        new Visita().obtenerNoSincronizados();
        new ManoObraEntrada().obtenerNoSincronizados();
        /**LOG DE BD**/
        vista = (View) findViewById(R.id.lyMainActivity);
        contexto = this;

        lockAnimation = (LottieAnimationView) findViewById(R.id.ltLockPin);
        lockAnimation.setMaxProgress(0.25f);
        pinMain = (PinLockView) findViewById(R.id.pin_main);
        pinIndicator = (IndicatorDots) findViewById(R.id.pin_dots);

        lyPin = (View) findViewById(R.id.lyPin);
        lyComprobado = (View) findViewById(R.id.lyComprobado);

        if(sesion.getPinesEmpresa().contains("[]") || sesion.getPinesEmpresa().isEmpty()){
            lyPin.setVisibility(View.GONE);
            lyComprobado.setVisibility(View.VISIBLE);
        }

        btnEntrada = (View) findViewById(R.id.btnRegistroEntrada);
        btnSalida = (View) findViewById(R.id.btnRegistroSalida);
        btnConsultaEntrada = (View) findViewById(R.id.btnConsultaEntrada);
        btnConsultaSalida = (View) findViewById(R.id.btnConsultaSalida);
        imgLogo = (View) findViewById(R.id.imgLogo);
        header = (View) findViewById(R.id.headerMain);
        txtFundo = (TextView) findViewById(R.id.txtFundo);

        ltEntrada = (LottieAnimationView) findViewById(R.id.ltRegistroEntrada);
        ltSalida = (LottieAnimationView) findViewById(R.id.ltRegistroSalida);
        ltConsultaEntrada = (LottieAnimationView) findViewById(R.id.ltConsultaEntrada);
        ltConsultaSalida = (LottieAnimationView) findViewById(R.id.ltConsultaSalida);
        ivMenu = (ImageView) findViewById(R.id.ltHamburguesa);

        shake = AnimationUtils.loadAnimation(MainActivity.this, R.anim.shake);
        shake.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                pinMain.resetPinLockView();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        pinMain.setPinLockListener(new PinLockListener() {
            @Override
            public void onComplete(String pin) {

                if(sesion.getPinesEmpresa().contains(pin)){
                    sesion.setPinUsuario(pin);
                    lockAnimation.setSpeed(4f);
                    lockAnimation.setMinAndMaxProgress(0.25f, 1f);
                    lockAnimation.playAnimation();
                    lockAnimation.addAnimatorListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            lyPin.setVisibility(View.GONE);
                            lyComprobado.setVisibility(View.VISIBLE);
                            super.onAnimationEnd(animation);
                        }
                    });
                }else{
                    pinIndicator.setAnimation(shake);
                    //lyPin.setVisibility(View.VISIBLE);
                    //lyComprobado.setVisibility(View.GONE);
                    //sesion.setPinUsuario("");
                }
            }

            @Override
            public void onEmpty() {
            }

            @Override
            public void onPinChange(int pinLength, String intermediatePin) {
            }
        });

        pinIndicator.setPinLength(8);
        pinMain.attachIndicatorDots(pinIndicator);


        ActionBar actionBar = getSupportActionBar();
        actionBar.hide();

        /* Animaciones */
        izquierdaDerechaEntrada = AnimationUtils.loadAnimation(this,R.anim.izquierdaderecha);
        derechaIzquierdaSalida = AnimationUtils.loadAnimation(this,R.anim.derechaizquierda);
        izquierdaDerechaConsultaEntrada = AnimationUtils.loadAnimation(this,R.anim.izquierdaderecha);
        derechaIzquierdaConsultaSalida = AnimationUtils.loadAnimation(this,R.anim.derechaizquierda);
        fadeIn = AnimationUtils.loadAnimation(this,R.anim.fadein);
        fadeLogin = AnimationUtils.loadAnimation(this,R.anim.fadein);

        header.setAnimation(fadeIn);
        fadeIn.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                imgLogo.setVisibility(View.VISIBLE);
                imgLogo.setAnimation(fadeLogin);
                ivMenu.setVisibility(View.VISIBLE);
                ivMenu.setAnimation(fadeLogin);
                btnEntrada.setAnimation(izquierdaDerechaEntrada);
                btnEntrada.setVisibility(View.VISIBLE);

                txtFundo.setText(sesion.getEmpresa());
                txtFundo.setVisibility(View.VISIBLE);
                txtFundo.setAnimation(fadeLogin);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        btnSalida.setAnimation(derechaIzquierdaSalida);
                        btnSalida.setVisibility(View.VISIBLE);
                        ltEntrada.playAnimation();
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                btnConsultaEntrada.setAnimation(izquierdaDerechaConsultaEntrada);
                                btnConsultaEntrada.setVisibility(View.VISIBLE);
                                ltSalida.playAnimation();
                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        btnConsultaSalida.setAnimation(derechaIzquierdaConsultaSalida);
                                        btnConsultaSalida.setVisibility(View.VISIBLE);
                                        ltConsultaEntrada.playAnimation();
                                        new Handler().postDelayed(new Runnable() {
                                            @Override
                                            public void run() {
                                                ltConsultaSalida.playAnimation();
                                            }
                                        }, 400);
                                    }
                                }, 300);
                            }
                        }, 300);
                    }
                }, 300);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        Funciones.habilitarDirectivasInternet();


        listaCategoriasVehiculo = new ArrayList<CategoriaVehiculo>();
        listaTipoVehiculo = new ArrayList<TipoVehiculo>();
        listaTipoPersonal = new ArrayList<TipoPersonal>();
        listaEmpresa = new ArrayList<Empresa>();
        listaPersona = new ArrayList<Persona>();
        listaSancionado = new ArrayList<Sancionado>();
        listaCasetas = new ArrayList<Caseta>();

        cargarDatosCategoriaVehiculo(listaCategoriasVehiculo);
        cargarDatosTipoVehiculo(listaTipoVehiculo);
        cargarDatosTipoPersonal(listaTipoPersonal);
        cargarDatosEmpresa(listaEmpresa);
        cargarDatosPersona(listaPersona);
        cargarDatosSancionado(listaSancionado);
        cargarDatosCaseta(listaCasetas);

        btnEntrada.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Intent i = new Intent(MainActivity.this, RegistroEntradaActivity.class);
                Intent i = new Intent(MainActivity.this, CamaraActivity.class);
                startActivity(i);
            }
        });

        btnSalida.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Intent i = new Intent(MainActivity.this, RegistroSalidaActivity.class);
                Intent i = new Intent(MainActivity.this, CamaraActivitySalida.class);
                startActivity(i);
            }
        });

        btnConsultaEntrada.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this, ListadoEntradaActivity.class);
                startActivity(i);
            }
        });

        btnConsultaSalida.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this, ListadoSalidaActivity.class);
                startActivity(i);
            }
        });

        cargandoEmpresas = false;

        cargandoPersonas = false;
        persona = new Persona();
        sancionado = new Sancionado();

        cargandoFundos = false;
        fundo = new Fundo();
        visita = new Visita();

        //registerForContextMenu(ltHamburguesa);
        ivMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //ltHamburguesa.showContextMenu();
                PopupMenu menu = new PopupMenu(contexto, ivMenu);
                if(sesion.getPinesEmpresa().contains("[]") || sesion.getPinesEmpresa().isEmpty()){
                    menu.getMenuInflater().inflate(R.menu.menu_sin_pin, menu.getMenu());
                    menu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem menuItem) {

                            switch (menuItem.getItemId()){
                                case R.id.menuCerrarSesion:
                                    cerrarSesion();
                                    break;
                                case R.id.menuSincronizarDatos:
                                    sincronizarDatos();
                                    break;

                            }
                            return true;
                        }
                    });
                }else{
                    menu.getMenuInflater().inflate(R.menu.menu, menu.getMenu());
                    menu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem menuItem) {

                            switch (menuItem.getItemId()){

                                case R.id.menuCerrarSesionUsuario:
                                    MainActivity.this.recreate();
                                    break;
                                case R.id.menuCerrarSesion:
                                    cerrarSesion();
                                    break;
                                case R.id.menuSincronizarDatos:
                                    sincronizarDatos();
                                    break;

                            }
                            return true;
                        }
                    });
                }

                menu.show();

            }
        });
    }

    private void enviarErrorSincronizacion(){
        if(fundo.cantidadNoSincronizados() > 0){
            boolean r = Funciones.mensajeConfirmacion("pregunta", contexto, "Al aceptar usted enviará todos los datos no sincronizados al servidor y nuestro equipo se comunicará a la brevedad.");
            if (r == false){
                return;
            }
            new EnviarReporteSincronizacion().execute();
        }else{
            Funciones.mensaje("info", contexto, "Esta opción solo es válida si tienes datos por sincronizar.");
        }
    }
    private void cerrarSesion(){
        boolean r = Funciones.mensajeConfirmacion("pregunta", contexto, "¿Cerrar sesión?");
        if (r == false){
            return;
        }

        int cantidadNoSincronizados = fundo.cantidadNoSincronizados();

        if(cantidadNoSincronizados > 0){
            Funciones.mensaje("advertencia",contexto, "Antes de cerrar sesión debe sincronizar sus entradas/salidas de vehículos.");
            return;
        }

        sesion.cerrarSesion();
    }

    private void sincronizarDatos(){
        Intent i = new Intent(contexto, SincronizadoActivity.class);
        startActivity(i);
        /* boolean r = Funciones.mensajeConfirmacion("pregunta", contexto, "Al sincronizar datos, se actualizarán todos los datos de la aplicación.\nEsto puede tomar un tiempo, así que asegúrate de disponer de él si vas a sincronizar todo.");
        if (r == false){
            return;
        }

        if(Funciones.obtenerTipoConexion(contexto) > 0){
            cargando = new Dialog(contexto);
            asyncEmpresas = new CargarListaEmpresas();
            descargarDatos();
        }else{
            Funciones.snack(MainActivity.this.vista, contexto, "No hay conexión con nuestro servidor", "rojo", "ok");
        }*/
    }
    /* Cargando de la base de datos SQLite datos fijos */

    private ArrayList<CategoriaVehiculo> cargarDatosCategoriaVehiculo(ArrayList<CategoriaVehiculo> lista){
        CategoriaVehiculo obj = new CategoriaVehiculo();
        return obj.cargarDatosCategoriaVehiculo(lista);
    }

    private ArrayList<TipoVehiculo> cargarDatosTipoVehiculo(ArrayList<TipoVehiculo> lista){
        TipoVehiculo obj = new TipoVehiculo();
        return obj.cargarDatosTipoVehiculo(lista);
    }

    private ArrayList<TipoPersonal> cargarDatosTipoPersonal(ArrayList<TipoPersonal> lista){
        TipoPersonal obj = new TipoPersonal();
        return obj.cargarDatosTipoPersonal(lista);
    }

    private ArrayList<Empresa> cargarDatosEmpresa(ArrayList<Empresa> lista){
        Empresa obj = new Empresa();
        return obj.cargarDatosEmpresa(lista);
    }

    private ArrayList<Persona> cargarDatosPersona(ArrayList<Persona> lista){
        Persona obj = new Persona();
        return obj.cargarDatosPersona(lista);
    }

    private ArrayList<Sancionado> cargarDatosSancionado(ArrayList<Sancionado> lista){
        Sancionado obj = new Sancionado();
        return obj.cargarDatosSancionados(lista);
    }
    /* Cargando de la base de datos SQLite datos fijos */

    /*Cargando casetas de arrayList*/
    private ArrayList<Caseta> cargarDatosCaseta(ArrayList<Caseta> lista){
        Caseta obj = new Caseta();
        return obj.cargarDatosCaseta(lista);
    }

    public static String obtenerEstadoConductor(String dni){
        String estado = "";
        for (int i=0; i<listaPersona.size(); i++){
            if(listaPersona.get(i).getDni().equalsIgnoreCase(dni)){
                estado = listaPersona.get(i).getEstado();
            }
        }
        return estado;
    }

    public static String obtenerTipoVehiculp(int codigo){
        String desc = "";
        for (int i=0; i<listaTipoVehiculo.size(); i++){
            if(listaTipoVehiculo.get(i).getCodigo() == codigo){
                desc = listaTipoVehiculo.get(i).getDescripcion();
            }
        }
        return desc;
    }

    public static String obtenerTipoPersonal(int codigo){
        String desc = "";
        for (int i=0; i<listaTipoPersonal.size(); i++){
            if(listaTipoPersonal.get(i).getCodigo() == codigo){
                desc = listaTipoPersonal.get(i).getDescripcion();
            }
        }
        return desc;
    }

    public static String obtenerCaseta(int codigo){
        String desc = "";
        for (int i=0; i<listaCasetas.size(); i++){
            if(listaCasetas.get(i).getCodigo() == codigo){
                desc = listaCasetas.get(i).getDescripcion();
            }
        }
        return desc;
    }

    private void descargarDatos(){
        Funciones.mensajito = "Estableciendo conexión segura con el servidor...";
        cargando = Funciones.cargando(contexto, "", "sinc");
        if(asyncEmpresas.getStatus() == AsyncTask.Status.PENDING || asyncEmpresas.getStatus() == AsyncTask.Status.FINISHED){
            asyncEmpresas.execute();
        }
    }

    private void descargarDatosPersona(){
        Funciones.mensajito = "Estableciendo conexión segura con el servidor...";
        cargando = Funciones.cargando(contexto, "", "sinc");
        if(asynPersonas.getStatus() == AsyncTask.Status.PENDING || asynPersonas.getStatus() == AsyncTask.Status.FINISHED){
            asynPersonas.execute();
        }
    }

    private void descargarDatosFundo(){
        Funciones.mensajito = "Enviando datos no sincronizados al servidor...";
        cargando = Funciones.cargando(contexto, "", "sinc");
        if(asynFundos.getStatus() == AsyncTask.Status.PENDING || asynFundos.getStatus() == AsyncTask.Status.FINISHED){
            asynFundos.execute();
        }
    }

    private void validar(Dialog dialog){
        if(cargandoEmpresas){
            if(dialog.isShowing()){
                dialog.dismiss();
                asynPersonas = new CargarListaPersonas();
                descargarDatosPersona();
            }
        }
    }

    private void validarPersonas(Dialog dialog){
        if(cargandoPersonas){
            if(dialog.isShowing()){
                dialog.dismiss();
                asynFundos = new CargarListaFundos();
                descargarDatosFundo();
            }
        }
    }

    private void validarFundos(Dialog dialog){
        if(dialog.isShowing()){
            dialog.dismiss();
        }

        if(cargandoFundos){
            Funciones.mensaje("exito", contexto, "Datos actualizados");
        }
    }


    /* Cargando empresas del servidor */

    public class CargarListaEmpresas extends AsyncTask<Void, Void, Integer> {


        @Override
        protected Integer doInBackground(Void... params) {
            Integer retorno = 0;
            try {
                String urlListado = Funciones.URL_WS + "empresa.cargar.movil.controlador.php";
                HashMap parametros = new HashMap<String, String>();
                String resultado = new Funciones().getHttpContent(urlListado, parametros);
                cargarEmpresas(resultado);
                retorno = 1; // Satisfactoriamente

            } catch (Exception e) {
                e.printStackTrace();
            }

            return retorno;
        }

        @Override
        protected void onPostExecute(Integer retorno) {
            if (retorno == 1) {
                cargandoEmpresas = true;
                validar(cargando);
            } else {
                Log.e("error","cargarListaEmpresas");
            }
        }
    }

    private void cargarEmpresas(String resultado) {
        try {
            if(resultado.equalsIgnoreCase("******-----****")){
                Funciones.snack(vista, contexto,"No hay conexión con nuestro servidor", "rojo", "ok");
                return;
            }
            JSONObject json = new JSONObject(resultado);
            JSONArray jsonArray = json.getJSONArray("datos");

            int numeroEmpresas = jsonArray.length(), progreso = 0, porcentaje = 0;
            Funciones.barraCargando.setProgress(0);
            Funciones.barraCargando.showContextMenu();
            Funciones.barraCargando.setMax(numeroEmpresas);
            Funciones.mensajito = "Sincronizando empresas: 0%";

            ArrayList<Empresa> listaEmpresa = new ArrayList<Empresa>();

            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonData = jsonArray.getJSONObject(i);

                Empresa item = new Empresa();
                item.setCodigo(jsonData.getInt("codigo_empresa"));
                item.setNombre(jsonData.getString("nombre_empresa"));
                item.setRuc(jsonData.getString("ruc"));
                item.setLatitud(jsonData.getDouble("lat"));
                item.setLongitud(jsonData.getDouble("long"));
                item.agregarEmpresa();
                listaEmpresa.add(item);
                Funciones.barraCargando.setProgress(progreso++);
                porcentaje = 100 * progreso / numeroEmpresas;
                Funciones.mensajito = "Sincronizando empresas: "+ porcentaje +"%";
            }

            Funciones.mensajito = "";
        }catch (Exception e){
            Log.e("Error: " , e.getMessage());
        }
    }

    /* Cargando empresas del servidor */


    /* Cargando conductores del servidor */

    public class CargarListaPersonas extends AsyncTask<Void, Void, Integer> {


        @Override
        protected Integer doInBackground(Void... params) {
            Integer retorno = 0;
            try {
                String urlListado = Funciones.URL_WS + "persona.cargar.movil.controlador.php";
                HashMap parametros = new HashMap<String, String>();
                String resultado = new Funciones().getHttpContent(urlListado, parametros);
                cargarPersonas(resultado);
                retorno = 1; // Satisfactoriamente

            } catch (Exception e) {
                e.printStackTrace();
            }

            return retorno;
        }

        @Override
        protected void onPostExecute(Integer retorno) {
            if (retorno == 1) {
                cargandoPersonas = true;
                validarPersonas(cargando);
            } else {
                Log.e("error","cargarListaPersonas");
            }
        }
    }

    private void cargarPersonas(String resultado) {
        try {
            JSONObject json = new JSONObject(resultado);
            JSONArray jsonArrayPersonas = json.getJSONArray("mensaje");
            JSONArray jsonArraySancionados = json.getJSONArray("datos");


            int numeroPersonas = jsonArrayPersonas.length(), numeroSancionados = jsonArraySancionados.length(), progreso = 0, totalSincronizar = numeroPersonas+numeroSancionados, porcentaje = 0;
            Funciones.barraCargando.setProgress(0);
            Funciones.barraCargando.showContextMenu();
            Funciones.barraCargando.setMax(totalSincronizar);
            Funciones.mensajito = "Sincronizando conductores: 0%";

            for (int i = 0; i < jsonArrayPersonas.length(); i++) {
                JSONObject jsonData = jsonArrayPersonas.getJSONObject(i);
                Persona item = new Persona();
                item.setDni(jsonData.getString("dni"));
                item.setNombre(jsonData.getString("nombre"));
                item.setPaterno(jsonData.getString("paterno"));
                item.setMaterno(jsonData.getString("materno"));
                item.setEstado(jsonData.getString("estado"));
                item.setSincronizado("S");
                item.setCodigo_off_line(0);
                item.agregarPersona(item);
                Funciones.barraCargando.setProgress(progreso++);
                porcentaje = 100 * progreso / totalSincronizar;
                Funciones.mensajito = "Sincronizando conductores: "+ porcentaje +"%";
            }

            for (int j = 0; j < jsonArraySancionados.length(); j++){
                JSONObject data = jsonArraySancionados.getJSONObject(j);
                Sancionado san = new Sancionado();
                san.setDni(data.getString("dni"));
                san.setMotivo(data.getString("motivo"));
                san.setDesde(data.getString("desde"));
                san.setHasta(data.getString("hasta"));
                san.agregarSancionado(san);
                Funciones.barraCargando.setProgress(progreso++);
                porcentaje = 100 * progreso / totalSincronizar;
                Funciones.mensajito = "Sincronizando conductores: "+ porcentaje +"%";
            }
        }catch (Exception e){
            Log.e("Error: " , e.getMessage());
        }
    }

    /* Cargando conductores del servidor */

    /* Cargando fundos del servidor */

    public class CargarListaFundos extends AsyncTask<Void, Void, Integer> {

        private JSONArray detalle = new Fundo().obtenerNoSincronizados();
        @Override
        protected void onPreExecute() {
            Funciones.mensajito = "Enviando datos no sincronizados al servidor...";
            super.onPreExecute();
        }

        @Override
        protected Integer doInBackground(Void... params) {
            Integer retorno = 0;
            try {
                String urlListado = Funciones.URL_WS + "fundo.sincronizar.movil.php";
                HashMap parametros = new HashMap<String, String>();
                parametros.put("p_detalle", detalle.toString());
                parametros.put("p_codigo", new Fundo().obtenerMaximoCodigoEntrada());
                parametros.put("p_empresa",sesion.getCodigoEmpresa());
                parametros.put("p_lista_completa", "S");
                String resultado = new Funciones().getHttpContent(urlListado, parametros);
                cargarFundos(resultado);
                cargandoFundos = false;
                retorno = 1; // Satisfactoriamente

            } catch (Exception e) {
                e.printStackTrace();
            }
            return retorno;
        }

        @Override
        protected void onPostExecute(Integer retorno) {
            if (retorno == 1) {
                validarFundos(cargando);
            } else {
                Log.e("error","cargarListaFundos");
            }
        }
    }

    private void cargarFundos(String resultado) {
        try {
            JSONObject json = new JSONObject(resultado);
            JSONArray jsonDatos = json.getJSONArray("datos");

            int numeroEntradas = jsonDatos.length(), progreso = 0, porcentaje = 0;
            Funciones.barraCargando.setProgress(0);
            Funciones.barraCargando.showContextMenu();
            Funciones.barraCargando.setMax(numeroEntradas);
            Funciones.mensajito = "Sincronizando datos: 0%";

            Fundo fundo = new Fundo();
            fundo.eliminarDatosFundo();

            for (int i = 0; i < jsonDatos.length(); i++) {
                JSONObject data = jsonDatos.getJSONObject(i);
                Fundo item = new Fundo();
                String ocupantes = data.getString("numero_ocupantes");
                if(ocupantes.equalsIgnoreCase("") || ocupantes.isEmpty()){
                    ocupantes = "0";
                }
                item.setCodigo((data.getInt("codigo_entrada_fundo")));
                item.setFecha(data.getString("fecha_entrada"));
                item.setFecha_manual(data.getString("fecha_manual"));
                item.setHora(data.getString("hora_entrada"));
                item.setDni(data.getString("dni"));
                item.setNombre(data.getString("conductor"));
                item.setCodigo_categoria_personal((data.getInt("codigo_categoria_personal")));
                item.setNombre_empresa(data.getString("nombre_empresa"));
                item.setCodigo_tipo_vehiculo((data.getInt("codigo_tipo_vehiculo")));
                item.setPlaca_entrada(data.getString("placa"));
                item.setProcedencia(data.getString("procedencia_fundo"));
                item.setNum_asientos(Integer.parseInt(data.getString("numero_asientos")));
                item.setNum_ocupantes(Integer.parseInt(ocupantes));
                item.setMotivo(data.getString("motivo_fundo"));
                item.setContenedor(data.getString("contenedor"));
                item.setEntrada_salida(data.getString("entrada_salida_fundo"));
                item.setCodigo_empresa((data.getInt("codigo_empresa")));
                item.setCodigo_usuario((data.getInt("codigo_usuario")));
                item.setEstado_entrada(data.getString("estado_entrada_fundo"));
                item.setHora_manual(data.getString("hora_manual"));
                item.setCodigo_ruta(data.getInt("codigo_ruta"));
                item.setCodigo_entrada(data.getInt("codigo_ruta"));
                item.setSincronizado("S");
                item.setCodigo_off_line(data.getString("codigo_off"));
                item.setOrientacion(data.getString("orientacion"));
                item.agregarFundo();

                JSONArray jsonVisitas = (JSONArray) data.get("visitas_lista");

                for (int j = 0; j < jsonVisitas.length(); j++){
                    JSONObject dataVisita = jsonVisitas.getJSONObject(j);
                    if(!dataVisita.getString("dni").equalsIgnoreCase("DNI no encontrado")){
                        Visita visita = new Visita();
                        visita.setCodigo_visita(dataVisita.getInt("codigo_visita_fundo"));
                        visita.setDni(dataVisita.getString("dni"));
                        visita.setCodigo_entrada(dataVisita.getInt("codigo_entrada_fundo"));
                        visita.setComentario(dataVisita.getString("comentario"));
                        visita.setSincronizado("S");
                        visita.setCodigo_off_line("");
                        visita.agregarVisita(visita);
                    }
                }

                Funciones.barraCargando.setProgress(progreso++);
                porcentaje = 100 * progreso / numeroEntradas;
                Funciones.mensajito = "Sincronizando datos: "+ porcentaje +"%";
            }
            cargandoFundos = true;
        }catch (Exception e){
            Log.e("Error Fundos" , e.getMessage());
        }
    }

    /* Cargando fundos del servidor */

    /* Reportar error en Sincronizar Datos */
    public class EnviarReporteSincronizacion extends AsyncTask<Void, Void, String> {
        JSONArray detalle = new Fundo().obtenerNoSincronizados();
        int codigo = new Fundo().obtenerMaximoCodigoEntrada();
        @Override
        protected void onPreExecute() {
            cargando = Funciones.cargando(contexto, "","sinc");
            Funciones.mensajito = "Enviando reporte al servidor...";
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(Void... params) {
            String resultado = "";
            try {
                String urlListado = Funciones.URL_WS + "fundo.sincronizar.movil.php";
                HashMap parametros = new HashMap<String, String>();
                parametros.put("p_detalle",this.detalle.toString());
                parametros.put("p_codigo",String.valueOf(this.codigo));
                parametros.put("p_reportar","R");
                parametros.put("p_empresa_nombre",sesion.getEmpresa());
                parametros.put("p_empresa",sesion.getCodigoEmpresa());
                parametros.put("p_usuario",sesion.getNombreUsuario());
                resultado = new Funciones().getHttpContent(urlListado, parametros);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return resultado;
        }

        @Override
        protected void onPostExecute(String resultado) {
            try {
                JSONObject json = new JSONObject(resultado);
                String mensaje = json.getString("mensaje");

                if(mensaje.equalsIgnoreCase("error")){
                    String mensajeError = json.getString("datos");
                    Funciones.mensajito = "";
                    if(cargando.isShowing()){
                        cargando.dismiss();
                    }
                    Funciones.mensaje("error", contexto, mensajeError);
                    return;
                }

                JSONArray jsonDatos = json.getJSONArray("datos");
                int numeroEntradas = jsonDatos.length(), progreso = 0, porcentaje = 0;
                if(numeroEntradas>0){
                    Funciones.barraCargando.setProgress(0);
                    Funciones.barraCargando.showContextMenu();
                    Funciones.barraCargando.setMax(numeroEntradas);
                    Funciones.mensajito = "Sincronizando datos: 0%";

                    for (int i = 0; i < numeroEntradas; i++) {
                        JSONObject data = jsonDatos.getJSONObject(i);
                        Fundo item = new Fundo();
                        String ocupantes = data.getString("numero_ocupantes");
                        if(ocupantes.equalsIgnoreCase("") || ocupantes.isEmpty()){
                            ocupantes = "0";
                        }
                        item.setCodigo((data.getInt("codigo_entrada_fundo")));
                        item.setFecha(data.getString("fecha_entrada"));
                        item.setFecha_manual(data.getString("fecha_manual"));
                        item.setHora(data.getString("hora_entrada"));
                        item.setDni(data.getString("dni"));
                        item.setNombre(data.getString("conductor"));
                        item.setCodigo_categoria_personal((data.getInt("codigo_categoria_personal")));
                        item.setNombre_empresa(data.getString("nombre_empresa"));
                        item.setCodigo_tipo_vehiculo((data.getInt("codigo_tipo_vehiculo")));
                        item.setPlaca_entrada(data.getString("placa"));
                        item.setProcedencia(data.getString("procedencia_fundo"));
                        item.setNum_asientos(Integer.parseInt(data.getString("numero_asientos")));
                        item.setNum_ocupantes(Integer.parseInt(ocupantes));
                        item.setMotivo(data.getString("motivo_fundo"));
                        item.setContenedor(data.getString("contenedor"));
                        item.setEntrada_salida(data.getString("entrada_salida_fundo"));
                        item.setCodigo_empresa((data.getInt("codigo_empresa")));
                        item.setCodigo_usuario((data.getInt("codigo_usuario")));
                        item.setEstado_entrada(data.getString("estado_entrada_fundo"));
                        item.setHora_manual(data.getString("hora_manual"));
                        item.setCodigo_ruta(data.getInt("codigo_ruta"));
                        item.setCodigo_entrada(data.getInt("codigo_ruta"));
                        item.setSincronizado("S");
                        item.setCodigo_off_line(data.getString("codigo_off"));
                        item.agregarFundo();

                        JSONArray jsonVisitas = (JSONArray) data.get("visitas_lista");

                        for (int j = 0; j < jsonVisitas.length(); j++){
                            JSONObject dataVisita = jsonVisitas.getJSONObject(j);
                            if(!dataVisita.getString("dni").equalsIgnoreCase("DNI no encontrado")){
                                Visita visita = new Visita();
                                visita.setCodigo_visita(dataVisita.getInt("codigo_visita_fundo"));
                                visita.setDni(dataVisita.getString("dni"));
                                visita.setCodigo_entrada(dataVisita.getInt("codigo_entrada_fundo"));
                                visita.setComentario(dataVisita.getString("comentario"));
                                visita.setSincronizado("S");
                                visita.setCodigo_off_line("");
                                visita.agregarVisita(visita);
                            }

                        }

                        Funciones.barraCargando.setProgress(progreso++);
                        porcentaje = 100 * progreso / numeroEntradas;
                        Funciones.mensajito = "Sincronizando datos: "+ porcentaje +"%";
                    }

                }

                Fundo fundo = new Fundo();
                fundo.eliminarNoSincronizados(detalle);

                Funciones.mensajito = "";
                if(cargando.isShowing()){
                    cargando.dismiss();
                }

                if(numeroEntradas>0){
                    Funciones.mensaje("exito",contexto,"Hemos podido sincronizar tus datos.");
                }else{
                    Funciones.mensaje("exito",contexto,"Hemos enviado un correo con tus datos no sincronizados.");
                }
            }catch (Exception e){
                if(cargando.isShowing()){
                    cargando.dismiss();
                }
                Funciones.mensaje("error", contexto, e.getMessage());
            }
        }
    }
    /* Reportar error en Sincronizar Datos */

    @Override
    protected void onResume() {
        sesion.validarSesion();
        Funciones.cargarManoObra(sesion);
        Funciones.cargarPines(sesion);
        super.onResume();
    }
}
