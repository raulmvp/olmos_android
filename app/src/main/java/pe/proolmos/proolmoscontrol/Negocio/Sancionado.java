package pe.proolmos.proolmoscontrol.Negocio;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.sql.Date;
import java.util.ArrayList;

import pe.proolmos.proolmoscontrol.datos.AccesoDatos;

public class Sancionado extends AccesoDatos {
    private String dni, motivo, desde, hasta;

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public String getMotivo() {
        return motivo;
    }

    public void setMotivo(String motivo) {
        this.motivo = motivo;
    }

    public String getDesde() {
        return desde;
    }

    public void setDesde(String desde) {
        this.desde = desde;
    }

    public String getHasta() {
        return hasta;
    }

    public void setHasta(String hasta) {
        this.hasta = hasta;
    }

    public ArrayList<Sancionado> cargarDatosSancionados(ArrayList<Sancionado> lista){
        SQLiteDatabase db = this.getReadableDatabase();
        String sql = "select * from sancionado";
        Cursor resultado = db.rawQuery(sql,null);
        lista.clear();
        while (resultado.moveToNext()){
            Sancionado obj = new Sancionado();
            obj.setDni(resultado.getString(0));
            obj.setMotivo(resultado.getString(1));
            obj.setDesde(resultado.getString(2));
            obj.setHasta(resultado.getString(3));
            lista.add(obj);
        }
        return lista;
    }

    public long eliminarSancionados(){
        SQLiteDatabase db = this.getWritableDatabase();
        long resultado = db.delete("sancionado", null, null);
        return resultado;
    }

    public boolean agregarSancionados(ArrayList<Sancionado> lista){
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            for(int i =0; i < lista.size(); i++){
                ContentValues values = new ContentValues();
                Sancionado item = lista.get(i);
                values.put("dni", item.getDni());
                values.put("motivo", item.getMotivo());
                values.put("desde", String.valueOf(item.getDesde()));
                values.put("hasta", String.valueOf(item.getHasta()));
                db.insert("sancionado", null, values);
            }

            return true;
        }catch (RuntimeException e){
            Log.e("Sancionado.java", "Agregar");
            return false;
        }
    }

    public boolean agregarSancionado(Sancionado item){
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put("dni", item.getDni());
            values.put("motivo", item.getMotivo());
            values.put("desde", String.valueOf(item.getDesde()));
            values.put("hasta", String.valueOf(item.getHasta()));
            db.insert("sancionado", null, values);
            return true;
        }catch (RuntimeException e){
            Log.e("Sancionado.java", "Agregar");
            return false;
        }
    }
}
