package pe.proolmos.proolmoscontrol.Negocio;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import pe.proolmos.proolmoscontrol.datos.AccesoDatos;

public class Persona extends AccesoDatos {
    private String dni;
    private String nombre;
    private String paterno;
    private String materno;
    private String estado;
    private String sincronizado;
    private int codigo_off_line;

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPaterno() {
        return paterno;
    }

    public void setPaterno(String paterno) {
        this.paterno = paterno;
    }

    public String getMaterno() {
        return materno;
    }

    public void setMaterno(String materno) {
        this.materno = materno;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getSincronizado() {
        return sincronizado;
    }

    public void setSincronizado(String sincronizado) {
        this.sincronizado = sincronizado;
    }

    public int getCodigo_off_line() {
        return codigo_off_line;
    }

    public void setCodigo_off_line(int codigo_off_line) {
        this.codigo_off_line = codigo_off_line;
    }

    public ArrayList<Persona> cargarDatosPersona(ArrayList<Persona> lista){
        SQLiteDatabase db = this.getReadableDatabase();
        String sql = "select * from persona order by 3,4,2";
        Cursor resultado = db.rawQuery(sql,null);
        lista.clear();
        while (resultado.moveToNext()){
            Persona obj = new Persona();
            obj.setDni(resultado.getString(0));
            obj.setNombre(resultado.getString(1));
            obj.setPaterno(resultado.getString(2));
            obj.setMaterno(resultado.getString(3));
            obj.setEstado(resultado.getString(4));
            obj.setSincronizado(resultado.getString(5));
            obj.setCodigo_off_line(resultado.getInt(6));
            lista.add(obj);
        }
        return lista;
    }

    public long eliminarPersonas(){
        SQLiteDatabase db = this.getWritableDatabase();
        long resultado = db.delete("persona", null, null);
        return resultado;
    }

    public boolean agregarPersona(Persona item){
        try {
            SQLiteDatabase db = this.getReadableDatabase();
            String sql = "SELECT count(*) from persona where dni = "+this.getDni();
            Cursor resultado = db.rawQuery(sql, null);
            int existe = 0;
            while (resultado.moveToNext()){
                existe = resultado.getInt(0);
            }

            db = this.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put("dni", item.getDni());
            values.put("nombre", item.getNombre());
            values.put("paterno", item.getPaterno());
            values.put("materno", item.getMaterno());
            values.put("estado", item.getEstado());
            values.put("sincronizado", item.getSincronizado());
            values.put("codigo_off_line", item.getCodigo_off_line());

            if(existe == 0){
                db.insert("persona", null, values);
            }else {
                db.update("persona",  values, "dni="+item.getDni(), null);
            }

            return true;
        }catch (RuntimeException e){
            Log.e("Persona.java", "Agregar");
            return false;
        }
    }

    public boolean agregarPersonas(ArrayList<Persona> lista){
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            for(int i =0; i < lista.size(); i++){
                ContentValues values = new ContentValues();
                Persona item = lista.get(i);
                values.put("dni", item.getDni());
                values.put("nombre", item.getNombre());
                values.put("paterno", item.getPaterno());
                values.put("materno", item.getMaterno());
                values.put("estado", item.getEstado());
                values.put("sincronizado", item.getSincronizado());
                values.put("codigo_off_line", item.getCodigo_off_line());
                db.insert("persona", null, values);
            }

            return true;
        }catch (RuntimeException e){
            Log.e("Persona.java", "Agregar");
            return false;
        }
    }

    public JSONObject buscarConductor(String dni){
        JSONObject json = new JSONObject();
        try{
            SQLiteDatabase db = this.getReadableDatabase();
            String sql = "Select * from persona where dni = '"+dni+"';";
            Cursor resultado = db.rawQuery(sql, null);

            while (resultado.moveToNext()){
                json.put("nombre_conductor", resultado.getString(2) + " " + resultado.getString(3) + ", " + resultado.getString(1));
                json.put("estado", resultado.getString(4));
                json.put("dni_conductor", resultado.getString(0));
            }

            if(!json.has("nombre_conductor")){
                return json;
            }

            sql = "Select " +
                    "   placa_entrada_fundo as placa, " +
                    "   nombre_empresa, " +
                    "   procedencia_fundo, " +
                    "   numero_asientos, " +
                    "   codigo_caseta, " +
                    "   codigo_categoria_personal, " +
                    "   codigo_tipo_vehiculo, " +
                    "   (substr(fecha_manual, 7, 4) || '-' || substr(fecha_manual, 4, 2) || '-' || substr(fecha_manual, 1, 2)) as fech " +
                "from fundo where trim(dni_conductor) = '"+dni+"' order by fech desc limit 1;";

            resultado = db.rawQuery(sql, null);

            while (resultado.moveToNext()){
                json.put("placa", resultado.getString(0));
                json.put("nombre_empresa", resultado.getString(1));
                json.put("procedencia_fundo", resultado.getString(2));
                json.put("numero_asientos", resultado.getInt(3));
                json.put("codigo_caseta", resultado.getInt(4));
                json.put("codigo_categoria_personal", resultado.getInt(5));
                json.put("codigo_tipo_vehiculo", resultado.getInt(6));
            }
        }catch (RuntimeException | JSONException e){
            Log.e("Persona.java", "buscarPersona->"+e.getLocalizedMessage());
        }
        return json;
    }

    public JSONObject buscarTransportista(JSONObject json){
        try{
            SQLiteDatabase db = this.getReadableDatabase();
            String sql = "Select " +
                    "   capacidad " +
                    "from vehiculo where trim(placa) = trim('"+json.getString("placa")+"') limit 1;";

            Cursor resultado = db.rawQuery(sql, null);

            while (resultado.moveToNext()){
                json.remove("numero_asientos");
                json.put("numero_asientos", resultado.getInt(0));
            }

            sql = "Select estado from persona where trim(dni) = trim('"+json.getString("dni_conductor")+"');";
            resultado = db.rawQuery(sql, null);

            json.put("estado", "A");
            while (resultado.moveToNext()){
                json.remove("estado");
                json.put("estado", resultado.getString(0));
            }

            sql = "Select " +
                    "   codigo_caseta " +
                    "from fundo where trim(dni_conductor) = '"+json.getString("dni_conductor")+"'limit 1;";

            resultado = db.rawQuery(sql, null);

            while (resultado.moveToNext()){
                json.remove("codigo_caseta");
                json.put("codigo_caseta", resultado.getInt(0));
            }
        }catch (RuntimeException | JSONException e){
            Log.e("Persona.java", "buscarPersona->"+e.getLocalizedMessage());
        }
        return json;
    }

    public JSONObject buscarConductorPorPlacaQR(String placa){
        JSONObject json = new JSONObject();
        try{
            SQLiteDatabase db = this.getReadableDatabase();
            String sql = "Select" +
                    "   placa_entrada_fundo as placa, " +
                    "   procedencia_fundo, " +
                    "   numero_asientos," +
                    "   codigo_caseta," +
                    "   codigo_tipo_vehiculo, " +
                    "   trim(dni_conductor), " +
                    "   nombre_conductor, " +
                    "   codigo_categoria_personal, " +
                    "   codigo_empresa, " +
                    "   (substr(fecha_manual, 7, 4) || '-' || substr(fecha_manual, 4, 2) || '-' || substr(fecha_manual, 1, 2)) as fech " +
                    "from fundo where trim(placa_entrada_fundo) = trim('"+placa+"') order by fech desc limit 1;";

            Cursor resultado = db.rawQuery(sql, null);

            json.put("placa", "");
            json.put("dni_conductor", "");
            json.put("nombre_conductor", "NO ENCONTRADO");
            json.put("procedencia_fundo", "");
            json.put("codigo_caseta", 0);
            json.put("numero_asientos", 0);
            json.put("codigo_tipo_vehiculo", 0);
            json.put("codigo_categoria_personal", 0);
            int codigo_empresa = 0;

            while (resultado.moveToNext()){
                json.remove("procedencia_fundo");
                json.remove("codigo_caseta");
                json.remove("placa");
                json.remove("numero_asientos");
                json.remove("codigo_tipo_vehiculo");

                json.put("placa", resultado.getString(0));
                json.put("procedencia_fundo", resultado.getString(1));
                json.put("numero_asientos", resultado.getInt(2));
                json.put("codigo_caseta", resultado.getInt(3));
                json.put("codigo_tipo_vehiculo", resultado.getInt(4));
                json.put("dni_conductor", resultado.getString(5));
                json.put("nombre_conductor", resultado.getString(6));
                json.put("codigo_categoria_personal", resultado.getInt(7));
                codigo_empresa = resultado.getInt(8);
            }

            sql = "Select estado from persona where trim(dni) = trim('"+json.getString("dni_conductor")+"');";
            resultado = db.rawQuery(sql, null);

            json.put("estado", "A");
            while (resultado.moveToNext()){
                json.remove("estado");
                json.put("estado", resultado.getString(0));
            }

            sql = "Select " +
                    "   nombre " +
                    "   from empresa where codigo = "+codigo_empresa;
            resultado = db.rawQuery(sql, null);

            json.put("nombre_empresa", "");
            while (resultado.moveToNext()){
                json.remove("nombre_empresa");
                json.put("nombre_empresa", resultado.getString(0));
            }
        }catch (RuntimeException | JSONException e){
            Log.e("Persona.java", "buscarPersona->"+e.getLocalizedMessage());
        }
        return json;
    }


    public JSONObject buscarConductorQr(String dni, String codigo_empresa, String nombre_conductor, int codigo_tipo_personal){
        JSONObject json = new JSONObject();
        try{
            SQLiteDatabase db = this.getReadableDatabase();
            String sql = "Select estado from persona where trim(dni) = trim('"+dni+"');";
            Cursor resultado = db.rawQuery(sql, null);

            json.put("estado", "A");
            while (resultado.moveToNext()){
                json.remove("estado");
                json.put("estado", resultado.getString(0));
            }

            json.put("dni_conductor", dni);
            json.put("nombre_conductor", nombre_conductor);
            json.put("codigo_categoria_personal", codigo_tipo_personal);

            sql = "Select" +
                    "   placa_entrada_fundo as placa, " +
                    "   procedencia_fundo, " +
                    "   numero_asientos," +
                    "   codigo_caseta," +
                    "   codigo_tipo_vehiculo, " +
                    "   (substr(fecha_manual, 7, 4) || '-' || substr(fecha_manual, 4, 2) || '-' || substr(fecha_manual, 1, 2)) as fech " +
                "from fundo where trim(dni_conductor) = trim('"+dni+"') order by fech desc limit 1;";

            resultado = db.rawQuery(sql, null);

            json.put("placa", "");
            json.put("procedencia_fundo", "");
            json.put("codigo_caseta", 0);
            json.put("numero_asientos", 0);
            json.put("codigo_tipo_vehiculo", 0);

            while (resultado.moveToNext()){
                json.remove("procedencia_fundo");
                json.remove("codigo_caseta");
                json.remove("placa");
                json.remove("numero_asientos");
                json.remove("codigo_tipo_vehiculo");

                json.put("placa", resultado.getString(0));
                json.put("procedencia_fundo", resultado.getString(1));
                json.put("numero_asientos", resultado.getInt(2));
                json.put("codigo_caseta", resultado.getInt(3));
                json.put("codigo_tipo_vehiculo", resultado.getInt(4));
            }

            sql = "Select " +
                    "   nombre " +
                "   from empresa where codigo = "+codigo_empresa;
            resultado = db.rawQuery(sql, null);

            json.put("nombre_empresa", "");
            while (resultado.moveToNext()){
                json.remove("nombre_empresa");
                json.put("nombre_empresa", resultado.getString(0));
            }
        }catch (RuntimeException | JSONException e){
            Log.e("Persona.java", "buscarPersona->"+e.getLocalizedMessage());
        }
        return json;
    }
}
