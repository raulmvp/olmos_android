package pe.proolmos.proolmoscontrol.Negocio;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

import pe.proolmos.proolmoscontrol.datos.AccesoDatos;

public class CategoriaVehiculo extends AccesoDatos {
    private int codigo;
    private String nombre;
    private String descripcion;
    private double precio;

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public ArrayList<CategoriaVehiculo> cargarDatosCategoriaVehiculo(ArrayList<CategoriaVehiculo> lista){
        SQLiteDatabase db = this.getReadableDatabase();
        String sql = "select * from categoria_vehiculo order by 2;";
        Cursor resultado = db.rawQuery(sql,null);
        lista.clear();
        while (resultado.moveToNext()){
            CategoriaVehiculo obj = new CategoriaVehiculo();
            obj.setCodigo(resultado.getInt(0));
            obj.setNombre(resultado.getString(1));
            obj.setDescripcion(resultado.getString(2));
            obj.setPrecio(resultado.getDouble(3));
            lista.add(obj);
        }
        return lista;
    }
}
