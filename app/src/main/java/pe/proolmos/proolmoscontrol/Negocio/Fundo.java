package pe.proolmos.proolmoscontrol.Negocio;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import pe.proolmos.proolmoscontrol.datos.AccesoDatos;

public class Fundo extends AccesoDatos {
    private int codigo, codigo_categoria_personal, codigo_tipo_vehiculo, num_asientos, num_ocupantes, codigo_empresa, codigo_usuario, codigo_ruta, codigo_entrada, codigo_caseta, codigo_proveedor, codigo_ruta_transporte, codigo_proveedor_empresa;
    private String fecha, hora, dni, nombre, nombre_empresa, placa_entrada, procedencia, motivo, contenedor, entrada_salida, estado_entrada, hora_manual, fecha_manual, sincronizado, estado_conductor, tipo_personal_nombre, tipo_vehiculo_nombre, codigo_off_line, orientacion, pin, mano_obra;

    public int getCodigo_proveedor() {
        return codigo_proveedor;
    }

    public void setCodigo_proveedor(int codigo_proveedor) {
        this.codigo_proveedor = codigo_proveedor;
    }

    public int getCodigo_ruta_transporte() {
        return codigo_ruta_transporte;
    }

    public void setCodigo_ruta_transporte(int codigo_ruta_transporte) {
        this.codigo_ruta_transporte = codigo_ruta_transporte;
    }

    public int getCodigo_proveedor_empresa() {
        return codigo_proveedor_empresa;
    }

    public void setCodigo_proveedor_empresa(int codigo_proveedor_empresa) {
        this.codigo_proveedor_empresa = codigo_proveedor_empresa;
    }

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public String getOrientacion() {
        return orientacion;
    }

    public void setOrientacion(String orientacion) {
        this.orientacion = orientacion;
    }

    public String getEstado_conductor() {
        return estado_conductor;
    }

    public void setEstado_conductor(String estado_conductor) {
        this.estado_conductor = estado_conductor;
    }

    public String getTipo_personal_nombre() {
        return tipo_personal_nombre;
    }

    public void setTipo_personal_nombre(String tipo_personal_nombre) {
        this.tipo_personal_nombre = tipo_personal_nombre;
    }

    public String getTipo_vehiculo_nombre() {
        return tipo_vehiculo_nombre;
    }

    public void setTipo_vehiculo_nombre(String tipo_vehiculo_nombre) {
        this.tipo_vehiculo_nombre = tipo_vehiculo_nombre;
    }

    public int getCodigo_caseta() {
        return codigo_caseta;
    }

    public void setCodigo_caseta(int codigo_caseta) {
        this.codigo_caseta = codigo_caseta;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public int getCodigo_categoria_personal() {
        return codigo_categoria_personal;
    }

    public void setCodigo_categoria_personal(int codigo_categoria_personal) {
        this.codigo_categoria_personal = codigo_categoria_personal;
    }

    public int getCodigo_tipo_vehiculo() {
        return codigo_tipo_vehiculo;
    }

    public void setCodigo_tipo_vehiculo(int codigo_tipo_vehiculo) {
        this.codigo_tipo_vehiculo = codigo_tipo_vehiculo;
    }

    public int getNum_asientos() {
        return num_asientos;
    }

    public void setNum_asientos(int num_asientos) {
        this.num_asientos = num_asientos;
    }

    public int getNum_ocupantes() {
        return num_ocupantes;
    }

    public void setNum_ocupantes(int num_ocupantes) {
        this.num_ocupantes = num_ocupantes;
    }

    public int getCodigo_empresa() {
        return codigo_empresa;
    }

    public void setCodigo_empresa(int codigo_empresa) {
        this.codigo_empresa = codigo_empresa;
    }

    public int getCodigo_usuario() {
        return codigo_usuario;
    }

    public void setCodigo_usuario(int codigo_usuario) {
        this.codigo_usuario = codigo_usuario;
    }

    public int getCodigo_ruta() {
        return codigo_ruta;
    }

    public void setCodigo_ruta(int codigo_ruta) {
        this.codigo_ruta = codigo_ruta;
    }

    public int getCodigo_entrada() {
        return codigo_entrada;
    }

    public void setCodigo_entrada(int codigo_entrada) {
        this.codigo_entrada = codigo_entrada;
    }

    public String getCodigo_off_line() {
        return codigo_off_line;
    }

    public void setCodigo_off_line(String codigo_off_line) {
        this.codigo_off_line = codigo_off_line;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getNombre_empresa() {
        return nombre_empresa;
    }

    public void setNombre_empresa(String nombre_empresa) {
        this.nombre_empresa = nombre_empresa;
    }

    public String getPlaca_entrada() {
        return placa_entrada;
    }

    public void setPlaca_entrada(String placa_entrada) {
        this.placa_entrada = placa_entrada;
    }

    public String getProcedencia() {
        return procedencia;
    }

    public void setProcedencia(String procedencia) {
        this.procedencia = procedencia;
    }

    public String getMotivo() {
        return motivo;
    }

    public void setMotivo(String motivo) {
        this.motivo = motivo;
    }

    public String getContenedor() {
        return contenedor;
    }

    public void setContenedor(String contenedor) {
        this.contenedor = contenedor;
    }

    public String getEntrada_salida() {
        return entrada_salida;
    }

    public void setEntrada_salida(String entrada_salida) {
        this.entrada_salida = entrada_salida;
    }

    public String getEstado_entrada() {
        return estado_entrada;
    }

    public void setEstado_entrada(String estado_entrada) {
        this.estado_entrada = estado_entrada;
    }

    public String getHora_manual() {
        return hora_manual;
    }

    public void setHora_manual(String hora_manual) {
        this.hora_manual = hora_manual;
    }

    public String getFecha_manual() {
        return fecha_manual;
    }

    public void setFecha_manual(String fecha_manual) {
        this.fecha_manual = fecha_manual;
    }

    public String getSincronizado() {
        return sincronizado;
    }

    public void setSincronizado(String sincronizado) {
        this.sincronizado = sincronizado;
    }

    public String getMano_obra() {
        return mano_obra;
    }

    public void setMano_obra(String mano_obra) {
        this.mano_obra = mano_obra;
    }

    public ArrayList<Fundo> cargarEntradasFundo(ArrayList<Fundo> lista, String estado, String tipo, String fechaInicio){
        try{
            SQLiteDatabase db = this.getReadableDatabase();
            String sql = "select " +
                    "               fecha_manual, " +
                    "               coalesce(substr(hora_manual, 1, 5), substr(hora_entrada_fundo, 1, 5)) as hora, " +
                    "               dni_conductor, " +
                    "               nombre_conductor, " +
                    "               codigo_categoria_personal, " +
                    "               nombre_empresa, " +
                    "               codigo_tipo_vehiculo, " +
                    "               placa_entrada_fundo, " +
                    "               numero_asientos, " +
                    "               numero_ocupantes, " +
                    "               motivo_fundo, " +
                    "               sincronizado, " +
                    "               codigo_caseta, " +
                    "               contenedor, " +
                    "               (substr(fecha_manual, 7, 4) || '-' || substr(fecha_manual, 4, 2) || '-' || substr(fecha_manual, 1, 2)), " +
                    "               orientacion, " +
                    "               pin, " +
                    "               numero_ocupantes    " +
                    "     from fundo where entrada_salida_fundo='"+estado+"' and estado_entrada='E' ";
            if(tipo.equalsIgnoreCase("H")){
                sql += " and fecha_manual ='"+fechaInicio+"'";
            }else{
                sql += " and (substr(fecha_manual, 7, 4) || '-' || substr(fecha_manual, 4, 2) || '-' || substr(fecha_manual, 1, 2)) = '"+fechaInicio+"'";
            }

            sql += " order by hora desc;";
            Cursor resultado = db.rawQuery(sql, null);
            lista.clear();
            while(resultado.moveToNext()){
                Fundo obj = new Fundo();
                obj.setFecha_manual(resultado.getString(0));
                obj.setHora_manual(resultado.getString(1));
                obj.setDni(resultado.getString(2));
                obj.setNombre(resultado.getString(3));
                obj.setCodigo_categoria_personal(resultado.getInt(4));
                obj.setNombre_empresa(resultado.getString(5));
                obj.setCodigo_tipo_vehiculo(resultado.getInt(6));
                obj.setPlaca_entrada(resultado.getString(7));
                obj.setNum_asientos(resultado.getInt(8));
                obj.setNum_ocupantes(resultado.getInt(9));
                obj.setMotivo(resultado.getString(10));
                obj.setSincronizado(resultado.getString(11));
                obj.setCodigo_caseta(resultado.getInt(12));
                if(resultado.getString(13).contains("C")){
                    obj.setContenedor("CONTENEDOR");
                }else{
                    obj.setContenedor("");
                }

                obj.setOrientacion(resultado.getString(15));
                obj.setPin(resultado.getString(16));
                obj.setNum_ocupantes(resultado.getInt(17));
                lista.add(obj);
            }
            db.close();
            return lista;
        }catch (RuntimeException e){
            Log.e("cargarEntradasFundo", e.getLocalizedMessage());
        }
        return lista;
    }


    public JSONArray obtenerNoSincronizadosDia(){
        JSONArray detalle = new JSONArray();
        SQLiteDatabase db = this.getReadableDatabase();
        String sql = "SELECT fecha_manual,  count(*) from fundo where sincronizado = 'N' group by fecha_manual order by fecha_manual asc;";
        Cursor resultado = db.rawQuery(sql, null);
        try {
            while (resultado.moveToNext()) {
                JSONObject obj = new JSONObject();
                obj.put("fecha", resultado.getString(0));
                obj.put("total", resultado.getString(1));
                detalle.put(obj);
            }
        } catch (JSONException e) {
            Log.e("jsonArray", e.getLocalizedMessage());
        }
        db.close();
        return detalle;
    }

    public JSONArray obtenerNoSincronizadosPorFechaManual(String fecha_manual){
        JSONArray detalle = new JSONArray();
        SQLiteDatabase db = this.getReadableDatabase();
        String sql = "SELECT * from fundo where sincronizado = 'N' and fecha_manual = '"+fecha_manual+"' limit 50;";
        Cursor resultado = db.rawQuery(sql, null);

        try{
            while (resultado.moveToNext()){
                JSONObject obj = new JSONObject();
                obj.put("dni_conductor", resultado.getString(3));
                obj.put("nombre_conductor", resultado.getString(4));
                obj.put("codigo_categoria_personal", resultado.getInt(5));
                obj.put("nombre_empresa", resultado.getString(6));
                obj.put("codigo_tipo_vehiculo", resultado.getInt(7));
                obj.put("placa_entrada_fundo", resultado.getString(8));
                obj.put("procedencia_fundo", resultado.getString(9));
                obj.put("numero_asientos", resultado.getInt(10));
                obj.put("numero_ocupantes", resultado.getInt(11));
                obj.put("motivo_fundo", resultado.getString(12));
                obj.put("contenedor", resultado.getString(13));
                obj.put("entrada_salida_fundo", resultado.getString(14));
                obj.put("codigo_empresa", resultado.getInt(15));
                obj.put("codigo_usuario", resultado.getInt(16));
                obj.put("hora_manual", resultado.getString(18));
                obj.put("codigo_ruta",resultado.getInt(19));
                obj.put("codigo_entrada", resultado.getInt(20));
                obj.put("fecha_manual", resultado.getString(21));
                obj.put("codigo_off_line", resultado.getString(23));
                obj.put("codigo_caseta", resultado.getInt(24));
                obj.put("orientacion", resultado.getString(25));
                obj.put("pin", resultado.getString(26));
                obj.put("mano_obra", resultado.getString(27));
                obj.put("codigo_proveedor", resultado.getString(28));
                obj.put("codigo_ruta_transporte", resultado.getString(29));
                obj.put("codigo_proveedor_empresa", resultado.getString(30));
                detalle.put(obj);
            }
        }catch (JSONException e){
            Log.e("jsonArray", e.getLocalizedMessage());
        }
        db.close();
        return detalle;
    }

    public JSONArray obtenerNoSincronizados(){
        JSONArray detalle = new JSONArray();
        SQLiteDatabase db = this.getReadableDatabase();
        String sql = "SELECT * from fundo where sincronizado = 'N';";
        Cursor resultado = db.rawQuery(sql, null);

        try{
            while (resultado.moveToNext()){
                JSONObject obj = new JSONObject();
                obj.put("dni_conductor", resultado.getString(3));
                obj.put("nombre_conductor", resultado.getString(4));
                obj.put("codigo_categoria_personal", resultado.getInt(5));
                obj.put("nombre_empresa", resultado.getString(6));
                obj.put("codigo_tipo_vehiculo", resultado.getInt(7));
                obj.put("placa_entrada_fundo", resultado.getString(8));
                obj.put("procedencia_fundo", resultado.getString(9));
                obj.put("numero_asientos", resultado.getInt(10));
                obj.put("numero_ocupantes", resultado.getInt(11));
                obj.put("motivo_fundo", resultado.getString(12));
                obj.put("contenedor", resultado.getString(13));
                obj.put("entrada_salida_fundo", resultado.getString(14));
                obj.put("codigo_empresa", resultado.getInt(15));
                obj.put("codigo_usuario", resultado.getInt(16));
                obj.put("hora_manual", resultado.getString(18));
                obj.put("codigo_ruta",resultado.getInt(19));
                obj.put("codigo_entrada", resultado.getInt(20));
                obj.put("fecha_manual", resultado.getString(21));
                obj.put("codigo_off_line", resultado.getString(23));
                obj.put("codigo_caseta", resultado.getInt(24));
                obj.put("orientacion", resultado.getString(25));
                obj.put("pin", resultado.getString(26));
                obj.put("mano_obra", resultado.getString(27));
                obj.put("codigo_proveedor", resultado.getString(28));
                obj.put("codigo_ruta_transporte", resultado.getString(29));
                obj.put("codigo_proveedor_empresa", resultado.getString(30));
                detalle.put(obj);
            }
        }catch (JSONException e){
            Log.e("jsonArray", e.getLocalizedMessage());
        }
        db.close();
        return detalle;
    }

    public int obtenerMaximoCodigoEntrada(){
        SQLiteDatabase db = this.getReadableDatabase();
        String sql = "SELECT MAX(codigo_entrada_fundo) from fundo;";
        Cursor resultado = db.rawQuery(sql,null);
        int codigo_maximo = 0;
        while (resultado.moveToNext()){
            codigo_maximo = resultado.getInt(0);
        }
        return codigo_maximo;
    }

    public int cantidadNoSincronizados(){
        SQLiteDatabase db = this.getReadableDatabase();
        String sql = "SELECT count(*) from fundo where sincronizado = 'N';";
        Cursor resultado = db.rawQuery(sql,null);
        int codigo_maximo = 0;
        while (resultado.moveToNext()){
            codigo_maximo = resultado.getInt(0);
        }
        return codigo_maximo;
    }


    public long eliminarDatosFundo(){
        SQLiteDatabase db = this.getWritableDatabase();
        long resultado = db.delete("fundo", null, null);
        return resultado;
    }

    public boolean eliminarHoy(String fecha){
        boolean estado = false;
        try{
            SQLiteDatabase db = this.getWritableDatabase();
            db.delete("fundo", "fecha_manual = '"+fecha+"' or fecha_entrada_fundo ='"+fecha+"'", null);
            estado = true;
        }catch ( RuntimeException e){
            Log.e("Error al eliminar", e.getLocalizedMessage());
            estado = false;
        }
        return estado;
    }

    public boolean eliminarNoSincronizados(JSONArray lista){
        try{
            SQLiteDatabase db = this.getWritableDatabase();
            for (int i = 0; i < lista.length(); i++) {
                JSONObject data = lista.getJSONObject(i);
                String codigo_off_line = data.getString("codigo_off_line");
                db.delete("fundo", "codigo_entrada_fundo = 0 and codigo_off_line = '"+codigo_off_line+"'", null);
            }
        }catch (JSONException | RuntimeException e){
            Log.e("No sincronizados", e.getLocalizedMessage());
        }
        return true;
    }

    public boolean eliminarNoSincronizadosAll(){
        try{
            SQLiteDatabase db = this.getWritableDatabase();
            db.delete("fundo", "sincronizado = 'N'", null);
        }catch ( RuntimeException e){
            Log.e("No sincronizados", e.getLocalizedMessage());
        }
        return true;
    }

    public boolean agregarFundo(){
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put("codigo_entrada_fundo", this.getCodigo());
            values.put("fecha_entrada_fundo", this.getFecha());
            values.put("hora_entrada_fundo", this.getHora());
            values.put("dni_conductor", this.getDni());
            values.put("nombre_conductor", this.getNombre());
            values.put("codigo_categoria_personal", this.getCodigo_categoria_personal());
            values.put("nombre_empresa", this.getNombre_empresa());
            values.put("codigo_tipo_vehiculo", this.getCodigo_tipo_vehiculo());
            values.put("placa_entrada_fundo", this.getPlaca_entrada());
            values.put("procedencia_fundo", this.getProcedencia());
            values.put("numero_asientos", this.getNum_asientos());
            values.put("numero_ocupantes", this.getNum_ocupantes());
            values.put("motivo_fundo", this.getMotivo());
            values.put("contenedor", this.getContenedor());
            values.put("entrada_salida_fundo", this.getEntrada_salida());
            values.put("codigo_empresa", this.getCodigo_empresa());
            values.put("codigo_usuario", this.getCodigo_usuario());
            values.put("estado_entrada", this.getEstado_entrada());
            values.put("hora_manual", this.getHora_manual());
            values.put("codigo_ruta", this.getCodigo_ruta());
            values.put("codigo_entrada", this.getCodigo_entrada());
            values.put("fecha_manual", this.getFecha_manual());
            values.put("sincronizado", this.getSincronizado());
            values.put("codigo_caseta", this.getCodigo_caseta());
            values.put("orientacion", this.getOrientacion());
            values.put("pin", this.getPin());
            values.put("codigo_off_line", this.getCodigo_off_line());
            values.put("mano_obra", this.getMano_obra());
            values.put("codigo_proveedor", this.getCodigo_proveedor());
            values.put("codigo_ruta_transporte", this.getCodigo_ruta_transporte());
            values.put("codigo_proveedor_empresa", this.getCodigo_proveedor_empresa());
            db.insert("fundo", null, values);
            return true;
        }catch (SQLException e){
            Log.e("Fundo SQL.java", "Agregar" + e.getLocalizedMessage());
            return false;
        }
    }

    public long actualizarFundo(){
        try{
            SQLiteDatabase db = this.getWritableDatabase();

            db.delete("fundo", "codigo_entrada_fundo = 0 and codigo_off_line = '"+this.getCodigo_off_line()+"'", null);


            ContentValues values = new ContentValues();
            values.put("codigo_entrada_fundo", this.getCodigo());
            values.put("fecha_entrada_fundo", this.getFecha());
            values.put("hora_entrada_fundo", this.getHora());
            values.put("dni_conductor", this.getDni());
            values.put("nombre_conductor", this.getNombre());
            values.put("codigo_categoria_personal", this.getCodigo_categoria_personal());
            values.put("nombre_empresa", this.getNombre_empresa());
            values.put("codigo_tipo_vehiculo", this.getCodigo_tipo_vehiculo());
            values.put("placa_entrada_fundo", this.getPlaca_entrada());
            values.put("procedencia_fundo", this.getProcedencia());
            values.put("numero_asientos", this.getNum_asientos());
            values.put("numero_ocupantes", this.getNum_ocupantes());
            values.put("motivo_fundo", this.getMotivo());
            values.put("contenedor", this.getContenedor());
            values.put("entrada_salida_fundo", this.getEntrada_salida());
            values.put("codigo_empresa", this.getCodigo_empresa());
            values.put("codigo_usuario", this.getCodigo_usuario());
            values.put("estado_entrada", this.getEstado_entrada());
            values.put("hora_manual", this.getHora_manual());
            values.put("codigo_ruta", this.getCodigo_ruta());
            values.put("codigo_entrada", this.getCodigo_entrada());
            values.put("fecha_manual", this.getFecha_manual());
            values.put("sincronizado", this.getSincronizado());
            values.put("codigo_off_line", this.getCodigo_off_line());
            values.put("codigo_caseta", this.getCodigo_caseta());
            values.put("orientacion", this.getOrientacion());
            values.put("pin", this.getPin());
            values.put("mano_obra", this.getMano_obra());

            return db.insert("fundo", null, values);
            //return db.update("fundo",values, "codigo_entrada_fundo = 0 and codigo_off_line = '" + this.getCodigo_off_line() + "'", null);
        }catch (SQLException e){
            Log.e("Fundo.java", "actualizarFundo" + e.getLocalizedMessage());
            return -1;
        }
    }
}
