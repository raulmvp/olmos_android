package pe.proolmos.proolmoscontrol.Negocio;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

import pe.proolmos.proolmoscontrol.datos.AccesoDatos;

public class TipoPersonal extends AccesoDatos {
    private int codigo;
    private String descripcion;

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public ArrayList<TipoPersonal> cargarDatosTipoPersonal(ArrayList<TipoPersonal> lista){
        SQLiteDatabase db = this.getReadableDatabase();
        String sql = "select * from tipo_personal order by 2";
        Cursor resultado = db.rawQuery(sql,null);
        lista.clear();
        while (resultado.moveToNext()){
            TipoPersonal obj = new TipoPersonal();
            obj.setCodigo(resultado.getInt(0));
            obj.setDescripcion(resultado.getString(1));
            lista.add(obj);
        }
        return lista;
    }
}
