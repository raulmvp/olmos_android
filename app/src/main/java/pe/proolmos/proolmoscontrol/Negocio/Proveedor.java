package pe.proolmos.proolmoscontrol.Negocio;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import pe.proolmos.proolmoscontrol.datos.AccesoDatos;

public class Proveedor extends AccesoDatos {
    int codigo_proveedor;
    String ruc, razon_social;

    public int getCodigo_proveedor() {
        return codigo_proveedor;
    }

    public void setCodigo_proveedor(int codigo_proveedor) {
        this.codigo_proveedor = codigo_proveedor;
    }

    public String getRuc() {
        return ruc;
    }

    public void setRuc(String ruc) {
        this.ruc = ruc;
    }

    public String getRazon_social() {
        return razon_social;
    }

    public void setRazon_social(String razon_social) {
        this.razon_social = razon_social;
    }


    public long eliminarProveedor(){
        SQLiteDatabase db = this.getWritableDatabase();
        long resultado = db.delete("proveedor", null, null);
        return resultado;
    }

    public boolean agregarProveedor(){
        try {
            SQLiteDatabase db = this.getReadableDatabase();
            String sql = "SELECT count(*) from proveedor where codigo_proveedor = "+this.getCodigo_proveedor();
            Cursor resultado = db.rawQuery(sql, null);
            int existe = 0;
            while (resultado.moveToNext()){
                existe = resultado.getInt(0);
            }

            db = this.getWritableDatabase();
            ContentValues obj = new ContentValues();
            obj.put("codigo_proveedor", this.getCodigo_proveedor());
            obj.put("ruc", this.getRuc());
            obj.put("razon_social", this.getRazon_social());

            if(existe == 0){
                //agregar porque no existe
                db.insert("proveedor", null, obj);
            }else{
                db.update("proveedor", obj, "codigo_proveedor = " + this.getCodigo_proveedor(), null);
            }
            return true;
        }catch (SQLException e){
            Log.e("proveedor", "Agregar" + e.getLocalizedMessage());
            return false;
        }
    }
}
