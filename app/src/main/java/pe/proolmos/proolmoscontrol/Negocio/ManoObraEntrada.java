package pe.proolmos.proolmoscontrol.Negocio;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import pe.proolmos.proolmoscontrol.datos.AccesoDatos;

public class ManoObraEntrada extends AccesoDatos {
    private String sincronizado, codigo_off_line, codigo_off_line_fundo;
    private int codigo_entrada, codigo_mano_obra, numero_trabajadores;

    public String getSincronizado() {
        return sincronizado;
    }

    public void setSincronizado(String sincronizado) {
        this.sincronizado = sincronizado;
    }

    public String getCodigo_off_line() {
        return codigo_off_line;
    }

    public void setCodigo_off_line(String codigo_off_line) {
        this.codigo_off_line = codigo_off_line;
    }

    public String getCodigo_off_line_fundo() {
        return codigo_off_line_fundo;
    }

    public void setCodigo_off_line_fundo(String codigo_off_line_fundo) {
        this.codigo_off_line_fundo = codigo_off_line_fundo;
    }

    public int getCodigo_entrada() {
        return codigo_entrada;
    }

    public void setCodigo_entrada(int codigo_entrada) {
        this.codigo_entrada = codigo_entrada;
    }

    public int getCodigo_mano_obra() {
        return codigo_mano_obra;
    }

    public void setCodigo_mano_obra(int codigo_mano_obra) {
        this.codigo_mano_obra = codigo_mano_obra;
    }

    public int getNumero_trabajadores() {
        return numero_trabajadores;
    }

    public void setNumero_trabajadores(int numero_trabajadores) {
        this.numero_trabajadores = numero_trabajadores;
    }

    public long eliminarDatosManoObraEntrada(){
        SQLiteDatabase db = this.getWritableDatabase();
        long resultado = db.delete("mano_obra_entrada", null, null);
        return resultado;
    }

    public JSONArray obtenerNoSincronizados(){
        JSONArray detalle = new JSONArray();

        SQLiteDatabase db = this.getReadableDatabase();
        String sql = "SELECT * from mano_obra_entrada where sincronizado = 'N'";
        Cursor resultado = db.rawQuery(sql, null);

        try{
            while (resultado.moveToNext()){
                JSONObject obj = new JSONObject();
                obj.put("codigo_tipo_mano_obra", resultado.getString(1));
                obj.put("numero_trabajadores", resultado.getString(2));
                obj.put("codigo_off_line", resultado.getString(4));
                obj.put("codigo_off_line_fundo", resultado.getString(5));
                detalle.put(obj);
            }
        }catch (JSONException e){
            Log.e("ManoObraEntrada", e.getLocalizedMessage());
        }
        db.close();
        return detalle;
    }

    public boolean eliminarNoSincronizados(JSONArray lista){
        try{
            SQLiteDatabase db = this.getWritableDatabase();
            for (int i = 0; i < lista.length(); i++) {
                JSONObject data = lista.getJSONObject(i);
                String codigo_off_line = data.getString("codigo_off_line");
                String codigo_off_line_fundo = data.getString("codigo_off_line_fundo");
                db.delete("mano_obra_entrada", "codigo_entrada_fundo = 0 and codigo_off_line = '"+codigo_off_line+"' and codigo_off_line_fundo = '"+codigo_off_line_fundo+"'", null);
            }
        }catch (JSONException | RuntimeException e){
            Log.e("No sincronizados mano", e.getLocalizedMessage());
        }
        return true;
    }

    public boolean agregarManoObraEntrada(){
        try {

            SQLiteDatabase db = this.getReadableDatabase();
            String sql = "SELECT count(*) from mano_obra_entrada where codigo_entrada_fundo = "+this.getCodigo_entrada()+" and codigo_tipo_mano_obra = "+this.getCodigo_mano_obra() +" and codigo_off_line ='"+this.getCodigo_off_line()+"' and codigo_off_line_fundo='"+this.getCodigo_off_line_fundo()+"'";
            Cursor resultado = db.rawQuery(sql, null);
            int existe = 0;
            while (resultado.moveToNext()){
                existe = resultado.getInt(0);
            }

            db = this.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put("codigo_entrada_fundo", this.getCodigo_entrada());
            values.put("codigo_tipo_mano_obra", this.getCodigo_mano_obra());
            values.put("numero_trabajadores", this.getNumero_trabajadores());
            values.put("sincronizado", this.getSincronizado());
            values.put("codigo_off_line", this.getCodigo_off_line());
            values.put("codigo_off_line_fundo", this.getCodigo_off_line_fundo());


            if(existe == 0){
                db.insert("mano_obra_entrada", null, values);
            }else{
                db.update("mano_obra_entrada", values, "codigo_entrada_fundo = " + this.getCodigo_entrada()+" and codigo_tipo_mano_obra = "+this.getCodigo_mano_obra() +" and codigo_off_line ='"+this.getCodigo_off_line()+"' and codigo_off_line_fundo='"+this.getCodigo_off_line_fundo()+"'", null);
            }
            return true;
        }catch (RuntimeException e){
            Log.e("ManoObraEntrada.java", "Agregar");
            return false;
        }
    }
}
