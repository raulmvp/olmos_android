package pe.proolmos.proolmoscontrol.Negocio;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

import pe.proolmos.proolmoscontrol.datos.AccesoDatos;

public class Caseta extends AccesoDatos {
    private int codigo;
    private String descripcion;

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public ArrayList<Caseta> cargarDatosCaseta(ArrayList<Caseta> lista){
        lista.clear();
        Caseta obj = new Caseta();
        obj.setCodigo(1);
        obj.setDescripcion("GARITA DE CONTROL DEL KM. 855");
        lista.add(obj);

        Caseta obj2 = new Caseta();
        obj2.setCodigo(2);
        obj2.setDescripcion("GARITA DE CONTROL PALO VERDE");
        lista.add(obj2);

        Caseta obj3 = new Caseta();
        obj3.setCodigo(3);
        obj3.setDescripcion("GARITA DE CONTROL MORROPE");
        lista.add(obj3);

        Caseta obj5 = new Caseta();
        obj5.setCodigo(0);
        obj5.setDescripcion("OTRA ENTRADA");
        lista.add(obj5);
        return lista;
    }
}
