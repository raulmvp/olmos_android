package pe.proolmos.proolmoscontrol.Negocio;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import pe.proolmos.proolmoscontrol.datos.AccesoDatos;

public class Vehiculo extends AccesoDatos {
    int codigo_vehiculo, codigo_tipo_vehiculo, capacidad;
    String placa, estado;

    public int getCodigo_vehiculo() {
        return codigo_vehiculo;
    }

    public void setCodigo_vehiculo(int codigo_vehiculo) {
        this.codigo_vehiculo = codigo_vehiculo;
    }

    public int getCodigo_tipo_vehiculo() {
        return codigo_tipo_vehiculo;
    }

    public void setCodigo_tipo_vehiculo(int codigo_tipo_vehiculo) {
        this.codigo_tipo_vehiculo = codigo_tipo_vehiculo;
    }

    public int getCapacidad() {
        return capacidad;
    }

    public void setCapacidad(int capacidad) {
        this.capacidad = capacidad;
    }

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public long eliminarVehiculo(){
        SQLiteDatabase db = this.getWritableDatabase();
        long resultado = db.delete("vehiculo", null, null);
        return resultado;
    }

    public boolean agregarVehiculo(){
        try {
            SQLiteDatabase db = this.getReadableDatabase();
            String sql = "SELECT count(*) from vehiculo where codigo_vehiculo = "+this.getCodigo_vehiculo();
            Cursor resultado = db.rawQuery(sql, null);
            int existe = 0;
            while (resultado.moveToNext()){
                existe = resultado.getInt(0);
            }

            db = this.getWritableDatabase();
            ContentValues obj = new ContentValues();
            obj.put("codigo_vehiculo", this.getCodigo_vehiculo());
            obj.put("codigo_tipo_vehiculo", this.getCodigo_tipo_vehiculo());
            obj.put("placa", this.getPlaca());
            obj.put("capacidad", this.getCapacidad());
            obj.put("estado", this.getEstado());

            if(existe == 0){
                //agregar porque no existe
                Log.e("agregar",this.getPlaca()+"cod:"+this.getCodigo_vehiculo());
                db.insert("vehiculo", null, obj);
            }else{
                Log.e("actualizar",this.getPlaca()+"cod:"+this.getCodigo_vehiculo());
                db.update("vehiculo", obj, "codigo_vehiculo = " + this.getCodigo_vehiculo(), null);
            }


            return true;
        }catch (SQLException e){
            Log.e("agregarVehiculo-vehicul", "Agregar" + e.getLocalizedMessage());
            return false;
        }
    }
}
