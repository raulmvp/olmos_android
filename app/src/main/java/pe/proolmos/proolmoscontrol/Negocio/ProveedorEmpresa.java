package pe.proolmos.proolmoscontrol.Negocio;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import pe.proolmos.proolmoscontrol.datos.AccesoDatos;

public class ProveedorEmpresa extends AccesoDatos {
    int codigo_proveedor_empresa, codigo_proveedor, codigo_conductor, codigo_vehiculo, codigo_ruta_transporte;
    String estado;

    public int getCodigo_proveedor_empresa() {
        return codigo_proveedor_empresa;
    }

    public void setCodigo_proveedor_empresa(int codigo_proveedor_empresa) {
        this.codigo_proveedor_empresa = codigo_proveedor_empresa;
    }

    public int getCodigo_proveedor() {
        return codigo_proveedor;
    }

    public void setCodigo_proveedor(int codigo_proveedor) {
        this.codigo_proveedor = codigo_proveedor;
    }

    public int getCodigo_conductor() {
        return codigo_conductor;
    }

    public void setCodigo_conductor(int codigo_conductor) {
        this.codigo_conductor = codigo_conductor;
    }

    public int getCodigo_vehiculo() {
        return codigo_vehiculo;
    }

    public void setCodigo_vehiculo(int codigo_vehiculo) {
        this.codigo_vehiculo = codigo_vehiculo;
    }

    public int getCodigo_ruta_transporte() {
        return codigo_ruta_transporte;
    }

    public void setCodigo_ruta_transporte(int codigo_ruta_transporte) {
        this.codigo_ruta_transporte = codigo_ruta_transporte;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public long eliminarProveedorEmpresa(){
        SQLiteDatabase db = this.getWritableDatabase();
        long resultado = db.delete("proveedor_empresa", null, null);
        return resultado;
    }

    public boolean agregarProveedorEmpresa(){
        try {
            SQLiteDatabase db = this.getReadableDatabase();
            String sql = "SELECT count(*) from proveedor_empresa where codigo_proveedor_empresa = "+this.getCodigo_proveedor_empresa();
            Cursor resultado = db.rawQuery(sql, null);
            int existe = 0;
            while (resultado.moveToNext()){
                existe = resultado.getInt(0);
            }

            db = this.getWritableDatabase();
            ContentValues obj = new ContentValues();
            obj.put("codigo_proveedor_empresa", this.getCodigo_proveedor_empresa());
            obj.put("codigo_proveedor", this.getCodigo_proveedor());
            obj.put("codigo_conductor", this.getCodigo_conductor());
            obj.put("codigo_vehiculo", this.getCodigo_vehiculo());
            obj.put("codigo_ruta_transporte", this.getCodigo_ruta_transporte());
            obj.put("estado", this.getEstado());

            if(existe == 0){
                //agregar porque no existe
                db.insert("proveedor_empresa", null, obj);
            }else{
                db.update("proveedor_empresa", obj, "codigo_proveedor_empresa = " + this.getCodigo_proveedor_empresa(), null);
            }
            return true;
        }catch (SQLException e){
            Log.e("proveedor_empresa", "Agregar" + e.getLocalizedMessage());
            return false;
        }
    }
}
