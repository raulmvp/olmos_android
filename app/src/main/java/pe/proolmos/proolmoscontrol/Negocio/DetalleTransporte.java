package pe.proolmos.proolmoscontrol.Negocio;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import pe.proolmos.proolmoscontrol.datos.AccesoDatos;

public class DetalleTransporte extends AccesoDatos {
    int codigo_detalle_transporte, codigo_conductor, codigo_vehiculo;
    String estado;

    public int getCodigo_detalle_transporte() {
        return codigo_detalle_transporte;
    }

    public void setCodigo_detalle_transporte(int codigo_detalle_transporte) {
        this.codigo_detalle_transporte = codigo_detalle_transporte;
    }

    public int getCodigo_conductor() {
        return codigo_conductor;
    }

    public void setCodigo_conductor(int codigo_conductor) {
        this.codigo_conductor = codigo_conductor;
    }

    public int getCodigo_vehiculo() {
        return codigo_vehiculo;
    }

    public void setCodigo_vehiculo(int codigo_vehiculo) {
        this.codigo_vehiculo = codigo_vehiculo;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }


    public long eliminarDetalleTransporte(){
        SQLiteDatabase db = this.getWritableDatabase();
        long resultado = db.delete("detalle_transporte", null, null);
        return resultado;
    }

    public boolean agregarDetalleTransporte(){
        try {
            SQLiteDatabase db = this.getReadableDatabase();
            String sql = "SELECT count(*) from detalle_transporte where codigo_detalle_transporte = "+this.getCodigo_detalle_transporte();
            Cursor resultado = db.rawQuery(sql, null);
            int existe = 0;
            while (resultado.moveToNext()){
                existe = resultado.getInt(0);
            }

            db = this.getWritableDatabase();
            ContentValues obj = new ContentValues();
            obj.put("codigo_detalle_transporte", this.getCodigo_detalle_transporte());
            obj.put("codigo_conductor", this.getCodigo_conductor());
            obj.put("codigo_vehiculo", this.getCodigo_vehiculo());
            obj.put("estado", this.getEstado());

            if(existe == 0){
                //agregar porque no existe
                db.insert("detalle_transporte", null, obj);
            }else{
                db.update("detalle_transporte", obj, "codigo_detalle_transporte = " + this.getCodigo_detalle_transporte(), null);
            }
            return true;
        }catch (SQLException e){
            Log.e("proveedor", "Agregar" + e.getLocalizedMessage());
            return false;
        }
    }
}
