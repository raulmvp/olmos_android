package pe.proolmos.proolmoscontrol.Negocio;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;

import pe.proolmos.proolmoscontrol.datos.AccesoDatos;

public class Empresa extends AccesoDatos {
    private int codigo;
    private String nombre;
    private String ruc;
    private Double longitud;
    private Double latitud;

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getRuc() {
        return ruc;
    }

    public void setRuc(String ruc) {
        this.ruc = ruc;
    }

    public Double getLongitud() {
        return longitud;
    }

    public void setLongitud(Double longitud) {
        this.longitud = longitud;
    }

    public Double getLatitud() {
        return latitud;
    }

    public void setLatitud(Double latitud) {
        this.latitud = latitud;
    }

    public ArrayList<Empresa> cargarDatosEmpresa(ArrayList<Empresa> lista){
        SQLiteDatabase db = this.getReadableDatabase();
        String sql = "select * from empresa order by 2";
        Cursor resultado = db.rawQuery(sql,null);
        lista.clear();
        while (resultado.moveToNext()){
            Empresa obj = new Empresa();
            obj.setCodigo(resultado.getInt(0));
            obj.setNombre(resultado.getString(1));
            obj.setRuc(resultado.getString(2));
            obj.setLongitud(resultado.getDouble(3));
            obj.setLatitud(resultado.getDouble(4));
            lista.add(obj);
        }
        return lista;
    }

    public long eliminarEmpresas(){
        SQLiteDatabase db = this.getWritableDatabase();
        long resultado = db.delete("empresa", null, null);
        return resultado;
    }

    public boolean agregarEmpresa(){
        try {
            SQLiteDatabase db = this.getReadableDatabase();
            String sql = "SELECT count(*) from empresa where codigo = "+this.getCodigo();
            Cursor resultado = db.rawQuery(sql, null);
            int existe = 0;
            while (resultado.moveToNext()){
                existe = resultado.getInt(0);
            }

            db = this.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put("codigo", this.getCodigo());
            values.put("nombre", this.getNombre());
            values.put("ruc", this.getRuc());
            values.put("longitud", this.getLongitud());
            values.put("latitud", this.getLatitud());

            if(existe == 0){
                db.insert("empresa", null, values);
            }else{
                db.update("empresa", values, "codigo="+this.getCodigo(), null);
            }
            return true;
        }catch (RuntimeException e){
            return false;
        }
    }
}
