package pe.proolmos.proolmoscontrol.Negocio;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;

import pe.proolmos.proolmoscontrol.datos.AccesoDatos;

public class ManoObra extends AccesoDatos {
    private int codigo;
    private String nombre, estado, elegido;

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getElegido() {
        return elegido;
    }

    public void setElegido(String elegido) {
        this.elegido = elegido;
    }

    public int existeManoObra(){
        SQLiteDatabase db = this.getReadableDatabase();
        String sql = "select * from mano_obra where estado = 'A' order by 2";
        Cursor resultado = db.rawQuery(sql,null);
        int existe = 0;
        while (resultado.moveToNext()){
            existe ++;
        }
        return existe;
    }

    public ArrayList<ManoObra> cargarDatosManoObra(ArrayList<ManoObra> lista){
        SQLiteDatabase db = this.getReadableDatabase();
        String sql = "select * from mano_obra where estado = 'A' order by 2";
        Cursor resultado = db.rawQuery(sql,null);
        lista.clear();
        while (resultado.moveToNext()){
            ManoObra obj = new ManoObra();
            obj.setCodigo(resultado.getInt(0));
            obj.setNombre(resultado.getString(1));
            obj.setEstado(resultado.getString(2));
            obj.setElegido("N");
            lista.add(obj);
        }
        return lista;
    }

    public long eliminarManoObras(){
        SQLiteDatabase db = this.getWritableDatabase();
        long resultado = db.delete("mano_obra", null, null);
        return resultado;
    }

    public boolean agregarManoObra(){
        try {
            SQLiteDatabase db = this.getReadableDatabase();
            String sql = "SELECT count(*) from mano_obra where codigo = "+this.getCodigo();
            Cursor resultado = db.rawQuery(sql, null);
            int existe = 0;
            while (resultado.moveToNext()){
                existe = resultado.getInt(0);
            }

            db = this.getWritableDatabase();
            ContentValues obj = new ContentValues();
            obj.put("codigo", this.getCodigo());
            obj.put("nombre", this.getNombre());
            obj.put("estado", this.getEstado());

            if(existe == 0){
                //agregar porque no existe
                db.insert("mano_obra", null, obj);
            }else{
                db.update("mano_obra", obj, "codigo = " + this.getCodigo(), null);
            }
            return true;
        }catch (RuntimeException e){
            Log.e("ManoObra.java", "Agregar");
            return false;
        }
    }
}
