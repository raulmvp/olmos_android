package pe.proolmos.proolmoscontrol.Negocio;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import pe.proolmos.proolmoscontrol.datos.AccesoDatos;

public class Visita extends AccesoDatos {
    private String dni, comentario, sincronizado, codigo_off_line, codigo_off_line_fundo;
    private int codigo_entrada, codigo_visita;

    public int getCodigo_visita() {
        return codigo_visita;
    }

    public void setCodigo_visita(int codigo_visita) {
        this.codigo_visita = codigo_visita;
    }

    public String getDni() {
        return dni;
    }

    public int getCodigo_entrada() {
        return codigo_entrada;
    }

    public void setCodigo_entrada(int codigo_entrada) {
        this.codigo_entrada = codigo_entrada;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public String getComentario() {
        return comentario;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

    public String getSincronizado() {
        return sincronizado;
    }

    public void setSincronizado(String sincronizado) {
        this.sincronizado = sincronizado;
    }

    public String getCodigo_off_line() {
        return codigo_off_line;
    }

    public void setCodigo_off_line(String codigo_off_line) {
        this.codigo_off_line = codigo_off_line;
    }

    public String getCodigo_off_line_fundo() {
        return codigo_off_line_fundo;
    }

    public void setCodigo_off_line_fundo(String codigo_off_line_fundo) {
        this.codigo_off_line_fundo = codigo_off_line_fundo;
    }

    public ArrayList<Visita> cargarDatosVisitas(ArrayList<Visita> lista){
        SQLiteDatabase db = this.getReadableDatabase();
        String sql = "select * from visita;";
        Cursor resultado = db.rawQuery(sql,null);
        lista.clear();
        while (resultado.moveToNext()){
            Visita obj = new Visita();
            obj.setCodigo_visita(resultado.getInt(0));
            obj.setDni(resultado.getString(1));
            obj.setCodigo_entrada(resultado.getInt(2));
            obj.setComentario(resultado.getString(3));
            obj.setSincronizado(resultado.getString(4));
            obj.setCodigo_off_line(resultado.getString(5));
            obj.setCodigo_off_line_fundo(resultado.getString(6));
            lista.add(obj);
        }
        return lista;
    }

    public JSONArray obtenerNoSincronizados(){
        JSONArray detalle = new JSONArray();

        SQLiteDatabase db = this.getReadableDatabase();
        String sql = "SELECT * from visita where sincronizado = 'N'";
        Cursor resultado = db.rawQuery(sql, null);

        try{
            while (resultado.moveToNext()){
                JSONObject obj = new JSONObject();
                obj.put("dni", resultado.getString(1));
                obj.put("comentario", resultado.getString(3));
                obj.put("codigo_off_line", resultado.getString(5));
                obj.put("codigo_off_line_fundo", resultado.getString(6));
                detalle.put(obj);
            }
        }catch (JSONException e){
            Log.e("visitasArray", e.getLocalizedMessage());
        }
        db.close();
        return detalle;
    }

    public boolean eliminarNoSincronizados(JSONArray lista){
        try{
            SQLiteDatabase db = this.getWritableDatabase();
            for (int i = 0; i < lista.length(); i++) {
                JSONObject data = lista.getJSONObject(i);
                String codigo_off_line = data.getString("codigo_off_line");
                String codigo_off_line_fundo = data.getString("codigo_off_line_fundo");
                db.delete("visita", "codigo_entrada_fundo = 0 and codigo_off_line = '"+codigo_off_line+"' and codigo_off_line_fundo = '"+codigo_off_line_fundo+"'", null);
            }
        }catch (JSONException | RuntimeException e){
            Log.e("No sincronizado visitas", e.getLocalizedMessage());
        }
        return true;
    }

    public long eliminarDatosVisitas(){
        SQLiteDatabase db = this.getWritableDatabase();
        long resultado = db.delete("visita", null, null);
        return resultado;
    }

    public boolean agregarVisitas(ArrayList<Visita> lista){
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            for(int i =0; i < lista.size(); i++){
                ContentValues values = new ContentValues();
                Visita item = lista.get(i);
                values.put("codigo_visita", item.getCodigo_visita());
                values.put("dni", item.getDni());
                values.put("codigo_entrada_fundo", item.getCodigo_entrada());
                values.put("comentario", item.getComentario());
                values.put("sincronizado", item.getSincronizado());
                values.put("codigo_off_line", item.getCodigo_off_line());
                values.put("codigo_off_line_fundo", item.getCodigo_off_line_fundo());
                db.insert("visita", null, values);
            }

            return true;
        }catch (RuntimeException e){
            Log.e("Visita.java", "Agregar");
            return false;
        }
    }

    public boolean agregarVisita(Visita item){
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put("codigo_visita", item.getCodigo_visita());
            values.put("dni", item.getDni());
            values.put("codigo_entrada_fundo", item.getCodigo_entrada());
            values.put("comentario", item.getComentario());
            values.put("sincronizado", item.getSincronizado());
            values.put("codigo_off_line", item.getCodigo_off_line());
            values.put("codigo_off_line_fundo", item.getCodigo_off_line_fundo());
            db.insert("visita", null, values);
            return true;
        }catch (RuntimeException e){
            Log.e("Visita.java", "Agregar");
            return false;
        }
    }

    public long actualizarVisita(){
        try{
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put("codigo_visita", this.getCodigo_visita());
            values.put("codigo_entrada_fundo", this.getCodigo_entrada());
            values.put("comentario", this.getComentario());
            values.put("sincronizado", this.getSincronizado());
            //values.put("codigo_off_line", this.getCodigo_off_line());
            return db.update("visita",values, "codigo_visita = 0 and codigo_off_line = '" + this.getCodigo_off_line() + "' and dni = '"+this.getDni()+"' and codigo_off_line_fundo = '"+this.getCodigo_off_line_fundo()+"'", null);
        }catch (SQLException e){
            Log.e("Fundo.java", "actualizarFundo" + e.getLocalizedMessage());
            return -1;
        }
    }
}
