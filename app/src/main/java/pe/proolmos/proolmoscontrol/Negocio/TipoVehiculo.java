package pe.proolmos.proolmoscontrol.Negocio;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

import pe.proolmos.proolmoscontrol.datos.AccesoDatos;

public class TipoVehiculo extends AccesoDatos {
    private int codigo;
    private int codigo_categoria;
    private String descripcion;

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public int getCodigo_categoria() {
        return codigo_categoria;
    }

    public void setCodigo_categoria(int codigo_categoria) {
        this.codigo_categoria = codigo_categoria;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public ArrayList<TipoVehiculo> cargarDatosTipoVehiculo(ArrayList<TipoVehiculo> lista){
        SQLiteDatabase db = this.getReadableDatabase();
        String sql = "select * from tipo_vehiculo order by 3,2";
        Cursor resultado = db.rawQuery(sql,null);
        lista.clear();
        while (resultado.moveToNext()){
            TipoVehiculo obj = new TipoVehiculo();
            obj.setCodigo(resultado.getInt(0));
            obj.setDescripcion(resultado.getString(1));
            obj.setCodigo_categoria(resultado.getInt(2));
            lista.add(obj);
        }
        return lista;
    }
}
